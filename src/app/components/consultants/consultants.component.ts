
import { DataService } from './../../providers/data/data.service';
import { ApiService } from './../../providers/api/api.service';
import { ConsultantService } from './../../providers/consultant/consultant.service';
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, NgZone } from '@angular/core';
import {  Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { remote, app } from 'electron';

@Component({
  selector: 'app-consultants',
  templateUrl: './consultants.component.html',
  styleUrls: ['./consultants.component.scss']
})
export class ConsultantsComponent implements OnInit {

  defaultConsultants: any;
  defaultConsultantsFilter: any;
  currentsConsultants: any;
  activeConsultants; any;
  totalPages: number;
  nbItems: number;
  nbItemsByPage: number;
  currentPage: number;
  filtreConsultant: string[] = [];
  numero: number;
  loading: boolean;
  percentageLoading: number;
  interval: any;
  userRole: any;

  @Output() consultants = new EventEmitter();

  constructor(
    private consultant: ConsultantService,
    private spinner: NgxSpinnerService,
    private api: ApiService,
    private route: Router,
    private data: DataService,
    public zone: NgZone
  ) {
    this.defaultConsultants = [];
    this.defaultConsultantsFilter = [];
    this.currentsConsultants = [];
    this.activeConsultants = [];
    this.currentPage = 1;
    this.totalPages = 1;
    this.nbItems = 0;
    this.nbItemsByPage = 15;
    this.filtreConsultant.push('noFilter');
    this.filtreConsultant.push('statut');
    this.filtreConsultant.push('domain');
    this.filtreConsultant.push('profil');
    this.filtreConsultant.push('niveau');
    this.filtreConsultant.push('age');
    this.filtreConsultant.push('exp_geographique');
    this.filtreConsultant.push('exp_professionel');
    this.filtreConsultant.push('genre');
    this.filtreConsultant.push('experience');
    this.numero = 1;
    this.loading = false;
    this.percentageLoading = 10;
    this.interval = null;
    this.getDataMessage();
    this.userRole = JSON.parse(localStorage.getItem('user-role'));
    this.consultant.getMessage().subscribe(data => {
          if (data && data.message === 'new-consultant' || data.message === 'update-consultant') {
            this.listConsultants();
          }
    });
  }


  ngOnInit() {
    this.listConsultantsOffline();
    this.resetDataMessage();
    this.refreshConsultantData();
  }

  scrollTop() {
    // Hack: Scrolls to top of Page after page view initialized
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

    // Set the loading
    setLoading() {
      this.interval = setInterval( () => {
         this.percentageLoading += 10;
         if (this.percentageLoading === 100) {
           this.percentageLoading = 0;
         }
       }, 1000);
     }

     // Clear the loading
     clearLoading() {
      this.percentageLoading = 0;
       clearInterval(this.interval );
       for (let i = 0; i < 100; i++) {
         clearInterval(i);
       }
     }


  // refrsh consultant data
  refreshConsultantData() {
    setInterval(() => {
        this.listConsultants();
    }, 600000);
  }

  // Get the list of consultants
  listConsultants() {
       this.consultant.listConsultantAll(true).then((consultants: any) => {
        this.loading = false;
        if (consultants.message === 'success') {
          this.zone.run(() => {
            this.defaultConsultants = consultants.Liste_consultant;
            this.defaultConsultantsFilter = this.defaultConsultants;
            this.consultants.emit(JSON.stringify(this.defaultConsultants));
            localStorage.setItem('consultant-data-session', JSON.stringify(this.defaultConsultants));
            this.currentsConsultants = this.data.formatArrayToMatrix(this.defaultConsultants, this.nbItemsByPage);
            this.totalPages = this.currentsConsultants.length;
            this.nbItems = this.defaultConsultants.length;
            this.scrollTop();
          });
            this.updateActiveList(1);
         
        }

      }).catch(error => {
        this.loading = false;
        this.api.manageServerError(error);
      });
  }


    // Get the list of consultants
    listConsultantsOffline() {

        this.loading = true;
        const listConsultants = localStorage.getItem('data-consultant-all') ? JSON.parse(localStorage.getItem('data-consultant-all')) : null;
        
        if (listConsultants && listConsultants.message === 'success') {
          this.zone.run(() => {
            this.loading = false;
            this.defaultConsultants = listConsultants.Liste_consultant;
            this.defaultConsultantsFilter = this.defaultConsultants;
            this.consultants.emit(JSON.stringify(this.defaultConsultants));
            this.currentsConsultants = this.data.formatArrayToMatrix(this.defaultConsultants, this.nbItemsByPage);
            this.totalPages = this.currentsConsultants.length;
            this.nbItems = this.defaultConsultants.length;
            this.scrollTop();
          });
            this.updateActiveList(1);
        
        } else {
            this.loading = false;
            if (this.currentsConsultants && this.currentsConsultants.length === 0) {
              this.loading = true;
              this.listConsultants();
            }
        }
  }


  // Open the consultants CV
  openCV(consultant: any) {
    if (consultant.cv_users && consultant.cv_users.length > 0) {
        const cvs = consultant.cv_users;
        const key = 'consultant';
        localStorage.setItem(key, JSON.stringify(cvs));
        const data = cvs;
        const url  = 'data:application/pdf;base64, ' + data[0].cv_en_base_64;
        window.open(url);
    } else {
          alert('Aucun cv enrégistré pour ce consultant !');
    }
  }


  // show the confirmation dialog
  confirmDialog(consultant: any, index: number) {
    const dialog = remote.dialog;
    dialog.showMessageBox({
      type: 'none',
      title: 'Confirmation',
      message: 'Voulez-vous supprimer ce consultant ?',
      buttons: ['Non', 'Oui']
    }, (reponse) => {
          if (reponse === 0) {
            return;
          } else {
            this.deleteConsultant(consultant, index);
          }
    });
  }

  // Delete the consultant
  deleteConsultant(consultant: any, index: number) {

    this.spinner.show('consultant', {
      type: 'ball-spin-fade',
      size: 'large',
      bdColor: 'rgba(251,140,0, .8)',
      color: 'white'
    });

    this.consultant.deleteConsultant(consultant.consultant_id).then((reponse: any) => {

      if (reponse && reponse.message === 'success') {
          // send a message with profil data
         this.consultant.sendMessage('delete-consultant'); 
         this.activeConsultants.splice(index, 1);
         this.spinner.hide('consultant');
         this.listConsultants();
      }

    }).catch(error => {

        console.log(error);
        this.spinner.hide('consultant');
        this.api.manageServerError(error);

        if (error.error && error.error.user_not_found) {
            alert('Impossible de supprimer le consultant. Essayer à nouveau');
        }

        if (error.error && error.error.consultant_id_not_exist) {
          alert('Ce consultant n existe pas dans la Base de Données');
        }
    });

  }

  // Show the detail informations of consultant
  showConsultant(consultant: any) {
    const key = 'consultant';
    localStorage.setItem(key, JSON.stringify(consultant));
    this.route.navigate([`/dashboard/consultant/${key}`]);
  }



  // Get the filters params and apply the jointure on data
  consultantFilters(event) {

    const filter  = JSON.parse(event);
    // filter by noFiler
    const noFilter = this.data.getDataByKey('noFilter', filter);
    if (noFilter && noFilter.length > 0) {
      const noFilterData = this.noFilter(filter);
      this.defaultConsultantsFilter = noFilterData;
    }

    // filter by statut
    const statut = this.data.getDataByKey('statut', filter);
    if (statut && statut.length > 0) {
      const statutData = this.filterByStatut(filter);
      this.defaultConsultantsFilter = statutData;
    }

    // Filter by domain
    const domain = this.data.getDataByKey('domain', filter);
    if (domain && domain.length > 0) {
      const domanainsData = this.filterByDomains(filter);
      this.defaultConsultantsFilter = domanainsData;
    }

    // Filter by Profil
    const profil = this.data.getDataByKey('profil', filter);
    if (profil && profil.length > 0) {
      const profilData = this.filterByProfil(filter);
      this.defaultConsultantsFilter = profilData;
    }

    // Filter by Niveau
    const niveau = this.data.getDataByKey('niveau', filter);
    if (niveau && niveau.length > 0) {
      const niveauData = this.filterByNiveau(filter);
      this.defaultConsultantsFilter = niveauData;
    }


    // Filter by age
    const age = this.data.getDataByKey('age', filter);
    if (age && age.length > 0) {
      const ageData = this.filterByAge(filter);
      this.defaultConsultantsFilter = ageData;
    }

    // Filter by experience Geographique
    const exp_geographique = this.data.getDataByKey('exp_geographique', filter);
    if (exp_geographique && exp_geographique.length > 0) {
      const expGeoData = this.filterByAge(filter);
      this.defaultConsultantsFilter = expGeoData;
    }

    // Filter by experience profesionnelle
    const exp_professionel = this.data.getDataByKey('exp_professionel', filter);
    if (exp_professionel && exp_professionel.length > 0) {
      const expProData = this.filterByAge(filter);
      this.defaultConsultantsFilter = expProData;
    }

    // Filter by genre
    const genre = this.data.getDataByKey('genre', filter);
    if (genre && genre.length > 0) {
      const genreData = this.filterByGenre(filter);
      this.defaultConsultantsFilter = genreData;
    }

    // Filter by experience
    const experience = this.data.getDataByKey('experience', filter);
    if (experience && experience.length > 0) {
      const experienceData = this.filterByExperience(filter);
      this.defaultConsultantsFilter = experienceData;
    }

    this.consultants.emit(JSON.stringify(this.defaultConsultantsFilter));
    localStorage.setItem('consultant-data-session', JSON.stringify(this.defaultConsultantsFilter));
    this.currentsConsultants = this.data.formatArrayToMatrix(this.defaultConsultantsFilter, this.nbItemsByPage);
    this.updateActiveList(1);
   }

   // Implement no filter
   noFilter (filter: any) {
    let datasNoFilter = [];
        if (filter && filter.length > 0) {
          const dataNoFilter = this.data.getDataByKey('noFilter', filter);
          if (dataNoFilter[0] === 1) {
            datasNoFilter = this.defaultConsultants;
          } else {
              datasNoFilter = this.activeConsultants;
          }
        }
        return datasNoFilter;
   }


    // Implement filter by domaines
    filterByStatut(filter: any) {

      const dataStatusFilter = [];
      if (filter && filter.length > 0) {
          const statutFilter = this.data.getDataByKey('statut', filter);
          this.defaultConsultantsFilter.forEach(element => {
                if (this.data.isIdinarray([element.statut], statutFilter)) {
                  dataStatusFilter.push(element);
                }
          });
      }
      return dataStatusFilter;

    }


   // Implement filter by domaines
   filterByDomains(filter: any) {

     const dataDomaninsFilter = [];

     if (filter && filter.length > 0) {
         const domainFilter = this.data.getDataByKey('domain', filter);
         this.defaultConsultantsFilter.forEach(element => {
               if (this.data.isIdinarray(element.domaine_expertise_users, domainFilter)) {
                  dataDomaninsFilter.push(element);
               }
         });
     }

     return dataDomaninsFilter;

   }

     // Implement filter by pofil
     filterByProfil(filter: any) {

       const dataProfilsFilter = [];

       if (filter && filter.length > 0) {
           const profilFilter = this.data.getDataByKey('profil', filter);
           this.defaultConsultantsFilter.forEach(element => {
                 if (this.data.isIdinarray(element.profil_users, profilFilter)) {
                   dataProfilsFilter.push(element);
                 }
           });
       }

       return dataProfilsFilter;

     }


        // Implement filter by niveaux
        filterByNiveau(filter: any) {

          const dataNiveauxFilter = [];

          if (filter && filter.length > 0) {
              const niveauFilter = this.data.getDataByKey('niveau', filter);
              this.defaultConsultantsFilter.forEach(element => {
                    if (this.data.isIdinarray([element.niveau_formation], niveauFilter)) {
                      dataNiveauxFilter.push(element);
                    }
              });
          }

          return dataNiveauxFilter;
        }


          // Implement filter by age
          filterByAge(filter: any) {

            const dataAgesFilter = [];

            if (filter && filter.length > 0) {
                const ageFilter = this.data.getDataByKey('age', filter);
                this.defaultConsultantsFilter.forEach(element => {
                      if (this.data.isIdinarray([{id: element.age}], ageFilter)) {
                        dataAgesFilter.push(element);
                      }
                });
            }

            return dataAgesFilter;
          }


        // Implement filter by experience geographique
        filterByExperienceGeographique(filter: any) {

          const dataExpGeographiqueFilter = [];

          if (filter && filter.length > 0) {
              const expGeoFilter = this.data.getDataByKey('exp_geographique', filter);
              this.defaultConsultantsFilter.forEach(element => {
                    if (this.data.isIdinarray(element.experience_geographique_users, expGeoFilter)) {
                      dataExpGeographiqueFilter.push(element);
                    }
              });
          }

          return dataExpGeographiqueFilter;
        }

        // Implement filter by experience professionnelle
        filterByExperienceProfessionnelle(filter: any) {

                const dataExpProfessionnelleFilter = [];
                if (filter && filter.length > 0) {
                    const expProFilter = this.data.getDataByKey('exp_professionel', filter);
                    this.defaultConsultantsFilter.forEach(element => {
                          if (this.data.isIdinarray([{id: element.nbre_annee_d_experience}], expProFilter)) {
                            dataExpProfessionnelleFilter.push(element);
                          }
                    });
                }
                return dataExpProfessionnelleFilter;
        }


     // Implement filter by genre
    filterByGenre(filter: any) {

            const dataGenresFilter = [];

            if (filter && filter.length > 0) {
                const ageFilter = this.data.getDataByKey('genre', filter);
                this.defaultConsultantsFilter.forEach(element => {
                      if (this.data.isIdinarray([{id: element.sexe}], ageFilter)) {
                        dataGenresFilter.push(element);
                      }
                });
            }

            return dataGenresFilter;
    }


   // Implement filter by experience
   filterByExperience(filter: any) {

     const dataExperienceFilter = [];

     if (filter && filter.length > 0) {
         const experienceFilter = this.data.getDataByKey('experience', filter);
         this.defaultConsultantsFilter.forEach(element => {
          if (experienceFilter[0] <= element.nbre_annee_d_experience &&  element.nbre_annee_d_experience  <= experienceFilter[1]) {
            dataExperienceFilter.push(element);
          }
         });
     }

     return dataExperienceFilter;
   }

   // Verify if a consultant is not in the list of consultant

   notIn (consultant: any, consultantsList: any) {

     let found = true, i = 0;

     while (i < consultantsList.length && found) {
       found = true;

       if (consultantsList[i].nom === consultant.nom) {
         found = false;
       }

       i++;
     }

     return found;

   }

   // Filter by keyword
   filterByKeyword(keyword: string, option: string) {
    const dataKeywordsFilter = [];
    let words = '', key = '';
    this.defaultConsultants.forEach(element => {
      words = element.nom + ' ' + element.prenom;
      words = words.toLowerCase();
      key = keyword.trim().toLowerCase();

      switch (option) {

        case 'all':
            if (words.match(key)) {
              dataKeywordsFilter.push(element);
            } else {
              const contenuCvs = element.cv_users || [];
              contenuCvs.forEach(cv => {
                if (cv.contenu_cv) {
                   key = keyword.trim().toLowerCase();
                   words = cv.contenu_cv.toLowerCase();
                      if (words.match(key)) {
                         dataKeywordsFilter.push(element);
                      }
                  }
              });
            }
          break;
        case 'nom_prenom':
            if (words.match(key)) {
              dataKeywordsFilter.push(element);
            }
          break;
        case 'cv':
            const contenuCv = element.cv_users || [];
            contenuCv.forEach(cv => {
              if (cv.contenu_cv) {
                 key = keyword.trim().toLowerCase();
                 words = cv.contenu_cv.toLowerCase();
                    if (words.match(key)) {
                       dataKeywordsFilter.push(element);
                    }
                }
            });
          break;
        default:
            if (words.match(key)) {
              dataKeywordsFilter.push(element);
            } else {
              const contenuCvs = element.cv_users || [];
              contenuCvs.forEach(cv => {
                if (cv.contenu_cv) {
                   key = keyword.trim().toLowerCase();
                   words = cv.contenu_cv.toLowerCase();
                      if (words.match(key)) {
                         dataKeywordsFilter.push(element);
                      }
                  }
              });
            }
        break;
      }

    });

    this.currentsConsultants = this.data.formatArrayToMatrix(dataKeywordsFilter, this.nbItemsByPage);
    localStorage.setItem('consultant-data-session', JSON.stringify(dataKeywordsFilter));
    this.consultants.emit(JSON.stringify(dataKeywordsFilter));
    this.updateActiveList(1);
   }

   // Get message from component
   getDataMessage() {
      this.data.getDataMessage().subscribe(data => {

        if (data && data.key === 'consultant' && data.mode === 'keyword') {
             this.filterByKeyword(data.value, data.option);
        }
      });
   }

   // reset data Message
   resetDataMessage() {
      this.data.getDataMessage().subscribe(message => {
            if (message === 'filter-consultant') {
              if (this.activeConsultants && this.activeConsultants.length === 0) {
                   this.listConsultantsOffline();
              }
            }
      });
   }


  // Pagination with server data
  nextPage(currentPage: number) {
    if ((currentPage + 1) <= this.totalPages) {
       this.currentPage = currentPage + 1;
       // this.listConsultantsByPage(this.currentPage, this.nbItemsByPage);
    }
  }


  previousPage(currentPage: number) {
    if (currentPage > 1) {
       this.currentPage = currentPage - 1;
      //  this.listConsultantsByPage(this.currentPage, this.nbItemsByPage);
    }
  }

  // Update the number of pages
  updateNumberItems(nbItems: number) {
    this.nbItemsByPage = nbItems;
    this.currentsConsultants = this.data.formatArrayToMatrix(this.defaultConsultants, this.nbItemsByPage);
    this.totalPages = this.currentsConsultants.length;
    this.updateActiveList(this.currentPage);
  }


  // get number items by page
  getNumberItems() {
    let i = 5;
    const nbItemsByPage = [];
    while (i < this.nbItems) {
      nbItemsByPage.push(i);
      i *= 2;
    }
     return nbItemsByPage;
  }


  // Pagination with local data

  // update active list
  updateActiveList(id: number) {
    this.zone.run(() => {
      this.numero = id;
      (this.currentsConsultants[id - 1] && this.currentsConsultants[id - 1].length > 0) ?
      this.activeConsultants = this.currentsConsultants[id - 1] :  this.activeConsultants = [];
      this.currentPage = this.numero;
    });
  }

  // Go to previous list
  previousActiveList(currentId: number) {
    if (currentId > 1) {
    const position = currentId - 1 ;
     position > 0 ? this.numero = position : this.numero = currentId ;
     (this.currentsConsultants[this.numero  - 1] && this.currentsConsultants[this.numero - 1].length > 0) ?
      this.activeConsultants = this.currentsConsultants[this.numero - 1] :  this.activeConsultants = [];
      this.currentPage = this.numero;
    }
  }

  // Go to next list
  nextActiveList(currentId: number) {
    if ((currentId + 1) <= this.totalPages) {
    const position = currentId + 1 ;
     position <= this.currentsConsultants.length ? this.numero = position : this.numero = currentId ;
     (this.currentsConsultants[this.numero  - 1] && this.currentsConsultants[this.numero  - 1].length > 0) ?
     this.activeConsultants = this.currentsConsultants[this.numero - 1] :  this.activeConsultants = [];
     this.currentPage = this.numero;
    }
  }




}
