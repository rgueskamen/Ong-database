import { DataService } from './../../providers/data/data.service';
import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { CabinetService } from '../../providers/cabinet/cabinet.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { remote } from 'electron';

@Component({
  selector: 'app-cabinets',
  templateUrl: './cabinets.component.html',
  styleUrls: ['./cabinets.component.scss']
})
export class CabinetsComponent implements OnInit {

  defaultCabinets: any;
  defaultCabinetsFilter: any;
  currentsCabinets: any;
  activeCabinets: any;
  totalPages: number;
  nbItems: number;
  nbItemsByPage: number;
  currentPage: number;
  disponibilites: any;
  juridictions: any;
  filtreCabinets: string[] = [];
  numero: number;
  workingMode: string;
  loading: boolean;
  percentageLoading: number;
  interval: any;
  @Output() cabinets = new EventEmitter();
  userRole: any;

  constructor(
    private cabinetService: CabinetService,
    private api: ApiService,
    private spinner: NgxSpinnerService,
    private route: Router,
    private data: DataService,
    public zone: NgZone
  ) {

    this.defaultCabinets = [];
    this.defaultCabinetsFilter = [];
    this.activeCabinets = [];
    this.currentsCabinets = [];
    this.currentPage = 1;
    this.totalPages = 1;
    this.nbItems = 0;
    this.nbItemsByPage = 15;
    this.disponibilites = [];
    this.juridictions = [];
    this.numero = 1;
    this.loading = false;
    this.percentageLoading = 0;
    this.interval = null;
    this.filtreCabinets.push('noFilter');
    this.filtreCabinets.push('statut');
    this.filtreCabinets.push('domain');
    this.filtreCabinets.push('profil');
    this.filtreCabinets.push('age');
    this.filtreCabinets.push('exp_geographique');
    this.filtreCabinets.push('exp_professionel');
    this.filtreCabinets.push('experience');
    this.workingMode = this.api.getWorkMode() || 'online';
    this.getDataMessage();
    this.userRole = JSON.parse(localStorage.getItem('user-role'));
    this.cabinetService.getMessage().subscribe(data => {
        if (data && data.message === 'new-cabinet' || data.message === 'update-cabinet') {
          this.listCabinets();
          this.getDisponibilites();
          this.getJuridictions();
        }
    });
  }


  ngOnInit() {
    this.listCabinetsOffline();
    this.getDisponibilites();
    this.getJuridictions();
    this.resetDataMessage();
    this.refreshCabinetsData();
  }


  scrollTop() {
    // Hack: Scrolls to top of Page after page view initialized
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }


  // Set the loading
  setLoading() {
    this.interval = setInterval( () => {
       this.percentageLoading += 10;
       if (this.percentageLoading === 100) {
         this.percentageLoading = 0;
       }
     }, 1000);
   }

  // Clear the loading
  clearLoading() {
    this.percentageLoading = 0;
    clearInterval(this.interval );
    for (let i = 0; i < 100; i++) {
      clearInterval(i);
    }
  }

  // Get all disponibilites
  getDisponibilites () {
    this.data.getAllDisponibilities().then((disponibilites: any) => {
      this.disponibilites = disponibilites.disponibilite;
    }).catch(error => {
       this.api.manageServerError(error);
    });
  }

    // Get all disponibilites
    getJuridictions () {
      this.data.getAllJuridictions().then((juridictions: any) => {
        this.juridictions = juridictions.forme_juridique;
      }).catch(error => {
         this.api.manageServerError(error);
      });
    }

  // Get the list of cabinets
  listCabinets() {
       this.cabinetService.listCabinetOrBureauAll(true).then((cabinets: any) => {
        this.loading = false;
        if (cabinets.message === 'success') {

            this.defaultCabinets = cabinets.liste_cabinet;
            this.defaultCabinetsFilter = this.defaultCabinets;
            this.cabinets.emit(JSON.stringify(this.defaultCabinets));
            localStorage.setItem('cabinets-data-session', JSON.stringify(this.defaultCabinets));
            this.currentsCabinets = this.data.formatArrayToMatrix(this.defaultCabinets, this.nbItemsByPage);
            this.updateActiveList(1);
            this.totalPages = this.currentsCabinets.length;
            this.nbItems = this.defaultCabinets.length;
            this.scrollTop();

        }
      }).catch(error => {
        this.loading = false;
        this.api.manageServerError(error);

      });
  }


  // refrsh cabinets data
  refreshCabinetsData() {
    setInterval(() => {
        this.listCabinets();
    }, 600000);
  }

    // Get the list of cabinets offline
    listCabinetsOffline() {

      const listCabinets = localStorage.getItem('data-cabinets-all') ? JSON.parse(localStorage.getItem('data-cabinets-all')) : null;

      if (listCabinets && listCabinets.message === 'success') {
           this.loading = false;
           this.defaultCabinets = listCabinets.liste_cabinet;
           this.defaultCabinetsFilter = this.defaultCabinets;
           this.cabinets.emit(JSON.stringify(this.defaultCabinets));
           this.currentsCabinets = this.data.formatArrayToMatrix(this.defaultCabinets, this.nbItemsByPage);
           this.updateActiveList(1);
           this.totalPages = this.currentsCabinets.length;
           this.nbItems = this.defaultCabinets.length;
           this.scrollTop();

       } else {
          this.loading = false;
          if (this.currentsCabinets && this.currentsCabinets.length === 0) {
              this.loading = true;
              this.listCabinets();
          }
      }
   }

  // Get Forme juridique
  getFormeJuridique(id: number) {
    let resultat = null;
      this.juridictions.forEach(element => {
        if (element.id === id) {
          resultat = element;
        }
      });
    return resultat ? resultat.intitule : '' ;
  }


  // Get Disponibilites
  getDisponibilite(id: number) {
    let resultat = null;
    this.disponibilites.forEach(element => {
      if (element.id === id) {
        resultat = element;
      }
    });
    return resultat ? resultat.intitule : '' ;
  }


  // Open the consultants CV
  openDocuments(cabinet: any) {

    if (cabinet.file_cabinet_bureau_etude && cabinet.file_cabinet_bureau_etude.length > 0) {
      const files = cabinet.file_cabinet_bureau_etude;
      // const key = 'consultant';
     // localStorage.setItem(key, JSON.stringify(files));
     // this.route.navigate([`/dashboard/pdf-viewer/${key}/2`]);

     const url  = 'data:application/pdf;base64, ' + files[0].file_en_base_64;
     window.open(url);
    } else {
        alert('Aucun document enregistré !');
    }

  }


  // show the confirmation dialog
  confirmDialog(cabinet: any, index: number) {
    const dialog = remote.dialog;
    dialog.showMessageBox({
      type: 'none',
      title: 'Confirmation',
      message: 'Voulez-vous supprimer ce cabinet ?',
      buttons: ['Non', 'Oui']
    }, (reponse) => {
          if (reponse === 0) {
            return;
          } else {
            this.deleteCabinet(cabinet, index);
          }
    });
  }

    // Delete the ong
    deleteCabinet(cabinet: any, index: number) {

      this.spinner.show('cabinet', {
        type: 'ball-spin-fade',
        size: 'large',
        bdColor: 'rgba(251,140,0, .8)',
        color: 'white'
      });

      this.cabinetService.deleteCabinet(cabinet.cabinet_bureau_etude_id).then((reponse: any) => {
        this.spinner.hide('cabinet');
        if (reponse && reponse.message === 'success') {
            // send a message with profil data 
           this.cabinetService.sendMessage('delete-cabinet');  
           this.activeCabinets.splice(index, 1);
           this.listCabinets();
        }

      }).catch(error => {
          this.spinner.hide('cabinet');
          this.api.manageServerError(error);
          if (error && error.user_not_found ) {
              alert('Impossible de supprimer ce cabinet. Essayer à nouveau!');
          }

          if (error && error.ong_association_id_not_exist ) {
            alert('Ce cabinet n existe pas dans la Base de Données');
          }
      });

    }

    // Show the detail informations of cabinet
    showCabinet(cabinet: any) {
      const key = 'cabinets';
      localStorage.setItem(key, JSON.stringify(cabinet));
      this.route.navigate([`/dashboard/cabinets/${key}`]);
    }

  // Get the filters params
  cabinetsFilters(event) {

   const filter  = JSON.parse(event);

    // filter by noFiler
    const noFilter = this.data.getDataByKey('noFilter', filter);
    if (noFilter && noFilter.length > 0) {
      const noFilterData = this.noFilter(filter);
      this.defaultCabinetsFilter = noFilterData;
    }
    // filter by statut
    const statut = this.data.getDataByKey('statut', filter);
    if (statut && statut.length > 0) {
      const statutData = this.filterByStatut(filter);
      this.defaultCabinetsFilter = statutData;
    }
  
    // Filter by domain
    const domain = this.data.getDataByKey('domain', filter);
    if (domain && domain.length > 0) {
      const domanainsData = this.filterByDomains(filter);
      this.defaultCabinetsFilter = domanainsData;
    }
 
    // Filter by Profil
    const profil = this.data.getDataByKey('profil', filter);
    if (profil && profil.length > 0) {
      const profilData = this.filterByProfil(filter);
      this.defaultCabinetsFilter = profilData;
    }
 
    // Filter by age
    const age = this.data.getDataByKey('age', filter);
    if (age && age.length > 0) {
      const ageData = this.filterByAge(filter);
      this.defaultCabinetsFilter = ageData;
    }

    // Filter by experience Geographique
    const exp_geographique = this.data.getDataByKey('exp_geographique', filter);
    if (exp_geographique && exp_geographique.length > 0) {
      const expGeoData = this.filterByAge(filter);
      this.defaultCabinetsFilter = expGeoData;
    }

    // Filter by experience profesionnelle
    const exp_professionel = this.data.getDataByKey('exp_professionel', filter);
    if (exp_professionel && exp_professionel.length > 0) {
      const expProData = this.filterByAge(filter);
      this.defaultCabinetsFilter = expProData;
    }

    // Filter by experience
    const experience = this.data.getDataByKey('experience', filter);
    if (experience && experience.length > 0) {
      const experienceData = this.filterByExperience(filter);
      this.defaultCabinetsFilter = experienceData;
    }

   this.cabinets.emit(JSON.stringify(this.defaultCabinetsFilter));
   localStorage.setItem('cabinets-data-session', JSON.stringify(this.defaultCabinetsFilter));
    this.currentsCabinets = this.data.formatArrayToMatrix(this.defaultCabinetsFilter, this.nbItemsByPage);
    this.updateActiveList(1);
  }

    // Implement no filter
    noFilter (filter: any) {
      let datasNoFilter = [];
          if (filter && filter.length > 0) {
            const dataNoFilter = this.data.getDataByKey('noFilter', filter);
            if (dataNoFilter[0] === 1) {
              datasNoFilter = this.defaultCabinets;
            } else {
                datasNoFilter = this.activeCabinets;
            }
          }
          return datasNoFilter;
     }

      // Implement filter by domaines
      filterByStatut(filter: any) {

        const dataStatusFilter = [];
        if (filter && filter.length > 0) {
            const statutFilter = this.data.getDataByKey('statut', filter);
            this.defaultCabinetsFilter.forEach(element => {
                  if (this.data.isIdinarray([element.statut], statutFilter)) {
                    dataStatusFilter.push(element);
                  }
            });
        }
        return dataStatusFilter;

      }


  // Implement filter
  filterByDomains(filter: any) {

    const dataDomaninsFilter = [];

    if (filter && filter.length > 0) {
        const domainFilter = this.data.getDataByKey('domain', filter);
        this.defaultCabinetsFilter.forEach(element => {
              if (this.data.isIdinarray(element.domaine_expertise_users, domainFilter)) {
                 dataDomaninsFilter.push(element);
              }
        });
    }

    return dataDomaninsFilter;

  }

    // Implement filter
    filterByProfil(filter: any) {

      const dataProfilsFilter = [];

      if (filter && filter.length > 0) {
          const profilFilter = this.data.getDataByKey('profil', filter);
          this.defaultCabinetsFilter.forEach(element => {
                if (this.data.isIdinarray(element.profil_users, profilFilter)) {
                  dataProfilsFilter.push(element);
                }
          });
      }

      return dataProfilsFilter;

    }


          // Implement filter by nombre année creation
          filterByAge(filter: any) {

            const dataAgesFilter = [];

            if (filter && filter.length > 0) {
                const ageFilter = this.data.getDataByKey('age', filter);
                const currentDate = new Date();
                let nbreAnneCreation = 0;
                this.defaultCabinetsFilter.forEach(element => {
                       nbreAnneCreation = currentDate.getFullYear() - parseInt(element.annee_creation, 10);
                      if (this.data.isIdinarray([{id: nbreAnneCreation}], ageFilter)) {
                        dataAgesFilter.push(element);
                      }
                });
            }

            return dataAgesFilter;
          }

               // Implement filter by experience geographique
        filterByExperienceGeographique(filter: any) {

          const dataExpGeographiqueFilter = [];

          if (filter && filter.length > 0) {
              const expGeoFilter = this.data.getDataByKey('exp_geographique', filter);
              this.defaultCabinetsFilter.forEach(element => {
                    if (this.data.isIdinarray(element.experience_geographique_users, expGeoFilter)) {
                      dataExpGeographiqueFilter.push(element);
                    }
              });
          }

          return dataExpGeographiqueFilter;
        }

              // Implement filter by experience professionnelle
              filterByExperienceProfessionnelle(filter: any) {

                const dataExpProfessionnelleFilter = [];
                if (filter && filter.length > 0) {
                    const expProFilter = this.data.getDataByKey('exp_professionel', filter);
                    const currentDate = new Date();
                    let anneeExperience = 0;
                    this.defaultCabinetsFilter.forEach(element => {
                        anneeExperience = currentDate.getFullYear() -
                        parseInt(element.annee_creation, 10);
                          if (this.data.isIdinarray([{id: anneeExperience}], expProFilter)) {
                            dataExpProfessionnelleFilter.push(element);
                          }
                    });
                }
                return dataExpProfessionnelleFilter;
          }


      // Implement filter
  filterByExperience(filter: any) {

    const dataExperienceFilter = [];

    if (filter && filter.length > 0) {
        const experienceFilter = this.data.getDataByKey('experience', filter);
        const currentDate = new Date();
        let anneeExperience = 0;
        this.defaultCabinetsFilter.forEach(element => {
          anneeExperience = currentDate.getFullYear() -
          parseInt(element.annee_creation, 10);
              if (experienceFilter[0] <= anneeExperience &&  anneeExperience <= experienceFilter[1]) {
                dataExperienceFilter.push(element);
              }
        });
    }

    return dataExperienceFilter;
  }

  // Verify if a cabinet is not in the list of cabinets

  notIn (cabinet: any, cabinetsList: any) {

    let found = true, i = 0;

    while (i < cabinetsList.length && found) {
      found = true;

      if (cabinetsList[i].nom === cabinet.nom) {
        found = false;
      }

      i++;
    }

    return found;

  }


   // Filter by status
   filterByKeyword(keyword: string, option: string) {
    const dataKeywordsFilter = [];
    let words = '', key = '';
    this.defaultCabinets.forEach(element => {
      words = element.nom + ' ' + element.prenom;
      words = words.toLowerCase();
      key = keyword.trim().toLowerCase();

      switch (option) {

          case 'all':

              if (words.match(key)) {
                dataKeywordsFilter.push(element);
              } else {

                const contenuCv = element.file_cabinet_bureau_etude || [];
                contenuCv.forEach(cv => {
                  if (cv.contenu_cv) {
                     key = keyword.trim().toLowerCase();
                     words = cv.contenu_file.toLowerCase();

                        if (words.match(key)) {
                           dataKeywordsFilter.push(element);
                        }
                    }
                });
              }
            break;

          case 'nom_prenom':

              if (words.match(key)) {
                dataKeywordsFilter.push(element);
              }
            break;

          case 'cv':

              const contenuCvs = element.file_cabinet_bureau_etude || [];
              contenuCvs.forEach(cv => {
                if (cv.contenu_cv) {
                   key = keyword.trim().toLowerCase();
                   words = cv.contenu_file.toLowerCase();

                      if (words.match(key)) {
                         dataKeywordsFilter.push(element);
                      }
                  }
              });
            break;

          default:

              if (words.match(key)) {
                dataKeywordsFilter.push(element);
              } else {
                const contenuCv = element.file_cabinet_bureau_etude || [];
                contenuCv.forEach(cv => {
                  if (cv.contenu_cv) {
                     key = keyword.trim().toLowerCase();
                     words = cv.contenu_file.toLowerCase();

                        if (words.match(key)) {
                           dataKeywordsFilter.push(element);
                        }
                    }
                });
              }
            break;
      }
    });

    this.currentsCabinets = this.data.formatArrayToMatrix(dataKeywordsFilter, this.nbItemsByPage);
    localStorage.setItem('cabinets-data-session', JSON.stringify(dataKeywordsFilter));
    this.cabinets.emit(JSON.stringify(dataKeywordsFilter));
    this.updateActiveList(1);
   }

     // Get message from component
     getDataMessage() {
        this.data.getDataMessage().subscribe(data => {

          if (data && data.key === 'cabinet' && data.mode === 'keyword') {
               this.filterByKeyword(data.value, data.option);
          }
        });
     }


  // Pagination with server data
  nextPage(currentPage: number) {
    if ((currentPage + 1) <= this.totalPages) {
       this.currentPage = currentPage + 1;
       // this.listCabinetsBypage(this.currentPage, this.nbItemsByPage);
    }
  }

  previousPage(currentPage: number) {
    if (currentPage > 1) {
       this.currentPage = currentPage - 1;
       // this.listCabinetsByPage(this.currentPage, this.nbItemsByPage);
    }
  }


  // Update the number of pages
  updateNumberItems(nbItems: number) {
    this.nbItemsByPage = nbItems;
    this.currentsCabinets = this.data.formatArrayToMatrix(this.defaultCabinets, this.nbItemsByPage);
    this.totalPages = this.currentsCabinets.length;
    this.updateActiveList(this.currentPage);
  }


  // get number items by page
  getNumberItems() {
    let i = 5;
    const nbItemsByPage = [];
    while (i < this.nbItems) {
      nbItemsByPage.push(i);
      i *= 2;
    }
     return nbItemsByPage;
  }


  // Pagination with local data

  // update active list
  updateActiveList(id: number) {
    this.zone.run(() => {
      this.numero = id;
      (this.currentsCabinets[id - 1] && this.currentsCabinets[id - 1].length > 0) ?
      this.activeCabinets = this.currentsCabinets[id - 1] :  this.activeCabinets = [];
      this.currentPage = this.numero;

    });
  }

  // Go to previous list
  previousActiveList(currentId: number) {
    if (currentId > 1) {
    const position = currentId - 1 ;
     position > 0 ? this.numero = position : this.numero = currentId ;
     (this.currentsCabinets[this.numero  - 1] && this.currentsCabinets[this.numero - 1].length > 0) ?
      this.activeCabinets = this.currentsCabinets[this.numero - 1] :  this.activeCabinets = [];
      this.currentPage = this.numero;
    }
  }

  // Go to next list
  nextActiveList(currentId: number) {
    if (currentId + 1 <= this.totalPages) {
    const position = currentId + 1 ;
     position <= this.currentsCabinets.length ? this.numero = position : this.numero = currentId ;
     (this.currentsCabinets[this.numero  - 1] && this.currentsCabinets[this.numero  - 1].length > 0) ?
     this.activeCabinets = this.currentsCabinets[this.numero - 1] :  this.activeCabinets = [];
     this.currentPage = this.numero;
    }
  }


     // reset data Message
     resetDataMessage() {
      this.data.getDataMessage().subscribe(message => {
            if (message === 'filter-cabinet') {
              if (this.activeCabinets && this.activeCabinets.length === 0) {
                this.listCabinetsOffline();
              }
            }
      });
   }

}
