import { DataService } from './../../providers/data/data.service';
import { ConsultantService } from './../../providers/consultant/consultant.service';
import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit, Input, Output, EventEmitter, NgZone } from '@angular/core';


interface Filter {
  choice: boolean;
  categorie: string;
  value: any;
  name: string;
  data: any;
}

interface FilterData {
  key: string;
  data: any;
}

@Component({
  selector: 'app-filter-consultant',
  templateUrl: './filter-consultant.component.html',
  styleUrls: ['./filter-consultant.component.scss']
})
export class FilterConsultantComponent implements OnInit {

  noFilter: string;
  filterData: any;
  nbExperienceMin: string;
  nbExperienceMax: string;
  userFilter: FilterData[];
  @Input() active: string[];
  @Output () filters  = new EventEmitter<string>();


  constructor(
    private api: ApiService,
    private data: DataService,
    private consultant: ConsultantService,
    public zone: NgZone
  ) {
     this.noFilter = '';
     this.nbExperienceMin = '0';
     this.nbExperienceMax = '0';
     this.userFilter = [];
     this.userFilter.push({key: 'noFilter', data: []});
     this.userFilter.push({key: 'statut', data: []});
     this.userFilter.push({key: 'domain', data: []});
     this.userFilter.push({key: 'profil', data: []});
     this.userFilter.push({key: 'niveau', data: []});
     this.userFilter.push({key: 'age', data: []});
     this.userFilter.push({key: 'exp_geographique', data: []});
     this.userFilter.push({key: 'exp_professionel', data: []});
     this.userFilter.push({key: 'genre', data: []});
     this.userFilter.push({key: 'experience', data: []});
     this.filterData = [[]];
     this.consultant.getMessage().subscribe(data => {
      if (data && data.message === 'new-consultant' || data.message === 'update-consultant' || data.message === 'delete-consultant') {
        this.getAllConsultantsFilter();
        this.getAllDomaines();
        this.getAllProfil();
        this.getAllStatutl();
      }
     });
  }

  ngOnInit() {
    this.nbExperienceMin = '0';
    this.nbExperienceMax = '0';
    this.getAllConsultantsFilterOffline();
    this.getAllDomaines();
    this.getAllProfil();
    this.getAllStatutl();
    this.refreshFilter();
  }


        // Get the list of domanines
        getAllDomaines() {
          this.data.getAllDomaines().then((domaines: any) => {
          }).catch(error => {
            this.api.manageServerError(error);
          });
        }

         // Get the list of profil
          getAllProfil() {
            this.data.getAllProfils().then((profils: any) => {
            }).catch(error => {
              this.api.manageServerError(error);
            });
          }

           // Get the list of profil
          getAllStatutl() {
            this.data.getAllStatut().then((statut: any) => {
            }).catch(error => {
              this.api.manageServerError(error);
            });
          }

          initFilterData (consultantFilter: any) {

           // console.log(consultantFilter);
            this.filterData[0] = [];
            if (consultantFilter && consultantFilter.liste_statut) {
              consultantFilter.liste_statut.forEach(element => {
                this.zone.run(() => {
                  this.filterData[0].push({choice: false, categorie: 'consultant', value: element.statut.id,
                  name: element.statut.intitule, data: element.nbre_consultant});
                });
          
              });
            }


          //  console.log(this.filterData[0]);
            this.filterData[1] = [];
            if (consultantFilter && consultantFilter.liste_domaine_expertise) {
              consultantFilter.liste_domaine_expertise.forEach(element => {
                this.zone.run(() => {
                  this.filterData[1] .push({choice: false, categorie: 'consultant', value: element.domaine.id,
                  name: element.domaine.intitule, data: element.nbre_consultant});
                });
           
              });
            }


         //   console.log(this.filterData[1]);

            this.filterData[2] = [];
            if (consultantFilter && consultantFilter.liste_profil) {
              consultantFilter.liste_profil.forEach(element => {
                this.zone.run(() => {
                  this.filterData[2].push({choice: false, categorie: 'consultant', value: element.profil.id,
                  name: element.profil.intitule, data: element.nbre_consultant});
                });

              });
            }


         //   console.log(this.filterData[2]);

            this.filterData[3] = [];
            if (consultantFilter && consultantFilter.liste_niveau_formation) {
              consultantFilter.liste_niveau_formation.forEach(element => {
                this.zone.run(() => {
                  this.filterData[3].push({choice: false, categorie: 'consultant', value: element.niveau_formation.id,
                  name: element.niveau_formation.intitule, data: element.nbre_consultant});
                });
        
              });
            }


        //    console.log(this.filterData[3]);

            this.filterData[4] = [];
            if (consultantFilter && consultantFilter.liste_age_consultant) {
              consultantFilter.liste_age_consultant.forEach(element => {
                this.zone.run(() => {
                  this.filterData[4].push({choice: false, categorie: 'consultant', value: element.age,
                  name: element.age > 1 ?  'ans' :  'an', data: element.nbre_consultant});
                });
           
              });
            }

           // console.log(this.filterData[4]);

            this.filterData[5] = [];
            if (consultantFilter && consultantFilter.liste_experience_geographique) {
              consultantFilter.liste_experience_geographique.forEach(element => {
                this.zone.run(() => {
                  this.filterData[5].push({choice: false, categorie: 'consultant', value: element.experience_geographique.id,
                  name: element.experience_geographique.intitule, data: element.nbre_consultant});
                });
              
              });
            }

          //  console.log(this.filterData[5]);

            this.filterData[6] = [];
            if (consultantFilter && consultantFilter.liste_annee_experience_pro_consultant) {
              consultantFilter.liste_annee_experience_pro_consultant.forEach(element => {
                this.zone.run(() => {
                  this.filterData[6].push({choice: false, categorie: 'consultant', value: element.nbre_annee_d_experience,
                  name: element.nbre_annee_d_experience > 1 ?  'ans' :  'an' , data: element.nbre_consultant});
                });
              
              });
            }

         //   console.log(this.filterData[6]);

            this.filterData[7] = [];
            if (consultantFilter && consultantFilter.liste_genre) {
              this.zone.run(() => {
                this.filterData[7].push({choice: false, categorie: 'consultant', value: 'M',
                name: 'Homme', data: consultantFilter.liste_genre[0].hommes});
  
                this.filterData[7].push({choice: false, categorie: 'consultant', value: 'F',
                name: 'Femme', data: consultantFilter.liste_genre[0].femmes});
              });

            }

             //  console.log(this.filterData[7]);

             this.zone.run(() => {
              this.filterData[8] = [0];
              this.filterData[9] = [0, 0];
            });

              localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));

          }

        // Get all consultant filter
        getAllConsultantsFilter() {

          this.consultant.getDomainesConsultants(true).then((consultantFilter: any) => {

            console.log(consultantFilter);

            if (consultantFilter && consultantFilter.message === 'success') {

                this.initFilterData(consultantFilter);
            }
          }).catch(error => {
            this.api.manageServerError(error);
          });
        }

        // Refresh the filter
        refreshFilter() {
          setInterval(() => {
            this.consultant.getDomainesConsultants();
          }, 600000);
        }


       // Get all consultant filter
        getAllConsultantsFilterOffline() {
                  const consultantFilter = localStorage.getItem('data-consultant-filters') ? JSON.parse(localStorage.getItem('data-consultant-filters')) : null;
                  const consultantFilterState = localStorage.getItem('filter-userdata-consultant') ?  JSON.parse(localStorage.getItem('filter-userdata-consultant')): [];
                  if (consultantFilterState && consultantFilterState.length > 0) {
                    this.filterData = consultantFilterState;
                    this.noFilter = consultantFilterState[8][0];
                    this.nbExperienceMin = consultantFilterState[9][0];
                    this.nbExperienceMax = consultantFilterState[9][1];
                  } else if (consultantFilter && consultantFilter.message === 'success') {
                      this.initFilterData(consultantFilter);
                  } else {
                    this.getAllConsultantsFilter();
                  }
         }

        // Update no Filter
        updateNoFilter() {
          this.userFilter[0].data = [this.noFilter ? 1 : 0 ];
          this.filterData[8] = [this.noFilter ? 1 : 0 ];
          localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

        // Update the status
        updateFilterStatus() {
          const status = [];
          this.filterData[0].forEach(statut => {
                if (statut.choice) {
                  status.push(statut.value);
                }
          });
          this.userFilter[1].data = status;
          localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

       // Update the filter domaine
       updateFilterDomaine() {
        const domanins = [];
        this.filterData[1].forEach(domain => {
              if (domain.choice) {
                domanins.push(domain.value);
              }
        });
        this.userFilter[2].data = domanins;
        localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
        this.filters.emit(JSON.stringify(this.userFilter));
       }

        // Update the filter profil
        updateFilterProfil() {
          const profils = [];
          this.filterData[2].forEach(domain => {
            if (domain.choice) {
              profils.push(domain.value);
            }
          });
          this.userFilter[3].data = profils;
          localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

         // Update the filter niveau
         updateFilterNiveau() {
          const niveaux = [];
          this.filterData[3].forEach(domain => {
            if (domain.choice) {
              niveaux.push(domain.value);
            }
          });
          this.userFilter[4].data = niveaux;
          localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
          this.filters.emit(JSON.stringify(this.userFilter));
         }

         // update age
         updateFilterAge() {
          const ages = [];
          this.filterData[4].forEach(age => {
            if (age.choice) {
              ages.push(age.value);
            }
          });
          this.userFilter[5].data = ages;
          localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
          this.filters.emit(JSON.stringify(this.userFilter));
         }

         // update experience geogragrique
         updateFilterExpGeographique() {

          const expGeo = [];
          this.filterData[5].forEach(exp => {
                if (exp.choice) {
                  expGeo.push(exp.value);
                }
          });
          this.userFilter[6].data = expGeo;
          localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
          this.filters.emit(JSON.stringify(this.userFilter));

        }

         // update experience professionnelle
         updateFilterExpProfessionnel() {

          const expPro = [];
          this.filterData[6].forEach(exp => {
                if (exp.choice) {
                  expPro.push(exp.value);
                }
          });
          this.userFilter[7].data = expPro;
          localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
          this.filters.emit(JSON.stringify(this.userFilter));

        }

         // update genre
         updateFilterGenre() {

          const genres = [];
          this.filterData[7].forEach(domain => {
                if (domain.choice) {
                  genres.push(domain.value);
                }
          });
          this.userFilter[8].data = genres;
          localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

         // Update the filter
         updateFilerExperience() {
             this.userFilter[9].data = [this.nbExperienceMin, this.nbExperienceMax];
             this.filterData[9] = this.userFilter[9].data;
             localStorage.setItem('filter-userdata-consultant', JSON.stringify(this.filterData));
              if (this.nbExperienceMin <= this.nbExperienceMax) {
                this.filters.emit(JSON.stringify(this.userFilter));
              }
          }


}
