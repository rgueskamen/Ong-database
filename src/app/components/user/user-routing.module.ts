import { AuthComponent } from './../auth/auth.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'user/login',
    pathMatch: 'full',
}, {
    path: '',
    component: AuthComponent,
    children: [{
        path: 'user/login',
        component: LoginComponent
    },
    {
        path: 'user/register',
        component : RegisterComponent
    }
]
},
{ path: '**', redirectTo: 'user/login' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
