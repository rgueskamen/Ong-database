
import { ApiService } from './../../../providers/api/api.service';
import { UserService } from './../../../providers/user/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  userRoles: any;
  userNotFound: boolean;
  internetError: boolean;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private api: ApiService
  ) {
      this.userRoles = [];
      this.userNotFound = false;
      this.internetError = false;
   }

  // Initialisation du formulaire
  ngOnInit() {
    this.initUserLogin();
  }

  // init the user login form
  initUserLogin() {
      this.formLogin = this.fb.group({
        email_or_phone: ['', Validators.required],
        password: ['', Validators.required]
      });
  }

  // Modification des emails
  updateData() {
    this.userNotFound = false;
    this.internetError = false;
  }

  // Login the user
  loginUser(userData: any) {
    this.userNotFound = false;
    this.internetError = false;

    this.spinner.show('login', {
      type: 'ball-spin-fade',
      size: 'large',
      bdColor: 'rgba(251,140,0, .8)',
      color: 'white'
    });

    this.userService.loginUser(userData).then((reponse: any) => {

      if (reponse && reponse.message === 'success') {
          this.router.navigate(['/dashboard']);
          this.spinner.hide('login');
      } 
    }).catch(error => {

         this.spinner.hide('login');
         if (error && error.error && error.error.user_not_found) {
            this.userNotFound = true;
         }
    });

  }

}
