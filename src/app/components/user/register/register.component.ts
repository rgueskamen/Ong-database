import { ApiService } from './../../../providers/api/api.service';
import { UserService } from './../../../providers/user/user.service';
import { CustomValidators } from 'ng2-validation';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formRegister: FormGroup;
  userRoles: any;
  emailError: boolean;
  phoneError: boolean;
  usernameError: boolean;
  dataError: boolean;
  roleError: boolean;
  internetError: boolean;

  constructor(
    private userService: UserService,
    private api: ApiService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
      this.userRoles = [];
      this.emailError = false;
      this.phoneError = false;
      this.usernameError = false;
      this.dataError = false;
      this.roleError = false;
      this.internetError = false;
   }

  ngOnInit() {
    this.initUserRegister();
    this.getUsersRoles();
  }


  // init the user login form
  initUserRegister() {
    const password = new FormControl('', [Validators.required]);
    const cpassword = new FormControl('', [CustomValidators.equalTo(password)]);

    this.formRegister = this.fb.group({
      email: ['', Validators.compose([Validators.required, CustomValidators.email])],
      phone: ['', Validators.compose([Validators.required, Validators.pattern('^[+0-9]{9,14}$')])],
      username: ['', Validators.required],
      password: password,
      cpassword: cpassword,
      role_id: ['', Validators.required]
    });
}


// Get the users roles
getUsersRoles() {
      this.userService.getAllUserRole().then((users: any) => {
        if (users && users.message === 'success') {
           this.userRoles = users.roles;
        }
      }).catch(error => {
            this.api.manageServerError(error);
      });
}


// update the username error
changeUsername() {
  this.usernameError = false;
  this.internetError = false;
}

// update the role error
updateRole() {
  this.roleError = false;
  this.internetError = false;
}

// Update the phone error
changePhone() {
  this.phoneError = false;
  this.internetError = false;
}

// Upadte the Email error
changeEmail() {
  this.emailError = false;
  this.internetError = false;
}

// Login the user
registerUser(userData: any) {

  this.dataError = false;
  this.internetError = false;

  this.spinner.show('register', {
    type: 'ball-spin-fade',
    size: 'large',
    bdColor: 'rgba(251,140,0, .8)',
    color: 'white'
  });

  this.userService.registerUser(userData).then((reponse: any) => {

    if (reponse && reponse.message === 'success') {
        this.router.navigate(['/dashboard']);
        this.spinner.hide('register');
    } else if (reponse && reponse.message === 'offline') {
        this.internetError = true;
        this.spinner.hide('register');
    } else {
      this.spinner.hide('register');
    }

  }).catch(error => {

      this.spinner.hide('register');

          if (error.error && error.error.email_exist_already) {
            this.emailError = true;
          }

          if (error.error && error.error.phone_exist_already) {
            this.phoneError = true;
          }

          if (error.error && error.error.username_exist_already) {
            this.usernameError = true;
          }

          if (error.error && error.error.parametre_found) {
            this.dataError = true;
          }

          if (error.error && error.error.role_id_not_exist) {
            this.roleError = true;
          }

  });

}

}
