import { ApiService } from './../../../providers/api/api.service';
import { UserService } from './../../../providers/user/user.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { DataService } from '../../../providers/data/data.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  loading: boolean;
  activeUsers: any;
  currentUser: any;
  usersList: any;
  numero: number;
  userRoles: any;
  nbItemsByPage: number;
  currentPage: number;
  totalPages: number;
  roleId: number;
  userSelected: any;
  showRoleForm: boolean;
  activeIndex: number;
  nbItems: number;
  userData: any;

  constructor(
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private data: DataService,
    private api: ApiService,
    public zone: NgZone
  ) {
    this.loading = false;
    this.activeUsers = [];
    this.currentUser = [];
    this.usersList = [];
    this.numero = 1;
    this.userRoles = [];
    this.nbItemsByPage = 10;
    this.currentPage = 1;
    this.totalPages = 1;
    this.roleId = 0;
    this.activeIndex = 0;
    this.nbItems = 0;
    this.userSelected = {};
    this.showRoleForm = false;
    this.userData = localStorage.getItem('user-role') ? JSON.parse(localStorage.getItem('user-role')) : null;
    this.userService.getMessage().subscribe(data => {
        if (data && data.message === 'update-user') {
            this.getUsersList();
        }
    });
  }

  ngOnInit() {
    this.getUsersRoles();
    this.getUsersListOffline();
    this.refreshUsersData();
  }

  // Get the list of users online
  getUsersList() {
    this.userService.getUsers(true).then((users: any) => {
     this.loading = false;
     if (users.message === 'success') {
         this.usersList = users.users;
         this.currentUser = this.data.formatArrayToMatrix(this.usersList, this.nbItemsByPage);
         this.updateActiveList(1);
         this.totalPages = this.currentUser.length;
         this.nbItems = this.usersList.length;
     }
   }).catch(error => {
     this.loading = false;
     this.api.manageServerError(error);

   });

  }


  // refrsh cabinets data
  refreshUsersData() {
    setInterval(() => {
        this.getUsersList();
    }, 600000);
  }

    // Get the list of users offline
    getUsersListOffline() {

      this.loading = true;
      const usersList = localStorage.getItem('user-list') ? JSON.parse(localStorage.getItem('user-list')) : null;

       if (usersList && usersList.message === 'success') {
           this.loading = false;
           this.usersList = usersList.users;
           this.currentUser = this.data.formatArrayToMatrix(this.usersList, this.nbItemsByPage);
           this.updateActiveList(1);
           this.totalPages = this.currentUser.length;
           this.nbItems = this.usersList.length;
       } else {
        this.loading = false;
        if (this.currentUser &&  this.currentUser.length === 0) {
            this.loading = true;
            this.getUsersList();
        }
       }
    }

// Get the users roles
getUsersRoles() {
  this.userService.getAllUserRole().then((users: any) => {
    if (users && users.message === 'success') {
       this.userRoles = users.roles;
    }
  }).catch(error => {
        this.api.manageServerError(error);
  });
}

// Disable a user
disableUser(user: any, index: number) {

  this.spinner.show('user-list', {
    type: 'ball-spin-fade',
    size: 'large',
    bdColor: 'rgba(251,140,0, .8)',
    color: 'white'
  });

  this.userService.disableUser(user.id).then((reponse: any) => {
        if (reponse && reponse.message === 'success') {
          this.activeUsers.splice(index, 1);
          this.getUsersList();
          this.spinner.hide('user-list');
          this.userService.sendMessage('update-user');
        }
  }).catch(error => {
    this.spinner.hide('user-list');
    this.api.manageServerError(error);
    if (error.error && error.error.user_not_exist) {
        alert('L\'utilisateur n\'existe pas');
    }

  });
}


// Disable a user
enableUser(user: any, index: number) {

  this.spinner.show('user-list', {
    type: 'ball-spin-fade',
    size: 'large',
    bdColor: 'rgba(251,140,0, .8)',
    color: 'white'
  });

this.userService.activateUser(user.id).then((reponse: any) => {
    if (reponse && reponse.message === 'success') {
      this.activeUsers[index].active = 1;
      this.getUsersList();
      this.spinner.hide('user-list');
      this.userService.sendMessage('update-user');
    }
}).catch(error => {
    this.spinner.hide('user-list');
    this.api.manageServerError(error);
    if (error.error && error.error.user_not_exist) {
        alert('L\'utilisateur n\'existe pas');
    }
});

}

// Show the update role form
updateRole(user: any, index: number) {
  this.userSelected = user;
  this.activeIndex = index;
  this.showRoleForm = true;
}

// cancel
updateRoleCancel() {
  this.userSelected = {};
  this.activeIndex = 0;
  this.showRoleForm = false;
}

// update the user  role
saveRole() {

  this.spinner.show('user-list', {
    type: 'ball-spin-fade',
    size: 'large',
    bdColor: 'rgba(251,140,0, .8)',
    color: 'white'
  });

  this.showRoleForm = false;
  const data = {user_id: this.userSelected.id, role_id: this.roleId};

  this.userService.updateUserRole(data).then((reponse: any) => {
    if (reponse && reponse.message === 'success') {
      this.activeUsers[this.activeIndex].active = 1;
      this.getUsersList();
      this.spinner.hide('user-list');
      this.userService.sendMessage('update-user');
    }
}).catch(error => {
this.spinner.hide('user-list');
this.api.manageServerError(error);
if (error.error  && error.error.user_not_exist) {
    alert('L\'utilisateur n\'existe pas');
}

if (error.error && error.error.role_id_not_exist) {
  alert('Le rôle n\'existe pas');
}

});

}

  // disable OR enable user
  userLock(user: any, index: number) {

    switch (user.active) {

      case 0 :
          this.enableUser(user, index);
      break;

      case 1 :
        this.disableUser(user, index);
      break;

      default:
      break;
    }

  }

  // Get the user role
  getUserRole(role_id: number) {

    const userRoles = localStorage.getItem('user-roles') ?  JSON.parse(localStorage.getItem('user-roles')) : {roles: []};
    let  role = '';
    userRoles.roles.forEach(userRole => {
      if (userRole.id === role_id) {
        role = userRole.title;
      }
    });

    return role;
  }

  // Update the number of pages
  updateNumberItems(nbItems: number) {
    this.nbItemsByPage = nbItems;
    this.currentUser = this.data.formatArrayToMatrix(this.usersList, this.nbItemsByPage);
    this.totalPages = this.currentUser.length;
    this.updateActiveList(this.currentPage);
  }


  // get number items by page
  getNumberItems() {
    let i = 5;
    const nbItemsByPage = [];
    while (i < this.nbItems) {
      nbItemsByPage.push(i);
      i*=2;
    }
     return nbItemsByPage;
  }

  // update active list
  updateActiveList(id: number) {
    this.zone.run(() => {
      this.numero = id;
      (this.currentUser[id - 1] && this.currentUser[id - 1].length > 0) ?
      this.activeUsers = this.currentUser[id - 1] :  this.activeUsers = [];
      this.currentPage = this.numero;
    });
  }

  // Go to previous list
  previousActiveList(currentId: number) {
    if (currentId > 1) {
    const position = currentId - 1 ;
     position > 0 ? this.numero = position : this.numero = currentId ;
     (this.currentUser[this.numero  - 1] && this.currentUser[this.numero - 1].length > 0) ?
      this.activeUsers = this.currentUser[this.numero - 1] :  this.activeUsers = [];
      this.currentPage = this.numero;
    }
  }

  // Go to next list
  nextActiveList(currentId: number) {
    if (currentId + 1 <= this.totalPages) {
    const position = currentId + 1 ;
     position <= this.currentUser.length ? this.numero = position : this.numero = currentId ;
     (this.currentUser[this.numero  - 1] && this.currentUser[this.numero  - 1].length > 0) ?
     this.activeUsers = this.currentUser[this.numero - 1] :  this.activeUsers = [];
     this.currentPage = this.numero;
    }
  }

}
