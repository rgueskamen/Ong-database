import { ApiService } from './../../../providers/api/api.service';
import { UserService } from './../../../providers/user/user.service';
import { CustomValidators } from 'ng2-validation';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {


  formUpdateUser: FormGroup;
  userRoles: any;
  emailError: boolean;
  phoneError: boolean;
  usernameError: boolean;
  dataError: boolean;
  roleError: boolean;
  internetError: boolean;
  updateSuccess: boolean;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private api: ApiService
  ) {
      this.userRoles = [];
      this.emailError = false;
      this.phoneError = false;
      this.usernameError = false;
      this.dataError = false;
      this.roleError = false;
      this.internetError = false;
      this.updateSuccess = false;
   }

  // Initialisation du formulaire
  ngOnInit() {
    this.getUsersRoles();
    this.initUserUpdateUser();
    this.checkSession();
  }


  checkSession() {
    if (this.userService.getSession()) {
      this.api.refreshAccesToken();
    }
  }

  // Logout the session
  logout() {
    localStorage.removeItem('user-data');
    localStorage.removeItem('user-pass');
    localStorage.removeItem('user-roles');
    localStorage.removeItem('user-role');
    localStorage.removeItem('user-token');
    localStorage.removeItem('user-list');
    localStorage.removeItem('workMode');
    localStorage.removeItem(`data-cabinets-all`);
    localStorage.removeItem(`data-cabinets-filters`);
    localStorage.removeItem(`data-cabinets-statistiques`);
    localStorage.removeItem('data-consultant-all');
    localStorage.removeItem('data-consultant-filters');
    localStorage.removeItem('data-consultant-statistique');
    localStorage.removeItem('ambero-status');
    localStorage.removeItem('ambero-disponibilites');
    localStorage.removeItem('ambero-recommandations');
    localStorage.removeItem('ambero-nivFormations');
    localStorage.removeItem('ambero-allProfil');
    localStorage.removeItem('ambero-expgeographique');
    localStorage.removeItem('ambero-domaines');
    localStorage.removeItem('ambero-juridictions');
    localStorage.removeItem(`data-ong-all`);
    localStorage.removeItem(`data-ong-filters`);
    localStorage.removeItem(`data-ong-statistiques`);
    localStorage.clear();
    this.router.navigate(['/session']);
  }

  // init the user login form
  initUserUpdateUser() {
      const user = localStorage.getItem('user-data') ? JSON.parse(localStorage.getItem('user-data')) : null;
      const pass = localStorage.getItem('user-pass') ? JSON.parse(localStorage.getItem('user-pass')) : null;

      const password = new FormControl(pass ? pass : '', [Validators.required]);
      const cpassword = new FormControl(pass ? pass : '', [CustomValidators.equalTo(password)]);

      this.formUpdateUser = this.fb.group({
        email: [user ? user.email : '', Validators.compose([Validators.required, CustomValidators.email])],
        phone: [user ? user.phone : '', Validators.compose([Validators.required, Validators.pattern('^[+0-9]{9,14}$')])],
        username: [user ? user.username : '', Validators.required],
        password: password,
        cpassword: cpassword,
        role_id: [user ? user.role_id : '']
      });
  }

  // update the username error
changeUsername() {
  this.usernameError = false;
  this.internetError = false;
}

// update the role error
updateRole() {
  this.roleError = false;
  this.internetError = false;
}

// Update the phone error
changePhone() {
  this.phoneError = false;
  this.internetError = false;
}

// Upadte the Email error
changeEmail() {
  this.emailError = false;
  this.internetError = false;
}

  // Get the users roles
  getUsersRoles() {
        this.userService.getAllUserRole().then((users: any) => {
          if (users && users.message === 'success') {
             this.userRoles = users.roles;
          }
        }).catch(error => {
              this.api.manageServerError(error);
        });
  }


  // Login the user
  updateUser(userData: any) {

    this.internetError = false;
    this.dataError = false;

    this.spinner.show('update-user', {
      type: 'ball-spin-fade',
      size: 'large',
      bdColor: 'rgba(251,140,0, .8)',
      color: 'white'
    });

    this.userService.updateUser(userData).then((reponse: any) => {

      if (reponse && reponse.message === 'success') {

        const param = {
          email_or_phone: userData.email || userData.phone,
          password: userData.password,
        };
         this.userService.loginUser(param);
          this.router.navigate(['/profil']);
          this.spinner.hide('update-user');
          this.userService.sendMessage('update-user');
          this.updateSuccess = true;
          setTimeout(() => {
            this.updateSuccess = false;
          }, 5000);
      }

    }).catch(error => {
        this.spinner.hide('update-user');
        this.api.manageServerError(error);

          if (error.error && error.error.email_exist_already) {
            this.emailError = true;
          }

          if (error.error && error.error.phone_exist_already) {
            this.phoneError = true;
          }

          if (error.error && error.error.username_exist_already) {
            this.usernameError = true;
          }

          if (error.error && error.error.parametre_found) {
            this.dataError = true;
          }

          if (error.error && error.error.role_id_not_exist) {
            this.roleError = true;
          }

    });

  }

}
