import { ApiService } from './../../../providers/api/api.service';
import { UserService } from './../../../providers/user/user.service';
import { CustomValidators } from 'ng2-validation';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  formAddUser: FormGroup;
  userRoles: any;
  emailError: boolean;
  phoneError: boolean;
  usernameError: boolean;
  dataError: boolean;
  roleError: boolean;
  internetError: boolean;
  addSucced: boolean;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private api: ApiService
  ) {
      this.userRoles = [];
      this.emailError = false;
      this.phoneError = false;
      this.usernameError = false;
      this.dataError = false;
      this.roleError = false;
      this.internetError = false;
      this.addSucced = false;
   }

  // Initialisation du formulaire
  ngOnInit() {
    this.getUsersRoles();
    this.initUserAddUser();
    this.checkSession();
  }

  checkSession() {
    if (this.userService.getSession()) {
      this.api.refreshAccesToken();
    }
  }

  // init the user login form
  initUserAddUser() {
      const password = new FormControl('', [Validators.required]);
      const cpassword = new FormControl('', [CustomValidators.equalTo(password)]);

      this.formAddUser = this.fb.group({
        email: ['', Validators.compose([Validators.required, CustomValidators.email])],
        phone: ['', Validators.compose([Validators.required, Validators.pattern('^[+0-9]{9,14}$')])],
        username: ['', Validators.required],
        password: password,
        cpassword: cpassword,
        role_id: ['', Validators.required]
      });
  }


  // Get the users roles
  getUsersRoles() {
        this.userService.getAllUserRole().then((users: any) => {
          if (users && users.message === 'success') {
             this.userRoles = users.roles;
          }
        }).catch(error => {
              this.api.manageServerError(error);
        });
  }

  // update the username error
changeUsername() {
  this.usernameError = false;
  this.internetError = false;
}

// update the role error
updateRole() {
  this.roleError = false;
  this.internetError = false;
}

// Update the phone error
changePhone() {
  this.phoneError = false;
  this.internetError = false;
}

// Upadte the Email error
changeEmail() {
  this.emailError = false;
  this.internetError = false;
}


  // Login the user
  addUser(userData: any) {
    this.internetError = false;
    this.dataError = false;

    this.spinner.show('add-user', {
      type: 'ball-spin-fade',
      size: 'large',
      bdColor: 'rgba(251,140,0, .8)',
      color: 'white'
    });

    this.userService.addUser(userData).then((reponse: any) => {

      if (reponse && reponse.message === 'success') {
          this.initUserAddUser();
          this.userService.sendMessage('update-user');
          this.userService.getUsers();
          this.addSucced = true;
          setTimeout(() => {
            this.addSucced = false;
          }, 5000);
          this.spinner.hide('add-user');
      }

    }).catch(error => {
        this.spinner.hide('add-user');
     

        if (error && error.error) {
          if (error.error && error.error.email_exist_already) {
            this.emailError = true;
          }

          if (error.error && error.error.phone_exist_already) {
            this.phoneError = true;
          }

          if (error.error && error.error.username_exist_already) {
            this.usernameError = true;
          }

          if (error.error && error.error.parametre_found) {
            this.dataError = true;
          }

          if (error.error && error.error.role_id_not_exist) {
            this.roleError = true;
          }
        }
        this.api.manageServerError(error);

    });

  }

}
