import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from './../../providers/api/api.service';
import { CabinetService } from './../../providers/cabinet/cabinet.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../providers/data/data.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CustomValidators } from 'ng2-validation';
import { remote } from 'electron';
import { UserService } from '../../providers/user/user.service';


interface Domaine {
  domaine_id: number;
  nbre_consultation: number;
  nomDomaine: string;
}

interface Documents {
  file: string;
  filemime: string;
  filename: string;
}

@Component({
  selector: 'app-add-cabinet',
  templateUrl: './add-cabinet.component.html',
  styleUrls: ['./add-cabinet.component.scss']
})
export class AddCabinetComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});

  domainesList: any;
  formCabinets: FormGroup;
  experienceList: any;
  disponibilites: any;
  statutList: any;
  profilList: any;
  domaine: string;
  formationsList: any;
  listCvs: Documents[];
  listDomaines: Domaine[];
  nbreConsultant: number;
  juridictions: any;
  recommandationsList: any;
  fillallfieldError: boolean;
  statutIdError: boolean;
  disponibilyError: boolean;
  recommandationError: boolean;
  formeJuridiqueError: boolean;
  profilError: boolean;
  pdfAddError: boolean;
  experienceError: boolean;
  domainError: boolean;
  doctypeError: boolean;
  bottomup: boolean;
  @Output () cancelled = new EventEmitter<string>();

  constructor(
    private api: ApiService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private route: Router,
    private cabinetService: CabinetService,
    private userService : UserService,
    private data: DataService
  ) {

    this.domainesList = [];
    this.experienceList = [];
    this.profilList = [];
    this.formationsList = [];
    this.recommandationsList = [];
    this.statutList = [];
    this.disponibilites = [];
    this.juridictions = [];
    this.listDomaines = [];
    this.listCvs = [];
    this.domaine = '';
    this.nbreConsultant = 0;
    this.fillallfieldError = false;
    this.statutIdError = false;
    this.disponibilyError = false;
    this.recommandationError = false;
    this.formeJuridiqueError = false;
    this.profilError = false;
    this.experienceError = false;
    this.domainError = false;
    this.pdfAddError = false;
    this.doctypeError = false;
    this.bottomup = false;
    this.getNewProfilOrDomain();
  }

  ngOnInit() {

    this.getAllDomaines();
    this.getAllExperiences();
    this.getAllProfil();
    this.getAllRecommandations();
    this.initCabinetForm();
    this.getAllStatus();
    this.getDisponibilites();
    this.getJuridictions();
    this.checkSession();

  }




      // permet d'ajouter les cabinets
      initCabinetForm() {
        const currentDate = new Date();
        const currentDateFormat = this.formatDate1(currentDate);
        this.formCabinets = this.fb.group({
          statut_id: [null],
          disponibilite_id: [null],
          recommandation_id: [null],
          recommandation_name: [null],
          nom: [null],
          sigle: [null],
          forme_juridique_id: [null],
          annee_creation_view: [null, Validators.compose([CustomValidators.maxDate(currentDateFormat)])],
          annee_creation: [null],
          registre_commerce: [null],
          siege_social: [null],
          liste_profil_view: [null],
          liste_profil: [null],
          date1ereExperiencePro_view: [null],
          date1ereAnneeExperienceEnTantQueCabinet_view: [null],
          date1ereExperiencePro: [null],
          date1ereAnneeExperienceEnTantQueCabinet: [null],
          liste_experience_geographique_view: [null],
          liste_experience_geographique: [null],
          experienceAvecCooperationAllemande_view: [null],
          experienceAvecLesNationsUnies_view: [null],
          experienceAvecLesAutresPartenaires_view: [null],
          experienceAvecCooperationAllemande: [null],
          experienceAvecLesNationsUnies: [null],
          experienceAvecLesAutresPartenaires: [null],
          liste_domaine_expertise: [[]],
          adresse: [null],
          liste_telephone_view: [null],
          liste_telephone: [null],
          liste_courriel_view: [null],
          liste_courriel: [null],
          nom_principal_dirigeant: [null],
          prenom_principal_dirigeant: [null],
          telephone_principal_dirigeant: [null],
          courriel_principal_dirigeant: [null],
          liste_files: [null]
        });
     }

 // Back button
  backButton() {
    this.cancelled.emit('cancel');
  }

  checkSession() {
    if (this.userService.getSession()) {
      this.api.refreshAccesToken();
    }
  }


  // check if date is valid
  checkDate(fisrtDate: string, secondDate: string) {
    return (group: FormGroup) => {
      const fisrtDateInput = group.controls[fisrtDate];
      const secondDateInput = group.controls[secondDate];
      if (secondDateInput.value <= fisrtDateInput.value) {
        return secondDateInput.setErrors({notMatch: true});
      } else {
        return secondDateInput.setErrors(null);
      }
    };
  }


      // Cancel  the page
      cancel () {
          this.initCabinetForm();
      }

      // Scroll on the top
    scrollTop() {
      // Hack: Scrolls to top of Page after page view initialized
      let top = document.getElementById('top');
      this.bottomup = true;
      if (top !== null) {
        top.scrollIntoView();
        top = null;
      }

      // animation duration
      setTimeout(() => {
        this.bottomup = false;
      }, 5000);
    }


          // show the confirmation dialog cancel
     confirmDialogCancel() {
      const dialog = remote.dialog;
      dialog.showMessageBox({
        type: 'none',
        title: 'Confirmation',
        message: 'Voulez-vous annuler l\'ajout du cabinet ?',
        buttons: ['Non', 'Oui']
      }, (reponse) => {
            if (reponse === 0) {
              return;
            } else {
              this.initCabinetForm();
            }
      });
    }

      // Get the list of status
      getAllStatus() {
          this.data.getAllStatut().then((status: any) => {

            if (status.message === 'success') {
              this.statutList = status.statut;
            }
          }).catch(error => {
            this.api.manageServerError(error);
          });
      }

      // Listen to messaga
      getNewProfilOrDomain() {
          this.data.getDataMessage().subscribe(message => {

              if (message && message.message) {

                  if (message.message === 'profil') {

                    this.profilList.push(message.data);

                  } else if (message.message === 'domain') {

                    this.domainesList.push(message.data);

                  }

              }

          });
      }


    // Get the list of domanines
    getAllDomaines() {
      this.data.getAllDomaines().then((domaines: any) => {

        if (domaines.message === 'success') {
          this.domainesList = domaines.domaines;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }


    // Get the list of experiences
    getAllExperiences() {
      this.data.getAllGeographiqueExperience().then((experiences: any) => {
        if (experiences.message === 'success') {
          this.experienceList = experiences.experience_geographique;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }


   // Get the list of profil
    getAllProfil() {
        this.data.getAllProfils().then((profils: any) => {
          if (profils.message === 'success') {
            this.profilList =  profils.profil;
          }
        }).catch(error => {
          this.api.manageServerError(error);
        });
     }


  // Get the list of recommandations
  getAllRecommandations() {
          this.data.getAllRecommandations().then((recommandations: any) => {

            if (recommandations.message === 'success') {
              this.recommandationsList = recommandations.recommandation;
            }

          }).catch(error => {
            this.api.manageServerError(error);
          });
   }

   // Get all disponibilites
   getDisponibilites () {
    this.data.getAllDisponibilities().then((disponibilites: any) => {
      this.disponibilites = disponibilites.disponibilite;
    }).catch(error => {
       this.api.manageServerError(error);
    });
  }

    // Get all disponibilites
    getJuridictions () {
      this.data.getAllJuridictions().then((juridictions: any) => {
        this.juridictions = juridictions.forme_juridique;
      }).catch(error => {
         this.api.manageServerError(error);
      });
    }


       // add un domaine
       addDomaine() {
         this.listDomaines.push({domaine_id: parseInt( this.domainesList[this.domaine].id, 10), nbre_consultation : this.nbreConsultant,
         nomDomaine: this.domainesList[this.domaine].intitule });
         this.formCabinets.controls['liste_domaine_expertise'].setValue(this.listDomaines);
         this.domaine = '';
         this.nbreConsultant = 0;
      }

      // Remove the selected domaine
     removeDomaine(index: number) {
       this.listDomaines.splice(index, 1);
     }

     // format liste profil
     formatProfil(profil: string) {

         let listProfil: string[] = [];
         const resultat = [];

         if (profil) {

           listProfil = profil.split(',');
           listProfil.forEach(profilId => {
               if (profilId) {
                 resultat.push({profil_id: profilId});
               }
           });

         }

         this.formCabinets.controls['liste_profil'].setValue(resultat);
     }

     // format liste experience
     formatExperience(experience: string) {

         let listExperience: string[] = [];
         const resultat = [];

         if (experience) {

           listExperience = experience.split(',');
           listExperience.forEach(experienceId => {
               if (experienceId) {
                 resultat.push({experience_geographique_id: experienceId});
               }
           });
         }

         this.formCabinets.controls['liste_experience_geographique'].setValue(resultat);
      }


      // format phone
     formatPhone(phone: string) {

       let listPhones: string[] = [];
       const resultat = [];

       if (phone) {

         listPhones = phone.split(',');
         listPhones.forEach(phoneData => {
             if (phoneData) {
               resultat.push({telephone: phoneData});
             }
         });

       }

       this.formCabinets.controls['liste_telephone'].setValue(resultat);
    }


    // format phone
    formatCouriel(couriels: string) {
      let listCouriel: string[] = [];
      const resultat = [];
       if (couriels) {
         listCouriel = couriels.split(',');
         listCouriel.forEach(couriel => {
           if (couriel) {
             resultat.push({email: couriel});
           }
          });
        }
        this.formCabinets.controls['liste_courriel'].setValue(resultat);
     }

       // Format date  aaaa-mm-jj
       formatDate1(date: any) {
        const dateFormat  = new Date(date);
        const month  = (dateFormat.getMonth() + 1) < 10 ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
        const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
        return dateFormat.getFullYear() + '-' + month + '-' + day;
      }


      // Format date aaaa
      formatDate2(date: any) {
        const dateFormat  = new Date(date);
        return dateFormat.getFullYear();
      }


    // Add a cv
     addDocument(docdata: any) {
       this.listCvs.push({file: docdata.data, filemime: docdata.filemime, filename: docdata.filename});
       this.formCabinets.controls['liste_files'].setValue(this.listCvs);
     }

     // remove cv
     removeDocument(index: number) {
       this.listCvs.splice(index, 1);
       this.uploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});
     }

  // Update upload the pdf
 onFilePdf(event) {
   this.pdfAddError = false;
   this.doctypeError = false;
   this.data.pdfReader(event).then(pdf => {
    if (pdf && pdf.filemime === 'application/pdf') {
      this.addDocument(pdf);
    } else {
        // alert('Veuillez choisir un document au format PDF');
        this.doctypeError = true;
    }
   }).catch(error => {
     this.pdfAddError = true;
   });
  }


     // add a consultant
     addCabinet(cabinetData: any) {

      this.spinner.show('cabinet', {
        type: 'ball-spin-fade',
        size: 'large',
        bdColor: 'rgba(251,140,0, .8)',
        color: 'white'
      });

      this.formCabinets.get('recommandation_id').setValue(this.formCabinets.value.recommandation_id ? this.formCabinets.value.recommandation_id : 'NULL');
      this.formCabinets.get('recommandation_name').setValue(this.formCabinets.value.recommandation_name ? this.formCabinets.value.recommandation_name : 'NULL');

       const data = cabinetData;

       data.experienceAvecCooperationAllemande_view ? this.formCabinets.controls['experienceAvecCooperationAllemande'].setValue(1)
       : this.formCabinets.controls['experienceAvecCooperationAllemande'].setValue(0);

       data.experienceAvecLesNationsUnies_view ? this.formCabinets.controls['experienceAvecLesNationsUnies'].setValue(1)
       : this.formCabinets.controls['experienceAvecLesNationsUnies'].setValue(0);

       data.experienceAvecLesNationsUnies_view ? this.formCabinets.controls['experienceAvecLesAutresPartenaires'].setValue(1)
       : this.formCabinets.controls['experienceAvecLesAutresPartenaires'].setValue(0);

       this.formatProfil(data.liste_profil_view);
       this.formatExperience(data.liste_experience_geographique_view);
       this.formatPhone(data.liste_telephone_view);
       this.formatCouriel(data.liste_courriel_view);

       if (data.annee_creation_view) {
        this.formCabinets.controls['annee_creation'].setValue(this.formatDate2(data.annee_creation_view));
       }

       if (data.date1ereExperiencePro_view) {
        this.formCabinets.controls['date1ereExperiencePro'].setValue(this.formatDate1(data.date1ereExperiencePro_view));
       }

       if (data.date1ereAnneeExperienceEnTantQueCabinet_view) {
        this.formCabinets.controls['date1ereAnneeExperienceEnTantQueCabinet'].setValue(
          this.formatDate1(data.date1ereAnneeExperienceEnTantQueCabinet_view));
       }
      
       this.cabinetService.saveCabinetOrBureau(this.formCabinets.value).then((response: any) => {
             if (response && response.message === 'success') {
                   this.scrollTop();
                   this.cabinetService.sendMessage('new-cabinet');
                   setTimeout(() => {
                    this.spinner.hide('cabinet');
                    this.route.navigateByUrl('/dashboard/dashboard-v1/2');
                  },15000);
             }
       }).catch(error => {
                this.spinner.hide('cabinet');
                alert('Essayer encore svp !');

               if (error && error.message === 'error') {
                 if (error.remplir_tous_les_champs) {
                      // Veuillez remplir tous les champs
                      this.fillallfieldError = true;
                 }
                 if (error.statut_id_not_exist) {
                   // Ce statut n'existe pas
                   this.statutIdError = true;
                 }
                 if (error.disponibilite_id_not_exist) {
                   // cette disponibilité n'existe pas
                   this.disponibilyError = true;
                 }
                 if (error.recommandation_id_not_exist) {
                   // Cette recommandation n'existe pas
                   this.recommandationError = true;
                 }
                 if (error.forme_juridique_id_not_exist) {
                    // Cette forme juridique n'existe pas
                    this.formeJuridiqueError = true;
                 }
                 if (error.profil_id_not_exist) {
                     // Ce profil n'existe pas
                     this.profilError = true;
                 }
                 if (error.experience_geographique_id_not_exist) {
                     // Cette experience n'existe pas
                     this.experienceError = false;
                 }
                 if (error.domaine_id_not_exist) {
                   // Ce domaine n'existe pas
                   this.domainError = false;
                 }
               } else {
                  this.api.manageServerError(error);
               }
       });

     }

}
