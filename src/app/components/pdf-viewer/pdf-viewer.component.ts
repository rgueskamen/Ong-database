import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.scss']
})
export class PdfViewerComponent implements OnInit {

  pdfdataUrl: string;
  index: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiService
  ) {

  }

  ngOnInit() {

    const key  = this.route.snapshot.params.key;
    this.index = this.route.snapshot.params.index || 1;
    const data = JSON.parse(localStorage.getItem(key));
    this.pdfdataUrl = 'data:application/pdf;base64,' + data[0].cv_en_base_64;
  }


     // Back to the dashboard
     backButton() {
      this.router.navigateByUrl(`/dashboard/dashboard-v1/${this.index}`);
    }



}
