import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { FileUploadModule } from 'ng2-file-upload';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  imports: SharedModule.MODULE_LIST,
  exports: SharedModule.MODULE_LIST
})
export class SharedModule {

  static readonly MODULE_LIST = [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    FileUploadModule,
    NgxSpinnerModule
];

}
