import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { OngService } from '../../providers/ong/ong.service';


interface Filter {
  choice: boolean;
  categorie: string;
  value: any;
  name: string;
  data: any;
}

interface FilterData {
  key: string;
  data: any;
}

@Component({
  selector: 'app-filter-ong',
  templateUrl: './filter-ong.component.html',
  styleUrls: ['./filter-ong.component.scss']
})
export class FilterOngComponent implements OnInit {

  noFilter: string;
  nbExperienceMin: string;
  nbExperienceMax: string;
  filterDataOng: any;
  userFilter: FilterData[];
  @Input() active: string[];
  @Output () filters  = new EventEmitter<string>();


  constructor(
    private api: ApiService,
    private ong: OngService,
    private zone: NgZone
  ) {
     this.noFilter = '';
     this.nbExperienceMin = '0';
     this.nbExperienceMax = '0';
     this.userFilter = [];
     this.userFilter.push({key: 'noFilter', data: []});
     this.userFilter.push({key: 'statut', data: []});
     this.userFilter.push({key: 'domain', data: []});
     this.userFilter.push({key: 'profil', data: []});
     this.userFilter.push({key: 'age', data: []});
     this.userFilter.push({key: 'exp_geographique', data: []});
     this.userFilter.push({key: 'exp_professionel', data: []});
     this.userFilter.push({key: 'experience', data: []});
     this.filterDataOng = [[]];
     this.ong.getMessage().subscribe(data => {
      if (data && data.message === 'new-ong' || data.message === 'update-ong' || data.message === 'delete-ong') {
        this.getAllOngFilter();
      }
     });
  }

  ngOnInit() {
    this.nbExperienceMin = '0';
    this.nbExperienceMax = '0';
    this.getAllOngFilterOffline();
    this.refreshFilter();
  }


        // Get all cabinets filter
        getAllOngFilter() {
          this.ong.getDomainesAndProfileOngAssociation(true).then((ongFilter: any) => {

            if (ongFilter && ongFilter.message === 'success') {
                this.initRefreshData(ongFilter);
            }
          }).catch(error => {
            this.api.manageServerError(error);
          });
       }


        // Refresh the filter
        refreshFilter() {
          setInterval(() => {
            this.ong.getDomainesAndProfileOngAssociation();
          }, 600000);
        }



          // Get all cabinets filter
          getAllOngFilterOffline() {
            const ongFilter = localStorage.getItem(`data-ong-filters`) ?  JSON.parse(localStorage.getItem(`data-ong-filters`)) : null;
            const ongFilterState = localStorage.getItem('filter-userdata-ong') ?  JSON.parse(localStorage.getItem('filter-userdata-ong')) : [];
            if (ongFilterState && ongFilterState.length > 0) {

              this.filterDataOng = ongFilterState;
              this.noFilter = ongFilterState[6][0];
              this.nbExperienceMin = ongFilterState[7][0];
              this.nbExperienceMax = ongFilterState[7][1];

            } else if (ongFilter && ongFilter.message === 'success') {
                  this.initRefreshData(ongFilter);
             } else {
               this.getAllOngFilter();
             }
         }

     // init refresh data
     initRefreshData(ongFilter: any) {

      this.filterDataOng[0] = [];

      if (ongFilter && ongFilter.liste_statut) {
        ongFilter.liste_statut.forEach(element => {
          this.zone.run(() => {
            this.filterDataOng[0].push({choice: false, categorie: 'ong', value: element.statut.id,
            name: element.statut.intitule, data: element.nbre_ong_association});
          });
  
        });
      }

     // console.log(this.filterDataOng[0]);
      this.filterDataOng[1] = [];
      if (ongFilter && ongFilter.liste_domaine_expertise) {
        ongFilter.liste_domaine_expertise.forEach(element => {
          this.zone.run(() => {
            this.filterDataOng[1].push({choice: false, categorie: 'ong', value: element.domaine.id,
            name: element.domaine.intitule, data: element.nbre_ong_association});
          });
       
        });
      }

      // console.log(this.filterDataOng[1]);
      this.filterDataOng[2] = [];
      if (ongFilter && ongFilter.liste_profil) {
        ongFilter.liste_profil.forEach(element => {
          this.zone.run(() => {
            this.filterDataOng[2].push({choice: false, categorie: 'ong', value: element.profil.id,
            name: element.profil.intitule, data: element.nbre_ong_association});
          });
        
        });
      }

      // console.log(this.filterDataOng[2]);
      this.filterDataOng[3] = [];
      if (ongFilter && ongFilter.liste_age_ong_association) {
        ongFilter.liste_age_ong_association.forEach(element => {
          this.zone.run(() => {
            this.filterDataOng[3].push({choice: false, categorie: 'ong', value: element.nbre_annee_creation,
            name: element.nbre_annee_creation > 1 ? ' ans' : ' an', data: element.nbre_ong_association});
          });
        
        });
      }

     // console.log(this.filterDataOng[3]);
      this.filterDataOng[4] = [];
      if (ongFilter && ongFilter.liste_experience_geographique) {
        ongFilter.liste_experience_geographique.forEach(element => {
          this.zone.run(() => {
            this.filterDataOng[4].push({choice: false, categorie: 'ong', value: element.experience_geographique.id,
            name: element.experience_geographique.intitule, data: element.nbre_ong_association});
          });
         
        });
      }

      // console.log(this.filterDataOng[4]);
      this.filterDataOng[5] = [];
      if (ongFilter && ongFilter.liste_annee_experience_ong_association) {
        ongFilter.liste_annee_experience_ong_association.forEach(element => {
          this.zone.run(() => {
            this.filterDataOng[5].push({choice: false, categorie: 'ong', value: element.nbre_annee_d_experience,
            name: element.nbre_annee_d_experience > 1 ? ' ans' : ' an', data: element.nbre_ong_association});
          });
       
        });
      }

     // console.log(this.filterDataOng[5]);
      this.zone.run(() => {
        this.filterDataOng[6] = [0];
        this.filterDataOng[7] = [0, 0];
      });

      localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));


     }

        // Update no Filter
        updateNoFilter() {
          this.userFilter[0].data = [this.noFilter ? 1 : 0 ];
          this.filterDataOng[6] = [this.noFilter ? 1 : 0 ];
          localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

        // Update the status
        updateFilterStatus() {
          const status = [];
          this.filterDataOng[0].forEach(statut => {
                if (statut.choice) {
                  status.push(statut.value);
                }
          });
          this.userFilter[1].data = status;
          localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

       // Update the filter domaine
       updateFilterDomaine() {
        const domanins = [];
        this.filterDataOng[1].forEach(domain => {
              if (domain.choice) {
                domanins.push(domain.value);
              }
        });
        this.userFilter[2].data = domanins;
        localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));
        this.filters.emit(JSON.stringify(this.userFilter));
       }

        // Update the filter profil
        updateFilterProfil() {
          const profils = [];
          this.filterDataOng[2].forEach(domain => {
            if (domain.choice) {
              profils.push(domain.value);
            }
          });
          this.userFilter[3].data = profils;
          localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));
          this.filters.emit(JSON.stringify(this.userFilter));
        }


         // update age
         updateFilterAge() {
          const ages = [];
          this.filterDataOng[3].forEach(age => {
            if (age.choice) {
              ages.push(age.value);
            }
          });
          this.userFilter[4].data = ages;
          localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));
          this.filters.emit(JSON.stringify(this.userFilter));
         }

         // update experience geogragrique
         updateFilterExpGeographique() {

          const expGeo = [];
          this.filterDataOng[4].forEach(exp => {
                if (exp.choice) {
                  expGeo.push(exp.value);
                }
          });
          this.userFilter[5].data = expGeo;
          localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));
          this.filters.emit(JSON.stringify(this.userFilter));

        }

         // update experience professionnelle
         updateFilterExpProfessionnel() {

          const expPro = [];
          this.filterDataOng[5].forEach(exp => {
                if (exp.choice) {
                  expPro.push(exp.value);
                }
          });
          this.userFilter[6].data = expPro;
          localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));
          this.filters.emit(JSON.stringify(this.userFilter));

        }


         // Update the filter
         updateFilerExperience() {
             this.userFilter[7].data = [this.nbExperienceMin, this.nbExperienceMax];
             this.filterDataOng[7] = this.userFilter[7].data;
             localStorage.setItem('filter-userdata-ong', JSON.stringify(this.filterDataOng));
              if (this.nbExperienceMin <= this.nbExperienceMax) {
                this.filters.emit(JSON.stringify(this.userFilter));
              }
          }


}
