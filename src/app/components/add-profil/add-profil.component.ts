import { DataService } from './../../providers/data/data.service';
import { ApiService } from './../../providers/api/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../providers/user/user.service';

@Component({
  selector: 'app-add-profil',
  templateUrl: './add-profil.component.html',
  styleUrls: ['./add-profil.component.scss']
})
export class AddProfilComponent implements OnInit {

  formAddProfil: FormGroup;
  intituleNotFound: boolean;
  intituleAlreadyExist: boolean;
  updateSuccess: boolean;
  internetError: boolean;

  constructor(
    private spinner: NgxSpinnerService,
    private api: ApiService,
    private userService: UserService,
    private data: DataService,
    private fb: FormBuilder
  ) {
      this.intituleNotFound = false;
      this.intituleAlreadyExist = false;
      this.updateSuccess = false;
      this.internetError = false;
  }

  ngOnInit() {
      this.initProfil();
      this.checkSession();
  }

    // init the user login form
    initProfil() {
      this.formAddProfil = this.fb.group({
        intitule: ['', Validators.required],
        description: ['']
      });
    }

    checkSession() {
      if (this.userService.getSession()) {
        this.api.refreshAccesToken();
      }
    }

  // Login the user
  addProfil(userData: any) {

    this.spinner.show('add-profil', {
      type: 'ball-spin-fade',
      size: 'large',
      bdColor: 'rgba(251,140,0, .8)',
      color: 'white'
    });

    this.updateSuccess = false;
    this.internetError = false;

    this.data.addProfil(userData).then((reponse: any) => {

      if (reponse && reponse.message === 'success') {
          // send a message with profil data
          this.initProfil();
          this.data.sendDataMessage({message: 'profil', data: reponse.profil});
          this.spinner.hide('add-profil');
          this.updateSuccess = true;
          setTimeout(() => {
            this.updateSuccess = false;
          }, 60000);
      }

    }).catch(error => {

        this.spinner.hide('add-profil');
        this.api.manageServerError(error);
        if (error.error && error.error.intitule_not_found) {
            this.intituleNotFound = true;
        }

        if (error.error && error.error.intitule_not_found) {
          this.intituleAlreadyExist = true;
        }
    });
}

}
