import { DataService } from './../../providers/data/data.service';
import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit, Output, EventEmitter, NgZone } from '@angular/core';
import { OngService } from '../../providers/ong/ong.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { remote } from 'electron';

@Component({
  selector: 'app-ong',
  templateUrl: './ong.component.html',
  styleUrls: ['./ong.component.scss']
})
export class OngComponent implements OnInit {

  defaultOng: any;
  defaultOngFilter: any;
  currentsOng: any;
  activeOng: any;
  totalPages: number;
  nbItems: number;
  nbItemsByPage: number;
  currentPage: number;
  disponibilites: any;
  juridictions: any;
  filtreOng: string[] = [];
  numero: number;
  workingMode: string;
  loading: boolean;
  percentageLoading: number;
  interval: any;
  @Output() ongs = new EventEmitter();
  userRole: any;


  constructor(
    private ongService: OngService,
    private spinner: NgxSpinnerService,
    private api: ApiService,
    private route: Router,
    private data: DataService,
    public zone: NgZone
  ) {

    this.defaultOng = [];
    this.defaultOngFilter = [];
    this.activeOng = [];
    this.currentsOng = [];
    this.currentPage = 1;
    this.totalPages = 1;
    this.nbItems = 0;
    this.nbItemsByPage = 15;
    this.disponibilites = [];
    this.juridictions = [];
    this.numero = 1;
    this.loading = false;
    this.percentageLoading = 0;
    this.interval = null;

    this.filtreOng.push('noFilter');
    this.filtreOng.push('statut');
    this.filtreOng.push('domain');
    this.filtreOng.push('profil');
    this.filtreOng.push('age');
    this.filtreOng.push('exp_geographique');
    this.filtreOng.push('exp_professionel');
    this.filtreOng.push('experience');
    this.workingMode = this.api.getWorkMode() || 'online';
    this.getDataMessage();
    this.userRole = localStorage.getItem('user-role') ? JSON.parse(localStorage.getItem('user-role')): null;
    this.ongService.getMessage().subscribe(data => {
          if (data && data.message === 'new-ong' || data.message === 'update-ong') {
            this.listOng();
            this.getDisponibilites();
            this.getJuridictions();
          }
    });
  }


  ngOnInit() {
    this.listOngOffline();
    this.getDisponibilites();
    this.getJuridictions();
    this.resetDataMessage();
    this.refreshOngData();
  }


  // Set the loading
  setLoading() {
    this.interval = setInterval( () => {
       this.percentageLoading += 10;
       if (this.percentageLoading === 100) {
         this.percentageLoading = 0;
       }
     }, 1000);
   }

  // Clear the loading
  clearLoading() {
    this.percentageLoading = 0;
    clearInterval(this.interval );
    for (let i = 0; i < 100; i++) {
      clearInterval(i);
    }
  }

  // Get all disponibilites
  getDisponibilites () {
    this.data.getAllDisponibilities().then((disponibilites: any) => {
      this.disponibilites = disponibilites.disponibilite;
    }).catch(error => {
       this.api.manageServerError(error);
    });
  }

   // Get all disponibilites
   getJuridictions () {
      this.data.getAllJuridictions().then((juridictions: any) => {
        this.juridictions = juridictions.forme_juridique;
      }).catch(error => {
         this.api.manageServerError(error);
      });
   }

  // Get the list of cabinets
  listOng() {

       this.ongService.listOngAssociationAll(true).then((ongs: any) => {
        this.loading = false;
        if (ongs.message === 'success') {
            this.defaultOng = ongs.liste_ong_association;
            this.ongs.emit(JSON.stringify(this.defaultOng));
            localStorage.setItem('ong-data-session', JSON.stringify(this.defaultOng));
            this.currentsOng = this.data.formatArrayToMatrix(this.defaultOng, this.nbItemsByPage);
            this.updateActiveList(1);
            this.totalPages = this.currentsOng.length;
            this.nbItems = this.defaultOng.length;
            this.scrollTop();
        }
      }).catch(error => {
        this.loading = false;
        this.api.manageServerError(error);

      });
  }


  // refrsh cabinets data
  refreshOngData() {
    setInterval(() => {
        this.listOng();
    }, 600000);
  }

    // Get the list of cabinets offline
    listOngOffline() {
      this.loading = true;
      const listOng = localStorage.getItem('data-ong-all') ? JSON.parse(localStorage.getItem('data-ong-all')) : null;
      if (listOng && listOng.message === 'success') {
           this.loading = false;
           this.defaultOng = listOng.liste_ong_association;
           this.ongs.emit(JSON.stringify(this.defaultOng));
           this.currentsOng = this.data.formatArrayToMatrix(this.defaultOng, this.nbItemsByPage);
           this.updateActiveList(1);
           this.totalPages = this.currentsOng.length;
           this.nbItems = this.defaultOng.length;
           this.scrollTop();
       } else {
           this.loading = false;
           if (this.currentsOng && this.currentsOng.length === 0) {
                this.loading = true;
                this.listOng();
           }
       }
    }


    // Get Forme juridique
    getFormeJuridique(id: number) {
      let resultat = null;
        this.juridictions.forEach(element => {
          if (element.id === id) {
            resultat = element;
          }
        });
      return resultat ? resultat.intitule : '' ;
    }
  
  
    // Get Disponibilites
    getDisponibilite(id: number) {
      let resultat = null;
      this.disponibilites.forEach(element => {
        if (element.id === id) {
          resultat = element;
        }
      });
      return resultat ? resultat.intitule : '' ;
    }


  // Open the consultants CV
  openDocuments(ong: any) {

    if (ong.file_ong_association && ong.file_ong_association.length > 0) {
      const files = ong.file_ong_association;
      const url  = 'data:application/pdf;base64, ' + files[0].file_en_base_64;
      window.open(url);
    } else {
        alert('Aucun document enregistré !');
    }

  }

    // show the confirmation dialog
    confirmDialog(ong: any, index: number) {
      const dialog = remote.dialog;
      dialog.showMessageBox({
        type: 'none',
        title: 'Confirmation',
        message: 'Voulez-vous supprimer cet ong ?',
        buttons: ['Non', 'Oui']
      }, (reponse) => {
            if (reponse === 0) {
              return;
            } else {
              this.deleteOng(ong, index);
            }
      });
    }


  // Delete the ong
  deleteOng(ong: any, index: number) {

    this.spinner.show('ong', {
      type: 'ball-spin-fade',
      size: 'large',
      bdColor: 'rgba(251,140,0, .8)',
      color: 'white'
    });

    this.ongService.deleteOngAssociation(ong.ong_association_id).then((reponse: any) => {
      this.spinner.hide('ong');
      if (reponse && reponse.message === 'success') {
          // send a message with profil data
         this.ongService.sendMessage('delete-ong');
         this.activeOng.splice(index, 1);
         this.listOng();
      }

    }).catch(error => {
        this.spinner.hide('ong');
        this.api.manageServerError(error);
        if (error && error.user_not_found ) {
            alert('Impossible de supprimer cette ong. Essayer à nouveau!');
        }

        if (error && error.ong_association_id_not_exist ) {
          alert('Cette ong n existe pas dans la Base de Données');
        }
    });

  }

  // Show the detail informations of ong
  showOng(ong: any) {
    const key = 'ongs';
    localStorage.setItem(key, JSON.stringify(ong));
    this.route.navigate([`/dashboard/ong/${key}`]);
  }


  // Get the filters params
  OngFilters(event) {

   const filter  = JSON.parse(event);

    // filter by noFiler
    const noFilter = this.data.getDataByKey('noFilter', filter);
    if (noFilter && noFilter.length > 0) {
      const noFilterData = this.noFilter(filter);
      this.defaultOngFilter = noFilterData;
    }

    // filter by statut
    const statut = this.data.getDataByKey('statut', filter);
    if (statut && statut.length > 0) {
      const statutData = this.filterByStatut(filter);
      this.defaultOngFilter = statutData;
    }
    // Filter by domain
    const domain = this.data.getDataByKey('domain', filter);
    if (domain && domain.length > 0) {
      const domanainsData = this.filterByDomains(filter);
      this.defaultOngFilter = domanainsData;
    }

    // Filter by Profil
    const profil = this.data.getDataByKey('profil', filter);
    if (profil && profil.length > 0) {
      const profilData = this.filterByProfil(filter);
      this.defaultOngFilter = profilData;
    }
    // Filter by age
    const age = this.data.getDataByKey('age', filter);
    if (age && age.length > 0) {
      const ageData = this.filterByAge(filter);
      this.defaultOngFilter = ageData;
    }
    // Filter by experience Geographique
    const exp_geographique = this.data.getDataByKey('exp_geographique', filter);
    if (exp_geographique && exp_geographique.length > 0) {
      const expGeoData = this.filterByAge(filter);
      this.defaultOngFilter = expGeoData;
    }

    // Filter by experience profesionnelle
    const exp_professionel = this.data.getDataByKey('exp_professionel', filter);
    if (exp_professionel && exp_professionel.length > 0) {
      const expProData = this.filterByAge(filter);
      this.defaultOngFilter = expProData;
    }

    // Filter by experience
    const experience = this.data.getDataByKey('experience', filter);
    if (experience && experience.length > 0) {
      const experienceData = this.filterByExperience(filter);
      this.defaultOngFilter = experienceData;
    }

    this.ongs.emit(JSON.stringify(this.defaultOngFilter));
    localStorage.setItem('ong-data-session', JSON.stringify(this.defaultOngFilter));
    this.currentsOng = this.data.formatArrayToMatrix(this.defaultOngFilter, this.nbItemsByPage);
    this.updateActiveList(1);
  }


    // Implement no filter
    noFilter (filter: any) {
      let datasNoFilter = [];
          if (filter && filter.length > 0) {
            const dataNoFilter = this.data.getDataByKey('noFilter', filter);
            if (dataNoFilter[0] === 1) {
              datasNoFilter = this.defaultOng;
            } else {
                datasNoFilter = this.activeOng;
            }
          }
          return datasNoFilter;
     }

      // Implement filter by domaines
      filterByStatut(filter: any) {

        const dataStatusFilter = [];
        if (filter && filter.length > 0) {
            const statutFilter = this.data.getDataByKey('statut', filter);
            this.defaultOngFilter.forEach(element => {
                  if (this.data.isIdinarray([element.statut], statutFilter)) {
                    dataStatusFilter.push(element);
                  }
            });
        }
        return dataStatusFilter;

      }


  // Implement filter
  filterByDomains(filter: any) {

    const dataDomaninsFilter = [];

    if (filter && filter.length > 0) {
        const domainFilter = this.data.getDataByKey('domain', filter);
        this.defaultOngFilter.forEach(element => {
              if (this.data.isIdinarray(element.domaine_expertise_users, domainFilter)) {
                 dataDomaninsFilter.push(element);
              }
        });
    }

    return dataDomaninsFilter;

  }

    // Implement filter
    filterByProfil(filter: any) {

      const dataProfilsFilter = [];

      if (filter && filter.length > 0) {
          const profilFilter = this.data.getDataByKey('profil', filter);
          this.defaultOngFilter.forEach(element => {
                if (this.data.isIdinarray(element.profil_users, profilFilter)) {
                  dataProfilsFilter.push(element);
                }
          });
      }

      return dataProfilsFilter;

    }

          // Implement filter by  nombre annee creation
          filterByAge(filter: any) {

            const dataAgesFilter = [];

            if (filter && filter.length > 0) {
                const ageFilter = this.data.getDataByKey('age', filter);
                const currentDate = new Date();
                let nbreAnneCreation = 0;
                this.defaultOngFilter.forEach(element => {
                       nbreAnneCreation = currentDate.getFullYear() - parseInt(element.annee_creation, 10);
                      if (this.data.isIdinarray([{id: nbreAnneCreation}], ageFilter)) {
                        dataAgesFilter.push(element);
                      }
                });
            }

            return dataAgesFilter;
          }

               // Implement filter by experience geographique
        filterByExperienceGeographique(filter: any) {

          const dataExpGeographiqueFilter = [];

          if (filter && filter.length > 0) {
              const expGeoFilter = this.data.getDataByKey('exp_geographique', filter);
              this.defaultOngFilter.forEach(element => {
                    if (this.data.isIdinarray(element.experience_geographique_users, expGeoFilter)) {
                      dataExpGeographiqueFilter.push(element);
                    }
              });
          }

          return dataExpGeographiqueFilter;
        }

         // Implement filter by experience professionnelle
        filterByExperienceProfessionnelle(filter: any) {

                const dataExpProfessionnelleFilter = [];
                if (filter && filter.length > 0) {
                    const expProFilter = this.data.getDataByKey('exp_professionel', filter);
                    const currentDate = new Date();
                    let anneeExperience = 0;
                    this.defaultOngFilter.forEach(element => {
                        anneeExperience = currentDate.getFullYear() -
                        parseInt(element.date1ereAnneeExperienceEnTantONG, 10);
                          if (this.data.isIdinarray([{id: anneeExperience}], expProFilter)) {
                            dataExpProfessionnelleFilter.push(element);
                          }
                    });
                }
                return dataExpProfessionnelleFilter;
         }


      // Implement filter
  filterByExperience(filter: any) {

    const dataExperienceFilter = [];

    if (filter && filter.length > 0) {
        const experienceFilter = this.data.getDataByKey('experience', filter);
        const currentDate = new Date();
        let anneeExperience = 0;
        this.defaultOngFilter.forEach(element => {
          anneeExperience = currentDate.getFullYear() -
          parseInt(element.date1ereAnneeExperienceEnTantONG, 10);
              if (experienceFilter[0] <= anneeExperience &&  anneeExperience <= experienceFilter[1]) {
                dataExperienceFilter.push(element);
              }
        });
    }

    return dataExperienceFilter;
  }

  // Verify if a cabinet is not in the list of cabinets

  notIn (cabinet: any, cabinetsList: any) {

    let found = true, i = 0;

    while (i < cabinetsList.length && found) {
      found = true;

      if (cabinetsList[i].nom === cabinet.nom) {
        found = false;
      }

      i++;
    }

    return found;

  }

  scrollTop() {
    // Hack: Scrolls to top of Page after page view initialized
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }


   // Filter by keyword and option
   filterByKeyword(keyword: string, option: string) {
    const dataKeywordsFilter = [];
    let words = '', key = '';
    this.defaultOng.forEach(element => {
      words = element.nom + ' ' + element.prenom;
      words = words.toLowerCase();
      key = keyword.trim().toLowerCase();

      switch (option) {

        case 'all':
            if (words.match(key)) {
              dataKeywordsFilter.push(element);
            } else {

              const contenuCv = element.file_ong_association || [];
              contenuCv.forEach(cv => {
                if (cv.contenu_cv) {
                   key = keyword.trim().toLowerCase();
                   words = cv.contenu_file.toLowerCase();

                      if (words.match(key)) {
                         dataKeywordsFilter.push(element);
                      }
                  }
              });
            }
          break;

        case 'nom_prenom':
            if (words.match(key)) {
              dataKeywordsFilter.push(element);
            }
          break;

        case 'cv':
            const contenuCvs = element.file_ong_association || [];
            contenuCvs.forEach(cv => {
              if (cv.contenu_cv) {
                 key = keyword.trim().toLowerCase();
                 words = cv.contenu_file.toLowerCase();

                    if (words.match(key)) {
                       dataKeywordsFilter.push(element);
                    }
                }
            });
          break;

        default:
            if (words.match(key)) {
              dataKeywordsFilter.push(element);
            } else {

              const contenuCv = element.file_ong_association || [];
              contenuCv.forEach(cv => {
                if (cv.contenu_cv) {
                   key = keyword.trim().toLowerCase();
                   words = cv.contenu_file.toLowerCase();

                      if (words.match(key)) {
                         dataKeywordsFilter.push(element);
                      }
                  }
              });
            }
          break;

      }

    });


    this.currentsOng = this.data.formatArrayToMatrix(dataKeywordsFilter, this.nbItemsByPage);
    localStorage.setItem('ong-data-session', JSON.stringify(dataKeywordsFilter));
    this.ongs.emit(JSON.stringify(dataKeywordsFilter));
    this.updateActiveList(1);
   }

     // Get message from component
     getDataMessage() {
        this.data.getDataMessage().subscribe(data => {

          if (data && data.key === 'ong' && data.mode === 'keyword') {
               this.filterByKeyword(data.value, data.option);
          }
        });
     }


  // Pagination with server data
  nextPage(currentPage: number) {
    if ((currentPage + 1) <= this.totalPages) {
       this.currentPage = currentPage + 1;
    }
  }

  previousPage(currentPage: number) {
    if (currentPage > 1) {
       this.currentPage = currentPage - 1;
    }
  }


    // Update the number of pages
    updateNumberItems(nbItems: number) {
      this.nbItemsByPage = nbItems;
      this.currentsOng = this.data.formatArrayToMatrix(this.defaultOng, this.nbItemsByPage);
      this.totalPages = this.currentsOng.length;
      this.updateActiveList(this.currentPage);
    }

    // get number items by page
    getNumberItems() {
      let i = 5;
      const nbItemsByPage = [];
      while (i < this.nbItems) {
        nbItemsByPage.push(i);
        i *= 2;
      }
       return nbItemsByPage;
    }


  // Pagination with local data

  // update active list
  updateActiveList(id: number) {
    this.zone.run(() => {
      this.numero = id;
      (this.currentsOng[id - 1] && this.currentsOng[id - 1].length > 0) ?
      this.activeOng = this.currentsOng[id - 1] :  this.activeOng = [];
      this.currentPage = this.numero;
    });
  }

  // Go to previous list
  previousActiveList(currentId: number) {
    if (currentId > 1) {
    const position = currentId - 1 ;
     position > 0 ? this.numero = position : this.numero = currentId ;
     (this.currentsOng[this.numero  - 1] && this.currentsOng[this.numero - 1].length > 0) ?
      this.activeOng = this.currentsOng[this.numero - 1] :  this.activeOng = [];
      this.currentPage = this.numero;
    }
  }

  // Go to next list
  nextActiveList(currentId: number) {
    if (currentId + 1 <= this.totalPages) {
    const position = currentId + 1 ;
     position <= this.currentsOng.length ? this.numero = position : this.numero = currentId ;
     (this.currentsOng[this.numero  - 1] && this.currentsOng[this.numero  - 1].length > 0) ?
     this.activeOng = this.currentsOng[this.numero - 1] :  this.activeOng = [];
     this.currentPage = this.numero;
    }
  }


      // reset data Message
      resetDataMessage() {
        this.data.getDataMessage().subscribe(message => {
              if (message === 'filter-ong') {
                if (this.activeOng && this.activeOng.length === 0) {
                    this.listOngOffline();
                }
              }
        });
     }


}
