import { OngService } from './../../../providers/ong/ong.service';
import { DataService } from './../../../providers/data/data.service';
import { ApiService } from './../../../providers/api/api.service';
import { Component, OnInit, NgZone } from '@angular/core';

interface ProfilData {
  optionName: string;
  nbreOption: number;
}

interface StastByDomainOption {
  domaineName: string;
  option: ProfilData[];
  frequence: number;
}


@Component({
  selector: 'app-ong-stats',
  templateUrl: './ong-stats.component.html',
  styleUrls: ['./ong-stats.component.scss']
})
export class OngStatsComponent implements OnInit {

  numero: number;
  currentsOngstat: any;
  activeOngstat: any;
  ongStatsByDomainProfil: any;
  ongStatsByDomainStatus: any;
  activeOngstatByProfil: any;
  activeOngstatByExperience: any;
  ongStats: any;
  total_ong: number;
  total_ong_pour_tous_les_domaines: number;
  nbItemsByPage: number;
  totalPages: number;
  currentPage: number;
  nbItems: number;
  loading: boolean;
  percentageLoading: number;
  interval: any;

  constructor(
    private api: ApiService,
    private data: DataService,
    private ongService: OngService,
    public zone: NgZone

  ) {
    this.numero = 1;
    this.currentsOngstat = [];
    this.activeOngstat = [];
    this.ongStats = [];
    this.total_ong = 0;
    this.total_ong_pour_tous_les_domaines = 0;
    this.nbItemsByPage = 15;
    this.totalPages = 1;
    this.currentPage = 1;
    this.nbItems = 0;
    this.loading = false;
    this.percentageLoading = 10;
    this.interval = null;
    this.ongService.getMessage().subscribe(data => {
      if (data && data.message === 'new-ong' || data.message === 'update-ong' || data.message === 'delete-ong') {
        this.getOngStats();
        this.getOngStatsByDomainProfil();
        this.getOngStatsByDomainStatut();
        this.getOngStatsByDomainExperience();
      }
     });
  }

  ngOnInit() {
    this.getOngStatsOffline();
    this.getOngStatsByDomainProfil();
    this.getOngStatsByDomainStatut();
    this.getOngStatsByDomainExperience();
    this.refreshData();
  }

    // Set the loading
    setLoading() {
      this.interval = setInterval( () => {
         this.percentageLoading += 10;
         if (this.percentageLoading === 100) {
           this.percentageLoading = 0;
         }
       }, 1000);
     }

     // Clear the loading
     clearLoading() {
      this.percentageLoading = 0;
       clearInterval(this.interval );
       for (let i = 0; i < 100; i++) {
         clearInterval(i);
       }
     }


  // Get the cabinets stats
  getOngStats() {
    this.ongService.ongAssociationStats(true).then((response: any) => {
      this.loading = false;
      if (response && response.message === 'success') {
         this.zone.run(() => {
          this.ongStats = response.liste_domaine_expertise;
          this.total_ong = response.total_ong_association;
          this.total_ong_pour_tous_les_domaines = response.total_ong_association_pour_tous_les_domaines;
          this.activeOngstat = this.ongStats;
         });
      }
    }).catch(error => {
          this.loading = false;
          this.api.manageServerError(error);
    });
  }


  // Refresh the stat data
  refreshData() {
    setInterval(() => {
        this.getOngStats();
    }, 600000);
  }


  // Get the cabinets stats
  getOngStatsOffline() {

      this.loading = true;
      const response =  localStorage.getItem(`data-ong-statistiques`) ? JSON.parse(localStorage.getItem(`data-ong-statistiques`)) : null;
      this.loading = false;
      if (response && response.message === 'success') {
         this.ongStats = response.liste_domaine_expertise;
         this.total_ong = response.total_ong_association;
         this.total_ong_pour_tous_les_domaines = response.total_ong_association_pour_tous_les_domaines;
         this.activeOngstat = this.ongStats;
      } else {
         this.loading = false;
         if (this.ongStats && this.ongStats.length === 0) {
            this.loading = true;
            this.getOngStats();
         }

      }
  }

   // show the currents cart data
   showCurrentsCharts(index: number) {
    const data = this.activeOngstat[index];
  }


  // Get the number arround
  arrondi(data: any) {
    return  Math.floor(data);
  }


  // get the total frequence
  frequency() {
    let frequence = 0;
    this.activeOngstat.forEach(element => {
        frequence += Math.floor(element.pourcentage);
    });
    return frequence;
  }


  // Format list of consultants b
  groupByDomainProfil(allConsulants: any) {

    const domaines = localStorage.getItem('ambero-domaines') ? JSON.parse(localStorage.getItem('ambero-domaines')): {domaines:[]};
    const profils = localStorage.getItem('ambero-allProfil') ? JSON.parse(localStorage.getItem('ambero-allProfil')): {profil:[]};

    let nombreProfil = 0, totalProfil = 0;
    let dataProfil: ProfilData[] = [];
    const consultantStatsByDomainProfil: StastByDomainOption[] = [];

    if (domaines.domaines && domaines.domaines.length) {
        domaines.domaines.forEach(domain => {
            // Get all consultants by domain
            const consultantsBydomain = this.data.getAllDomainById(domain.id, allConsulants);
            // Get all consultant by profil
            dataProfil = [];
            nombreProfil = 0;
            totalProfil = 0;
            profils.profil.forEach(profil => {
                  const profilsList = this.data.getAllProfilById(profil.id, consultantsBydomain);
                  nombreProfil = profilsList.length;
                  dataProfil.push({
                    optionName: profil.intitule,
                    nbreOption: nombreProfil || 0
                 });
            });

                dataProfil.forEach(profil => {
                  totalProfil += profil.nbreOption;
                });
                consultantStatsByDomainProfil.push({domaineName: domain.intitule, option: dataProfil,
                  frequence: totalProfil});
          });
    }

    return consultantStatsByDomainProfil;
}


  // Experience less or egal than 2
 getAllExperienceLess2(ongs: any) {

      const ongsStatut = [];
      const dateFormat =  new Date();
      let nbreAnnee = 0;
      ongs.forEach(ong => {
        nbreAnnee =  dateFormat.getFullYear() - parseInt(ong.annee_creation, 10);
          if (nbreAnnee <= 2 ) {
            ongsStatut.push(ong);
          }
      });

      return ongsStatut;
  }


 // Experience between 2 and 5
 getAllExperienceBetween2and5(ongs: any) {

  const ongsStatut = [];
  const dateFormat =  new Date();
  let nbreAnnee = 0;
  ongs.forEach(ong => {
    nbreAnnee =  dateFormat.getFullYear() - parseInt(ong.annee_creation, 10);
      if (2 < nbreAnnee && nbreAnnee <= 5) {
        ongsStatut.push(ong);
      }
  });

  return ongsStatut;
}

   // Experience between 5 and 10
   getAllExperienceBetween5and10(ongs: any) {

    const ongsStatut = [];
    const dateFormat =  new Date();
    let nbreAnnee = 0;
    ongs.forEach(ong => {
      nbreAnnee =  dateFormat.getFullYear() - parseInt(ong.annee_creation, 10);
        if (5 < nbreAnnee && nbreAnnee <= 10) {
          ongsStatut.push(ong);
        }
    });
    return ongsStatut;
  }

   // Experience between 5 and 10
   getAllExperienceUpTo10(ongs: any) {

    const ongsStatut = [];
    const dateFormat =  new Date();
    let nbreAnnee = 0;
    ongs.forEach(ong => {
      nbreAnnee =  dateFormat.getFullYear() - parseInt(ong.annee_creation, 10);
        if (nbreAnnee > 10) {
          ongsStatut.push(ong);
        }
    });

    return ongsStatut;
  }


// Format list of consultants b
groupByDomainExperience(allOngs: any) {

  const domaines = localStorage.getItem('ambero-domaines') ? JSON.parse(localStorage.getItem('ambero-domaines')) : {domaines: []};

  let  totalStatut = 0;
  let dataStatut: ProfilData[] = [];
  const ongStatsByExperience: StastByDomainOption[] = [];

  if (domaines.domaines && domaines.domaines.length) {
      domaines.domaines.forEach(domain => {
          // Get all consultants by domain
          const consultantsBydomain = this.data.getAllDomainById(domain.id, allOngs);
          // Get all consultant by profil
          const resultat1 = this.getAllExperienceLess2(consultantsBydomain);
          totalStatut += resultat1.length;
          const resultat2 = this.getAllExperienceBetween2and5(consultantsBydomain);
          totalStatut += resultat2.length;
          const resultat3 = this.getAllExperienceBetween5and10(consultantsBydomain);
          totalStatut += resultat3.length;
          const resultat4 = this.getAllExperienceUpTo10(consultantsBydomain);
          totalStatut += resultat4.length;

          dataStatut = [];
          dataStatut.push({optionName: 'Experience <= 2', nbreOption: resultat1.length});
          dataStatut.push({optionName: 'Experience > 2 & <= 5', nbreOption: resultat2.length});
          dataStatut.push({optionName: 'Experience > 5 & <= 10', nbreOption: resultat3.length});
          dataStatut.push({optionName: 'Experience > 10', nbreOption: resultat4.length});

          ongStatsByExperience.push({domaineName: domain.intitule, option: dataStatut,
          frequence: totalStatut});
        });
  }

  return ongStatsByExperience;
}

 // Get the consultants stats by domain and experience
 getOngStatsByDomainExperience() {
  const  listConsultant =  localStorage.getItem('data-ong-all') ? JSON.parse(localStorage.getItem('data-ong-all')) : {liste_ong_association: []} ;
  const resultat =  this.groupByDomainExperience(listConsultant.liste_ong_association);
  this.zone.run(() => {
    this.activeOngstatByExperience = resultat;
   });
}


 // Get the consultants stats by domain and profil
 getOngStatsByDomainProfil() {
     const  listConsultant = localStorage.getItem('data-ong-all') ? JSON.parse(localStorage.getItem('data-ong-all')) : {liste_ong_association: []};
     const resultat =  this.groupByDomainProfil(listConsultant.liste_ong_association);
     this.zone.run(() => {
      this.activeOngstatByProfil = resultat;
     });
 }


// Format list of consultants b
groupByDomainStatut(allConsulants: any) {

  const domaines = localStorage.getItem('ambero-domaines') ?  JSON.parse(localStorage.getItem('ambero-domaines')) : {domaines : []};
  const status = localStorage.getItem('ambero-status')  ? JSON.parse(localStorage.getItem('ambero-status')) : {statut : []};

  let nombreStatut = 0, totalStatut = 0;
  let dataStatut: ProfilData[] = [];
  const consultantStatsByDomainStatus: StastByDomainOption[] = [];

  if (domaines.domaines && domaines.domaines.length) {
      domaines.domaines.forEach(domain => {
          // Get all consultants by domain
          const consultantsBydomain = this.data.getAllDomainById(domain.id, allConsulants);
          // Get all consultant by profil
          dataStatut = [];
          nombreStatut = 0;
          totalStatut = 0;
          status.statut.forEach(statut => {
                const statutList = this.data.getAllStatutById(statut.id, consultantsBydomain);
                nombreStatut = statutList.length;
                dataStatut.push({
                  optionName: statut.intitule,
                  nbreOption: nombreStatut || 0
               });
          });

          dataStatut.forEach(profil => {
              totalStatut += profil.nbreOption;
          });
          consultantStatsByDomainStatus.push({domaineName: domain.intitule, option: dataStatut,
          frequence: totalStatut});
        });
  }

  return consultantStatsByDomainStatus;
}

// Get the consultants stats by domain and profil
getOngStatsByDomainStatut() {
  const  listConsultant = localStorage.getItem('data-ong-all') ?  JSON.parse(localStorage.getItem('data-ong-all')) : {liste_ong_association:[]};
  const resultat =  this.groupByDomainStatut(listConsultant.liste_ong_association);
  this.zone.run(() => {
    this.ongStatsByDomainStatus = resultat;
   });
}

}
