import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultantStatsComponent } from './consultant-stats.component';

describe('ConsultantStatsComponent', () => {
  let component: ConsultantStatsComponent;
  let fixture: ComponentFixture<ConsultantStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultantStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultantStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
