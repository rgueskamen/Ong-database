import { ConsultantService } from './../../../providers/consultant/consultant.service';
import { DataService } from './../../../providers/data/data.service';
import { ApiService } from './../../../providers/api/api.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { Label } from 'ng2-charts';
import { ChartType, ChartOptions } from 'chart.js';

interface ProfilData {
  optionName: string;
  nbreOption: number;
}

interface StastByDomainOption {
  domaineName: string;
  option: ProfilData[];
  frequence: number;
}


@Component({
  selector: 'app-consultant-stats',
  templateUrl: './consultant-stats.component.html',
  styleUrls: ['./consultant-stats.component.scss']
})
export class ConsultantStatsComponent implements OnInit {

  numero: number;
  currentsConsultantstat: any;
  activeConsultantstat: any;
  activeConsultantstatByProfil: any;
  consultantStats: any;
  consultantStatsByDomainProfil: any;
  consultantStatsByDomainStatus: any;
  activeConsultantstatByExperience: any;
  total_nbre_hommes_par_domaines_expertise: number;
  total_nbre_femmes_par_domaines_expertise: number;
  total_consultants_par_domaines_expertise: number;
  nbItemsByPage: number;
  totalPages: number;
  currentPage: number;
  nbItems: number;
  nbre_consultant_par_genre: any;
  loading: boolean;
  percentageLoading: number;
  interval: any;

  // Pie chart expertise par genre
  public pieChartLabelsExpertiseGenre: Label = [];
  public pieChartDataExpertiseGenre: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartColors: any[] = [{
      backgroundColor: [
        '#1A237E', '#e85f1c', 'rgba(0,0,255,0.3)'
    ]
  }];

  public PieChartOptions: ChartOptions = {
  legend: {
    position: 'left',
  },
  elements: {
    arc: {
      borderWidth: 0
    }
  }
};

public pieChartLegend = true;

  constructor(
    private api: ApiService,
    private data: DataService,
    private consultantServive: ConsultantService,
    public zone: NgZone

  ) {
    this.numero = 1;
    this.currentsConsultantstat = [];
    this.activeConsultantstat = [];
    this.activeConsultantstatByProfil = [];
    this.consultantStatsByDomainStatus = [];
    this.activeConsultantstatByExperience = [];
    this.consultantStats = [];
    this.consultantStatsByDomainProfil = [];
    this.total_nbre_hommes_par_domaines_expertise = 0;
    this.total_nbre_femmes_par_domaines_expertise = 0;
    this.total_consultants_par_domaines_expertise = 0;
    this.nbItemsByPage = 15;
    this.totalPages = 1;
    this.currentPage = 1;
    this.nbItems = 0;
    this.nbre_consultant_par_genre = [];
    this.loading = false;
    this.percentageLoading = 10;
    this.interval = null;
    this.consultantServive.getMessage().subscribe(data => {
      if (data && data.message === 'new-consultant' || data.message === 'update-consultant' || data.message === 'delete-consultant') {
        this.getConsultantStatsByDomainGenre();
        this.getConsultantStatsByDomainProfil();
        this.getConsultantStatsByDomainStatut();
        this.getConsultantStatsByDomainExperience();
      }
     });
  }

  ngOnInit() {
    this.getConsultantStatsByDomainGenreOffline();
    this.getConsultantStatsByDomainProfil();
    this.getConsultantStatsByDomainStatut();
    this.getConsultantStatsByDomainExperience();
    this.refreshStatData();
  }


    // Set the loading
    setLoading() {
      this.interval = setInterval( () => {
         this.percentageLoading += 10;
         if (this.percentageLoading === 100) {
           this.percentageLoading = 0;
         }
       }, 1000);
     }

     // Clear the loading
     clearLoading() {
      this.percentageLoading = 0;
       clearInterval(this.interval );
       for (let i = 0; i < 100; i++) {
         clearInterval(i);
       }
     }


  // Get the number around
  arrondi(data: any) {
     return  Math.floor(data);
  }


  // Get the cabinets stats
  getConsultantStatsByDomainGenre() {

    this.consultantServive.consultantStats(true).then((response: any) => {
      this.loading = false;
      if (response && response.message === 'success') {
         this.consultantStats = response.liste_domaine_expertise_par_genre;
         this.nbre_consultant_par_genre = response.nbre_consultant_par_genre;
         this.total_nbre_hommes_par_domaines_expertise = response.total_nbre_hommes_par_domaines_expertise;
         this.total_nbre_femmes_par_domaines_expertise = response.total_nbre_femmes_par_domaines_expertise;
         this.total_consultants_par_domaines_expertise = response.total_consultants_par_domaines_expertise;
         const frenquence = [];
         const pourcentageHomme = Math.floor((this.total_nbre_hommes_par_domaines_expertise /
        this.total_consultants_par_domaines_expertise) * 100);

          const pourcentageFemme = Math.floor((this.total_nbre_femmes_par_domaines_expertise /
          this.total_consultants_par_domaines_expertise) * 100);

         const labels = [];
         labels.push('Homme : ' + pourcentageHomme + '%');
         labels.push('Femme : ' + pourcentageFemme + '%');

         frenquence.push(pourcentageHomme);
         frenquence.push(pourcentageFemme);

         this.zone.run(() => {
          this.pieChartLabelsExpertiseGenre = labels;
          this.pieChartDataExpertiseGenre = frenquence;
          this.activeConsultantstat = this.consultantStats;
         });
      }
    }).catch(error => {
          this.loading = false;
          this.api.manageServerError(error);
    });

  }


  // Refresh Stats
  refreshStatData() {
    setInterval(() => {
          this.getConsultantStatsByDomainGenre();
    }, 600000);
  }



    // Get the cabinets stats offline
    getConsultantStatsByDomainGenreOffline() {

       this.loading = true;

       const response = localStorage.getItem('data-consultant-statistique') ? JSON.parse(localStorage.getItem('data-consultant-statistique')) : null;

        if (response && response.message === 'success') {
           this.loading = false;
           this.consultantStats = response.liste_domaine_expertise_par_genre;
           this.nbre_consultant_par_genre = response.nbre_consultant_par_genre;
           this.total_nbre_hommes_par_domaines_expertise = response.total_nbre_hommes_par_domaines_expertise;
           this.total_nbre_femmes_par_domaines_expertise = response.total_nbre_femmes_par_domaines_expertise;
           this.total_consultants_par_domaines_expertise = response.total_consultants_par_domaines_expertise;
           const frenquence = [];
           const pourcentageHomme = Math.floor((this.total_nbre_hommes_par_domaines_expertise /
          this.total_consultants_par_domaines_expertise) * 100);

            const pourcentageFemme = Math.floor((this.total_nbre_femmes_par_domaines_expertise /
            this.total_consultants_par_domaines_expertise) * 100);

           const labels = [];
           labels.push('Homme : ' + pourcentageHomme + '%');
           labels.push('Femme : ' + pourcentageFemme + '%');

           frenquence.push(pourcentageHomme);
           frenquence.push(pourcentageFemme);
           this.pieChartLabelsExpertiseGenre = labels;
           this.pieChartDataExpertiseGenre = frenquence;
           this.activeConsultantstat = this.consultantStats;
        } else {
            this.loading = false;
            if (this.consultantStats && this.consultantStats.length === 0) {
               this.loading = true;
               this.getConsultantStatsByDomainGenre();
            }

        }

    }



  // Format list of consultants b
  groupByDomainProfil(allConsulants: any) {

      const domaines = localStorage.getItem('ambero-domaines') ? JSON.parse(localStorage.getItem('ambero-domaines')) : {domaines : []};
      const profils = localStorage.getItem('ambero-allProfil') ? JSON.parse(localStorage.getItem('ambero-allProfil')) :  {profil : []} ;

      let nombreProfil = 0, totalProfil = 0;
      let dataProfil: ProfilData[] = [];
      const consultantStatsByDomainProfil: StastByDomainOption[] = [];

      if (domaines.domaines && domaines.domaines.length) {
          domaines.domaines.forEach(domain => {
              // Get all consultants by domain
              const consultantsBydomain = this.data.getAllDomainById(domain.id, allConsulants);
              // Get all consultant by profil
              dataProfil = [];
              nombreProfil = 0;
              totalProfil = 0;
              profils.profil.forEach(profil => {
                    const profilsList = this.data.getAllProfilById(profil.id, consultantsBydomain);
                    nombreProfil = profilsList.length;
                    dataProfil.push({
                      optionName: profil.intitule,
                      nbreOption: nombreProfil || 0
                   });
              });

                  dataProfil.forEach(profil => {
                    totalProfil += profil.nbreOption;
                  });
                  consultantStatsByDomainProfil.push({domaineName: domain.intitule, option: dataProfil,
                    frequence: totalProfil});
            });
      }

      return consultantStatsByDomainProfil;
  }



   // Get the consultants stats by domain and profil
   getConsultantStatsByDomainProfil() {
    const  listConsultant = localStorage.getItem('data-consultant-all') ?  JSON.parse(localStorage.getItem('data-consultant-all')) : {Liste_consultant: []};
    const resultat =  this.groupByDomainProfil(listConsultant.Liste_consultant);
    this.zone.run(() => {
      this.activeConsultantstatByProfil = resultat;
     });
   
}

      // experience less than 2
      getAllExperienceLess2(consulatnts: any) {
        const consultantsStatut = [];
        consulatnts.forEach(consultant => {
            if (consultant.nbre_annee_d_experience <= 2) {
              consultantsStatut.push(consultant);
            }
        });
        return consultantsStatut;
      }

       // experience between 2 and 5
       getAllExperienceBetween2and5(consulatnts: any) {
          const consultantsStatut = [];
          consulatnts.forEach(consultant => {
              if (2 < consultant.nbre_annee_d_experience && consultant.nbre_annee_d_experience <= 5) {
                  consultantsStatut.push(consultant);
              }
          });
          return consultantsStatut;
      }

      // experience between 5 and 10
      getAllExperienceBetween5and10(consulatnts: any) {
            const consultantsStatut = [];
            consulatnts.forEach(consultant => {
                if (2 < consultant.nbre_annee_d_experience && consultant.nbre_annee_d_experience <= 5) {
                    consultantsStatut.push(consultant);
                }
            });
            return consultantsStatut;
       }

       // experience up to 10
       getAllExperienceUpTo10(consulatnts: any) {
          const consultantsStatut = [];
          consulatnts.forEach(consultant => {
              if (2 < consultant.nbre_annee_d_experience && consultant.nbre_annee_d_experience <= 5) {
                  consultantsStatut.push(consultant);
              }
          });
          return consultantsStatut;
      }


     // Format list of consultants b
groupByDomainExperience(allConsultants: any) {

  const domaines = localStorage.getItem('ambero-domaines') ? JSON.parse(localStorage.getItem('ambero-domaines')) : {domaines:[]};

  let totalStatut = 0;
  let dataStatut: ProfilData[] = [];
  const consultantStatsByExperience: StastByDomainOption[] = [];

  if (domaines.domaines && domaines.domaines.length) {
      domaines.domaines.forEach(domain => {
          // Get all consultants by domain
          const consultantsBydomain = this.data.getAllDomainById(domain.id, allConsultants);
          // Get all consultant by profil
          const resultat1 = this.getAllExperienceLess2(consultantsBydomain);
          totalStatut += resultat1.length;
          const resultat2 = this.getAllExperienceBetween2and5(consultantsBydomain);
          totalStatut += resultat2.length;
          const resultat3 = this.getAllExperienceBetween5and10(consultantsBydomain);
          totalStatut += resultat3.length;
          const resultat4 = this.getAllExperienceUpTo10(consultantsBydomain);
          totalStatut += resultat4.length;

          dataStatut = [];
          dataStatut.push({optionName: 'Experience <= 2', nbreOption: resultat1.length});
          dataStatut.push({optionName: 'Experience > 2 & <= 5', nbreOption: resultat2.length});
          dataStatut.push({optionName: 'Experience > 5 & <= 10', nbreOption: resultat3.length});
          dataStatut.push({optionName: 'Experience > 10', nbreOption: resultat4.length});

          consultantStatsByExperience.push({domaineName: domain.intitule, option: dataStatut,
          frequence: totalStatut});
        });
  }

  return consultantStatsByExperience;
}


   // Get the consultants stats by domain and profil
   getConsultantStatsByDomainExperience() {
       const  listConsultant = localStorage.getItem('data-consultant-all') ?  JSON.parse(localStorage.getItem('data-consultant-all')) : {Liste_consultant: []};
       const resultat =  this.groupByDomainExperience(listConsultant.Liste_consultant);
      
       this.zone.run(() => {
        this.activeConsultantstatByExperience = resultat;
       });
   }


  // Format list of consultants b
  groupByDomainStatut(allConsulants: any) {

    const domaines = localStorage.getItem('ambero-domaines') ?  JSON.parse(localStorage.getItem('ambero-domaines')) : {domaines: []} ;
    const status = localStorage.getItem('ambero-status') ? JSON.parse(localStorage.getItem('ambero-status')) : {statut: []};

    let nombreStatut = 0, totalStatut = 0;
    let dataStatut: ProfilData[] = [];
    const consultantStatsByDomainStatus: StastByDomainOption[] = [];

    if (domaines.domaines && domaines.domaines.length) {
        domaines.domaines.forEach(domain => {
            // Get all consultants by domain
            const consultantsBydomain = this.data.getAllDomainById(domain.id, allConsulants);
            // Get all consultant by profil
            dataStatut = [];
            nombreStatut = 0;
            totalStatut = 0;
            status.statut.forEach(statut => {
                  const statutList = this.data.getAllStatutById(statut.id, consultantsBydomain);
                  nombreStatut = statutList.length;
                  dataStatut.push({
                    optionName: statut.intitule,
                    nbreOption: nombreStatut || 0
                 });
            });

            dataStatut.forEach(profil => {
                totalStatut += profil.nbreOption;
            });
            consultantStatsByDomainStatus.push({domaineName: domain.intitule, option: dataStatut,
            frequence: totalStatut});
          });
    }

    return consultantStatsByDomainStatus;
}

  // Get the consultants stats by domain and profil
  getConsultantStatsByDomainStatut() {
    const  listConsultant =  localStorage.getItem('data-consultant-all') ? JSON.parse(localStorage.getItem('data-consultant-all')) : {Liste_consultant:[]};
    const resultat =  this.groupByDomainStatut(listConsultant.Liste_consultant);
    this.zone.run(() => {
      this.consultantStatsByDomainStatus = resultat;
     });
  }


}
