import { CabinetService } from './../../../providers/cabinet/cabinet.service';
import { DataService } from './../../../providers/data/data.service';
import { ApiService } from './../../../providers/api/api.service';
import { Component, OnInit, NgZone } from '@angular/core';

interface ProfilData {
  optionName: string;
  nbreOption: number;
}

interface StastByDomainOption {
  domaineName: string;
  option: ProfilData[];
  frequence: number;
}

@Component({
  selector: 'app-cabinet-stats',
  templateUrl: './cabinet-stats.component.html',
  styleUrls: ['./cabinet-stats.component.scss']
})
export class CabinetStatsComponent implements OnInit {

  numero: number;
  currentsCabinetstat: any;
  activeCabinetstat: any;
  cabinetStats: any;
  cabinetStatsByDomainProfil: any;
  cabinetStatsByDomainStatus: any;
  activeCabinetstatByProfil: any;
  activeCabinetstatByExperience: any;
  total_cabinet_bureau_etude: number;
  total_cabinet_bureau_etude_pour_tous_les_domaines: number;
  nbItemsByPage: number;
  totalPages: number;
  currentPage: number;
  nbItems: number;
  loading: boolean;
  percentageLoading: number;
  interval: any;

  constructor(
    private api: ApiService,
    private data: DataService,
    private cabinetService: CabinetService,
    private zone: NgZone
  ) {
    this.numero = 1;
    this.currentsCabinetstat = [];
    this.activeCabinetstat = [];
    this.activeCabinetstatByExperience = [];
    this.cabinetStats = [];
    this.total_cabinet_bureau_etude = 0;
    this.total_cabinet_bureau_etude_pour_tous_les_domaines = 0;
    this.nbItemsByPage = 15;
    this.totalPages = 1;
    this.currentPage = 1;
    this.nbItems = 0;
    this.loading = false;
    this.percentageLoading = 10;
    this.interval = null;
    this.cabinetService.getMessage().subscribe(data => {
      if (data && data.message === 'new-cabinet' || data.message === 'update-cabinet' || data.message === 'delete-cabinet') {
        this.getCabinetStats();
        this.getCabinetStatsByDomainStatut();
        this.getCabinetStatsByDomainProfil();
        this.getCabinetStatsByDomainExperience();
      }
     });
  }

  ngOnInit() {
    this.getCabinetStatsOffline();
    this.getCabinetStatsByDomainStatut();
    this.getCabinetStatsByDomainProfil();
    this.getCabinetStatsByDomainExperience();
    this.refreshStatData();
  }

    // Set the loading
    setLoading() {
      this.interval = setInterval( () => {
         this.percentageLoading += 10;
         if (this.percentageLoading === 100) {
           this.percentageLoading = 0;
         }
       }, 1000);
     }

     // Clear the loading
     clearLoading() {
      this.percentageLoading = 0;
       clearInterval(this.interval );
       for (let i = 0; i < 100; i++) {
         clearInterval(i);
       }
     }


  // Get the cabinets stats
  getCabinetStats() {

    this.cabinetService.cabinetStats(true).then((response: any) => {
      this.loading = false;
      if (response && response.message === 'success') {
        this.zone.run(() => {
          this.cabinetStats = response.liste_domaine_expertise;
          this.total_cabinet_bureau_etude = response.total_cabinet_bureau_etude;
          this.total_cabinet_bureau_etude_pour_tous_les_domaines = response.total_cabinet_bureau_etude_pour_tous_les_domaines;
          this.activeCabinetstat = this.cabinetStats;
        });
      }
    }).catch(error => {
          this.loading = false;
          this.api.manageServerError(error);
    });

  }


  // Refresh stats data
  refreshStatData() {
    setInterval(() => {
        this.getCabinetStats();
    }, 600000);
  }

    // Get the cabinets stats
    getCabinetStatsOffline() {

      this.loading = true;
      const statsData  = localStorage.getItem(`data-cabinets-statistiques`) ?  JSON.parse(localStorage.getItem(`data-cabinets-statistiques`)) : null;
        if (statsData && statsData.message === 'success') {
           this.loading = false;
           this.cabinetStats = statsData.liste_domaine_expertise;
           this.total_cabinet_bureau_etude = statsData.total_cabinet_bureau_etude;
           this.total_cabinet_bureau_etude_pour_tous_les_domaines = statsData.total_cabinet_bureau_etude_pour_tous_les_domaines;
           this.activeCabinetstat = this.cabinetStats;
        } else {
          this.loading = false;
          if (this.cabinetStats && this.cabinetStats.length === 0) {
               this.loading = true;
              this.getCabinetStats();
          }
        }

    }

   // Get the number arround
   arrondi(data: any) {
    return  Math.floor(data);
  }

  // get the total frequence
  frequency() {
    let frequence = 0;
    this.activeCabinetstat.forEach(element => {
        frequence += Math.floor(element.pourcentage);
    });
    return frequence;
  }

  // show the currents cart data
  showCurrentsCharts(index: number) {
    const data = this.activeCabinetstat[index];
  }


  // Format list of consultants b
  groupByDomainProfil(allConsulants: any) {

    const domaines = localStorage.getItem('ambero-domaines') ?  JSON.parse(localStorage.getItem('ambero-domaines')) : {domaines: []};
    const profils = localStorage.getItem('ambero-allProfil') ?  JSON.parse(localStorage.getItem('ambero-allProfil')) : {profil: []};

    let nombreProfil = 0, totalProfil = 0;
    let dataProfil: ProfilData[] = [];
    const consultantStatsByDomainProfil: StastByDomainOption[] = [];

    if (domaines.domaines && domaines.domaines.length) {
        domaines.domaines.forEach(domain => {
            // Get all consultants by domain
            const consultantsBydomain = this.data.getAllDomainById(domain.id, allConsulants);
            // Get all consultant by profil
            dataProfil = [];
            nombreProfil = 0;
            totalProfil = 0;
            profils.profil.forEach(profil => {
                  const profilsList = this.data.getAllProfilById(profil.id, consultantsBydomain);
                  nombreProfil = profilsList.length;
                  dataProfil.push({
                    optionName: profil.intitule,
                    nbreOption: nombreProfil || 0
                 });
            });

                dataProfil.forEach(profil => {
                  totalProfil += profil.nbreOption;
                });
                consultantStatsByDomainProfil.push({domaineName: domain.intitule, option: dataProfil,
                  frequence: totalProfil});
          });
    }

    return consultantStatsByDomainProfil;
}


    // Experience less than 2
    getAllExperienceLess2(cabinets: any) {
      const cabinetsStatut = [];
      const dateFormat =  new Date();
      let nbreAnnee = 0;
      cabinets.forEach(cabinet => {
        nbreAnnee =  dateFormat.getFullYear() - parseInt(cabinet.annee_creation, 10);
          if (nbreAnnee <= 2 ) {
              cabinetsStatut.push(cabinet);
          }
      });

      return cabinetsStatut;
   }

       // Experience between  2 and 5
       getAllExperienceBetween2and5(cabinets: any) {
        const cabinetsStatut = [];
        const dateFormat =  new Date();
        let nbreAnnee = 0;
        cabinets.forEach(cabinet => {
          nbreAnnee =  dateFormat.getFullYear() - parseInt(cabinet.annee_creation, 10);
            if (2 < nbreAnnee && nbreAnnee <= 5 ) {
                cabinetsStatut.push(cabinet);
            }
        });

        return cabinetsStatut;
     }


       // Experience between  5 and 10
       getAllExperienceBetween5and10(cabinets: any) {
        const cabinetsStatut = [];
        const dateFormat =  new Date();
        let nbreAnnee = 0;
        cabinets.forEach(cabinet => {
          nbreAnnee =  dateFormat.getFullYear() - parseInt(cabinet.annee_creation, 10);
            if (5 < nbreAnnee && nbreAnnee <= 10 ) {
                cabinetsStatut.push(cabinet);
            }
        });

        return cabinetsStatut;
     }

         // Experience up to 10
         getAllExperienceUpTo10(cabinets: any) {
          const cabinetsStatut = [];
          const dateFormat =  new Date();
          let nbreAnnee = 0;
          cabinets.forEach(cabinet => {
            nbreAnnee =  dateFormat.getFullYear() - parseInt(cabinet.annee_creation, 10);
              if (nbreAnnee > 10 ) {
                  cabinetsStatut.push(cabinet);
              }
          });

          return cabinetsStatut;
       }

       // Format list of consultants b
groupByDomainExperience(allCabinets: any) {

  const domaines = localStorage.getItem('ambero-domaines') ? JSON.parse(localStorage.getItem('ambero-domaines')) : {domaines: []} ;

  let totalStatut = 0;
  let dataStatut: ProfilData[] = [];
  const cabinetsStatsByExperience: StastByDomainOption[] = [];

  if (domaines.domaines && domaines.domaines.length) {
      domaines.domaines.forEach(domain => {
          // Get all consultants by domain
          const consultantsBydomain = this.data.getAllDomainById(domain.id, allCabinets);
          // Get all consultant by profil
          const resultat1 = this.getAllExperienceLess2(consultantsBydomain);
          totalStatut += resultat1.length;
          const resultat2 = this.getAllExperienceBetween2and5(consultantsBydomain);
          totalStatut += resultat2.length;
          const resultat3 = this.getAllExperienceBetween5and10(consultantsBydomain);
          totalStatut += resultat3.length;
          const resultat4 = this.getAllExperienceUpTo10(consultantsBydomain);
          totalStatut += resultat4.length;

          dataStatut = [];
          dataStatut.push({optionName: 'Experience <= 2', nbreOption: resultat1.length});
          dataStatut.push({optionName: 'Experience > 2 & <= 5', nbreOption: resultat2.length});
          dataStatut.push({optionName: 'Experience > 5 & <= 10', nbreOption: resultat3.length});
          dataStatut.push({optionName: 'Experience > 10', nbreOption: resultat4.length});

          cabinetsStatsByExperience.push({domaineName: domain.intitule, option: dataStatut,
          frequence: totalStatut});
        });
  }

  return cabinetsStatsByExperience;
}


// Get the consultants stats by domain and profil
getCabinetStatsByDomainExperience() {
  const  listConsultant =  localStorage.getItem('data-cabinets-all') ? JSON.parse(localStorage.getItem('data-cabinets-all')) : {liste_cabinet : []};
  const resultat =  this.groupByDomainExperience(listConsultant.liste_cabinet);
  this.zone.run(() => {
    this.activeCabinetstatByExperience = resultat;
  });
}



 // Get the consultants stats by domain and profil
 getCabinetStatsByDomainProfil() {
     const  listConsultant =  localStorage.getItem('data-cabinets-all') ? JSON.parse(localStorage.getItem('data-cabinets-all')) : {liste_cabinet:[]};
     const resultat =  this.groupByDomainProfil(listConsultant.liste_cabinet);
     this.zone.run(() => {
      this.activeCabinetstatByProfil = resultat;
    });
  
 }


// Format list of consultants b
groupByDomainStatut(allConsulants: any) {

  const domaines = localStorage.getItem('ambero-domaines') ? JSON.parse(localStorage.getItem('ambero-domaines')) : {domaines: []} ;
  const status = localStorage.getItem('ambero-status') ? JSON.parse(localStorage.getItem('ambero-status')) : {statut: []};

  let nombreStatut = 0, totalStatut = 0;
  let dataStatut: ProfilData[] = [];
  const consultantStatsByDomainStatus: StastByDomainOption[] = [];

  if (domaines.domaines && domaines.domaines.length) {
      domaines.domaines.forEach(domain => {
          // Get all consultants by domain
          const consultantsBydomain = this.data.getAllDomainById(domain.id, allConsulants);
          // Get all consultant by profil
          dataStatut = [];
          nombreStatut = 0;
          totalStatut = 0;
          status.statut.forEach(statut => {
                const statutList = this.data.getAllStatutById(statut.id, consultantsBydomain);
                nombreStatut = statutList.length;
                dataStatut.push({
                  optionName: statut.intitule,
                  nbreOption: nombreStatut || 0
               });
          });

          dataStatut.forEach(profil => {
              totalStatut += profil.nbreOption;
          });
          consultantStatsByDomainStatus.push({domaineName: domain.intitule, option: dataStatut,
          frequence: totalStatut});
        });
  }

  return consultantStatsByDomainStatus;
}

// Get the consultants stats by domain and profil
getCabinetStatsByDomainStatut() {
  const  listConsultant =  localStorage.getItem('data-cabinets-all') ? JSON.parse(localStorage.getItem('data-cabinets-all')): {liste_cabinet:[]};
  const resultat =  this.groupByDomainStatut(listConsultant.liste_cabinet);
  this.zone.run(() => {
    this.cabinetStatsByDomainStatus = resultat;
  });
}

}
