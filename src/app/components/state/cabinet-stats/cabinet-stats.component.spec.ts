import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinetStatsComponent } from './cabinet-stats.component';

describe('ImprimePdfComponent', () => {
  let component: CabinetStatsComponent;
  let fixture: ComponentFixture<CabinetStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinetStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinetStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
