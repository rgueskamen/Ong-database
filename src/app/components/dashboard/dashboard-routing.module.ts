import { PdfViewerComponent } from './../pdf-viewer/pdf-viewer.component';
import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AddComponent } from '../add/add.component';
import { CabinetDetailComponent } from './../cabinet-detail/cabinet-detail.component';
import { ConsultantDetailComponent } from './../consultant-detail/consultant-detail.component';
import { OngDetailComponent } from './../ong-detail/ong-detail.component';

export const DashboardRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard-v1/1',
        pathMatch: 'full',
    }, {
        path: '',
        children: [{
            path: 'dashboard-v1/:idTab',
            component: DashboardComponent
        },
        {
            path: 'pdf-viewer/:key/:index',
            component: PdfViewerComponent
        },
        {
            path: 'consultant/:key',
            component: ConsultantDetailComponent
        },
        {
            path: 'cabinets/:key',
            component: CabinetDetailComponent
        },
        {
            path: 'ong/:key',
            component: OngDetailComponent
        },
        {
            path: 'add',
            component: AddComponent
        }
    ]
    },
    { path: '**', redirectTo: 'dashboard-v1/1' }
];

