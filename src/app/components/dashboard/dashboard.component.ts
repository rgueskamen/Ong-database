import { UserService } from './../../providers/user/user.service';
import { DataService } from './../../providers/data/data.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../providers/api/api.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { NgxSpinnerService } from 'ngx-spinner';
import * as JSZip from 'jszip';
import { saveAs } from 'file-saver';
import { remote } from 'electron';
import { AngularCsv } from 'angular7-csv/dist/Angular-csv';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  tabSelect: number;
  statut: string;
  keywordValue: string;
  statutValue: string;
  statutList: any[];
  mode: string;
  searchForm: FormGroup;
  cabinetsList: any[];
  consultantsList: any[];
  ongList: any[];
  impessionData: any[];
  tableData: any[][];
  disponibilites: any;
  juridictions: any;
  userData: any;
  userInfos: any;
  fileTilte: string;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private data: DataService,
    private userService: UserService,
    private active: ActivatedRoute,
    public spinner: NgxSpinnerService,
    private api: ApiService
  ) {
    this.keywordValue = '';
    this.statutValue = '';
    this.statutList = [];
    this.statut = '';
    this.cabinetsList = [];
    this.consultantsList = [];
    this.ongList = [];
    this.impessionData = [];
    this.tableData = [[]];
    this.userData = localStorage.getItem('user-role') ? JSON.parse(localStorage.getItem('user-role')) : null;
    this.userInfos = localStorage.getItem('user-data') ?  JSON.parse(localStorage.getItem('user-data')) : null;
    this.data.sendDataMessage('no-back');
    this.fileTilte = '';

  }

  ngOnInit() {
    this.getAllStatus();
    this.getJuridictions();
    this.getDisponibilites();
    this.initSearchForm();
    this.selectedTab(1);
    this.getUsersRoles();
    this.tabSelect = parseInt(this.active.snapshot.params.idTab, 10) || 1;
    this.selectedTab(this.tabSelect);
  }


  // Get all disponibilites
  getDisponibilites() {
    this.data.getAllDisponibilities().then((disponibilites: any) => {
      this.disponibilites = disponibilites.disponibilite;
    }).catch(error => {
      this.api.manageServerError(error);
    });
  }

  // Get the users roles
  getUsersRoles() {
    this.userService.getAllUserRole().then((users: any) => {
    }).catch(error => {
      this.api.manageServerError(error);
    });
  }

  // Get all disponibilites
  getJuridictions() {
    this.data.getAllJuridictions().then((juridictions: any) => {
      this.juridictions = juridictions.forme_juridique;
    }).catch(error => {
      this.api.manageServerError(error);
    });
  }

  // Get Forme juridique
  getFormeJuridique(id: number) {
    const resultat = this.data.getData(id, 'id', this.juridictions);
    return resultat ? resultat.intitule : '';
  }


  // Get Disponibilites
  getDisponibilite(id: number) {
    const resultat = this.data.getData(id, 'id', this.disponibilites);
    return resultat ? resultat.intitule : '';
  }


  // Get the list of consultants
  getConsultants(event: any) {
    this.consultantsList = event ? JSON.parse(event) : [];
  }

  // Get the list of Cabites d'etude
  getCabinets(event: any) {
    this.cabinetsList = event ? JSON.parse(event) : [];
  }

  // Get the list of Ong an association
  getOngs(event: any) {
    this.ongList = event ?  JSON.parse(event) : [];
  }


  initSearchForm() {
    this.searchForm = this.fb.group({
      keywordValue: [''],
      searchOption: ['all']
    });
  }


  // Go to the page add consultant / cabinets
  add() {
    this.router.navigate(['/dashboard/add']);
  }

  // Selected an option
  selectedTab(index: number) {
    this.tabSelect = index;
    this.searchForm.controls['keywordValue'].setValue('');
    switch (index) {
      case 1:
        this.mode = 'consultant';
        break;

      case 2:
        this.mode = 'cabinet';
        break;

      case 3:
        this.mode = 'ong';
        break;
      default:
        this.mode = 'consultant';
        break;
    }
  }

  // Get the list of status
  getAllStatus() {
    this.data.getAllStatut().then((status: any) => {

      if (status.message === 'success') {
        this.statutList = status.statut;
      }
    }).catch(error => {
      this.api.manageServerError(error);
    });
  }

  // Filter data  consultant by status
  searchFilterBykeyword(keyword: any) {
    const data = { key: this.mode, mode: 'keyword', value: keyword.keywordValue, option: keyword.searchOption };
    this.data.sendDataMessage(data);
  }

  // Construct the table for Impression
  constructTableConsulant(data: any[]) {
    this.tableData = [];
    let ligneData = [];
    let phoneData = '';
    let courriel = '';
    let i = 0, j = 0;
    let index = 1;
    data.forEach(element => {
      ligneData = [];
      ligneData.push(index);
      ligneData.push(element.nom + ' ' + element.nom);
      ligneData.push(element.nbre_annee_d_experience);
      phoneData = '';
      i = 0;
      element.contact_users.forEach(contact => {
        if (contact && contact.telephone) {
          phoneData = contact.telephone;
          const separator = i + 1 < element.contact_users.length ? '/' : '';
          phoneData = phoneData + separator;
        }
        i++;
      });
      ligneData.push(phoneData);

      j = 0;
      element.courriel_users.forEach(couriel => {
        if (couriel && couriel.email) {
          courriel = couriel.email;
          const separator = j + 1 < element.courriel_users.length ? '/' : '';
          courriel = courriel + separator;
        }
        j++;
      });
      ligneData.push(courriel);
      this.tableData.push(ligneData);
      index++;
    });
    return this.tableData;
  }


  // Construct the table for Impression
  constructTableCabinets(data: any[]) {
    this.tableData = [];
    let ligneData = [];
    let index = 1;
    data.forEach(element => {
      ligneData = [];
      ligneData.push(index);
      ligneData.push(element.nom + ' ' + '(' + element.sigle + ')');
      ligneData.push(element.forme_juridique.intitule);
      ligneData.push(element.annee_creation);
      ligneData.push(element.prenom_principal_dirigeant + ' ' + element.nom_principal_dirigeant);
      ligneData.push(element.telephone_principal_dirigeant);
      ligneData.push(element.courriel_principal_dirigeant);
      this.tableData.push(ligneData);
      index++;
    });
    return this.tableData;
  }


  // Construct the table for Impression
  constructTableOngs(data: any[]) {
    this.tableData = [];
    let ligneData = [];
    let index = 1;
    data.forEach(element => {
      ligneData = [];
      ligneData.push(index);
      ligneData.push(element.nom + ' ' + '(' + element.sigle + ')');
      ligneData.push(element.forme_juridique.intitule);
      ligneData.push(element.annee_creation);
      ligneData.push(element.prenom_principal_dirigeant + ' ' + element.nom_principal_dirigeant);
      ligneData.push(element.telephone_principal_dirigeant);
      ligneData.push(element.courriel_principal_dirigeant);
      this.tableData.push(ligneData);
      index++;
    });
    return this.tableData;
  }

  // Generate the Pdf
  imprimerPdf() {

    const doc = new jsPDF('landscape');
    const dateFormat = new Date();
    const dateValue = dateFormat.getFullYear() + '-' + (dateFormat.getMonth() + 1 < 10 ?
      '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1)) + '-' + (dateFormat.getDate() < 10 ?
        '0' + dateFormat.getDate() : dateFormat.getDate());
    const heureValue = (dateFormat.getHours() < 10 ?
      '0' + dateFormat.getHours() : dateFormat.getHours()) + ':' + (dateFormat.getMinutes() < 10 ?
        '0' + dateFormat.getMinutes() : dateFormat.getMinutes()) + ':' + (dateFormat.getSeconds() < 10 ?
          '0' + dateFormat.getSeconds() : dateFormat.getSeconds());
    const userInfos = this.userInfos ? this.userInfos.username : 'no name';

    doc.setFont('courier');
    doc.setFontType('normal');
    doc.setFontSize(12);
    const img = new Image();
    img.src = 'assets/img/ambero.png';
    doc.addImage(img, 'png', 230, 10, 50, 20);

    switch (this.tabSelect) {

      case 1:
        this.impessionData = this.consultantsList;
        doc.text(20, 35, 'RESULTATS – Consultants individuels');
        doc.autoTable({
          theme: 'grid',
          head: [['Nº', 'Noms et prénoms', 'Expérience',
            'Téléphones', 'Couriel']],
          body: this.constructTableConsulant(this.impessionData),
          margin: { top: 40 },
          didDrawPage: (data) => {
            doc.text(150, 5, 'Imprimé par : ' + userInfos
              + ' le  ' + dateValue + ' ' + ' à ' + heureValue);
          }
        });
        this.fileTilte = 'Consultants';
        break;

      case 2:
        this.impessionData = this.cabinetsList;
        doc.text(20, 35, 'RESULTATS – Bureaux d’Etudes');
        doc.autoTable({
          theme: 'grid',
          head: [['Nº', 'Noms des Bureaux d\'Etudes (Sigles)', 'Forme Juridique', 'Année de création', 'Nom du principal Dirigeant',
            'Téléphones', 'Couriel']],
          body: this.constructTableCabinets(this.impessionData),
          margin: { top: 40 },
          didDrawPage: (data) => {
            doc.text(150, 5, 'Imprimé par : ' + userInfos +
              ' le  ' + dateValue + ' ' + ' à ' + heureValue);
          }
        });
        this.fileTilte = 'Bureaux_Etudes';
        break;

      case 3:
        this.impessionData = this.ongList;
        doc.text(20, 35, 'RESULTATS – ONG, Associations et autres');
        doc.autoTable({
          theme: 'grid',
          head: [['Nº', 'Noms des structures (Sigles)', 'Forme Juridique', 'Année de création', 'Nom du principal Dirigeant',
            'Téléphones', 'Couriel']],
          body: this.constructTableOngs(this.impessionData),
          margin: { top: 40 },
          didDrawPage: (data) => {
            doc.text(150, 5, 'Imprimé par : ' + userInfos
              + ' le  ' + dateValue + ' ' + ' à ' + heureValue);
          }
        });
        this.fileTilte = 'ONG_Associations';
        break;

      default:
        doc.text(20, 35, 'RESULTATS – Consultants individuels');
        this.impessionData = this.consultantsList;
        doc.autoTable({
          theme: 'grid',
          head: [['Nº', 'Noms et prénoms', 'Expérience',
            'Téléphones', 'Couriel']],
          body: this.constructTableConsulant(this.impessionData),
          margin: { top: 40 },
          didDrawPage: (data) => {
            doc.text(150, 5, 'Imprimé par : ' + userInfos +
              ' le  ' + dateValue + ' ' + ' à ' + heureValue);
          }
        });
        this.fileTilte = 'Consultants';
        break;

    }

    doc.save(`${this.fileTilte}_${this.api.getTimePrefix()}.pdf`);

  }

  // show the confirmation dialog cancel
  confirmDialogOption() {
    const dialog = remote.dialog;
    dialog.showMessageBox({
      type: 'none',
      title: 'EXPORTATION',
      message: 'Exporter le resultat sous forme de :',
      buttons: ['Annuler', 'PDF', 'CSV']
    }, (reponse) => {
      if (reponse === 0) {
        return;
      } else if (reponse === 1) {
        this.imprimerPdf();
      } else {
        this.imprimerCsv();
      }
    });
  }


  // export data to CSV
  imprimerCsv() {

    switch (this.tabSelect) {

      case 1:
        this.impessionData = this.consultantsList;
        this.constructTableConsultantsCSV(this.impessionData);
        break;

      case 2:
        this.impessionData = this.cabinetsList;
        this.constructTableCabinetsCSV(this.impessionData);
        break;

      case 3:
        this.impessionData = this.ongList;
        this.constructTableOngCSV(this.impessionData);
        break;

      default:
        this.impessionData = this.consultantsList;
        this.constructTableConsultantsCSV(this.impessionData);
        break;
    }
  }

  // Construct csv of consultant Impression
  constructTableConsultantsCSV(data: any) {

    const consultantsVsv = [];
    let phoneData = '';
    let couriel = '';
    let i, j;

    const dateFormat = new Date();
    const dateValue = dateFormat.getFullYear() + '-' + (dateFormat.getMonth() + 1 < 10 ?
      '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1)) + '-' + (dateFormat.getDate() < 10 ?
        '0' + dateFormat.getDate() : dateFormat.getDate());
    const heureValue = (dateFormat.getHours() < 10 ?
      '0' + dateFormat.getHours() : dateFormat.getHours()) + ':' + (dateFormat.getMinutes() < 10 ?
        '0' + dateFormat.getMinutes() : dateFormat.getMinutes()) + ':' + (dateFormat.getSeconds() < 10 ?
          '0' + dateFormat.getSeconds() : dateFormat.getSeconds());
    // const title = 'Imprimé par : ' + this.userInfos.username + ' le  ' + dateValue + ' à ' + heureValue;
    const title = 'RESULTATS – Consultants individuels';

    const csvOptions = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: title,
      useBom: true,
      noDownload: false,
      headers: ['Nº', 'Noms et prénoms', 'Expérience', 'Téléphones', 'Couriel']
    };

    let index = 1;
    data.forEach(element => {

      phoneData = '';
      i = 0;
      element.contact_users.forEach(contact => {
        if (contact && contact.telephone) {
          phoneData = contact.telephone;
          const separator = i + 1 < element.contact_users.length ? '/' : '';
          phoneData = phoneData + separator;
        }
        i++;
      });

      couriel = '';
      j = 0;
      element.courriel_users.forEach(courriel => {
        if (courriel && courriel.email) {
          couriel = courriel.email;
          const separator = j + 1 < element.courriel_users.length ? '/' : '';
          couriel = couriel + separator;
        }
        j++;
      });
      consultantsVsv.push({
        Numero: index,
        Noms_Prenoms: element.nom + ' ' + element.prenom,
        Exp: element.nbre_annee_d_experience,
        Phone: phoneData,
        Email: couriel
      });
      index++;
    });

    const nomArchiveCsv = 'archive_consultant_csv_' + this.api.getTimePrefix();

    new AngularCsv(consultantsVsv, nomArchiveCsv, csvOptions);

  }


  // Format consultant data to export offline
  formatConsultantDataOffline(data: any) {
    const zip = new JSZip();
    // Readme file detail
    zip.file('ReadMe.txt', 'Ce fichier repertoire contient la liste des CVs des Cvs des consultants');
    const pdf = zip.folder('cvs');
    let index = 1;
    data.forEach(element => {
      if (element && element.cv_users && element.cv_users.length > 0) {
        index = 1;
        element.cv_users.forEach(cvData => {
          if (cvData.cv_en_base_64) {
            pdf.file(`cv_${element.nom}_${element.prenom}_${index++}.pdf`, cvData.cv_en_base_64, { base64: true });
          }
        });
      }
    });

    // Generate the zip file asynchronously
    const nomArchive = 'archive_consultant_cv_' + this.api.getTimePrefix() + '.zip';
    zip.generateAsync({ type: 'blob' })
      .then(function (content) {
        // Force down of the Zip file
        saveAs(content, nomArchive);
      });

  }


  // Construct csv of Cabinets
  constructTableCabinetsCSV(data: any) {

    const dateFormat = new Date();
    const dateValue = dateFormat.getFullYear() + '-' + (dateFormat.getMonth() + 1 < 10 ?
      '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1)) + '-' + (dateFormat.getDate() < 10 ?
        '0' + dateFormat.getDate() : dateFormat.getDate());
    const heureValue = (dateFormat.getHours() < 10 ?
      '0' + dateFormat.getHours() : dateFormat.getHours()) + ':' + (dateFormat.getMinutes() < 10 ?
        '0' + dateFormat.getMinutes() : dateFormat.getMinutes()) + ':' + (dateFormat.getSeconds() < 10 ?
          '0' + dateFormat.getSeconds() : dateFormat.getSeconds());
    // const title = 'Imprimé par : ' + this.userInfos.username + ' le  ' + dateValue + ' à ' + heureValue;
    const title = 'RESULTATS – Bureaux d’Etudes';

    const cabinetsCsv = [];
    const csvOptions = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: title,
      useBom: true,
      noDownload: false,
      headers: ['Nº', 'Noms des Bureaux d\'Etudes (Sigles)', 'Forme Juridique', 'Année de création', 'Nom du principal Dirigeant',
        'Téléphones', 'Couriel']
    };
    let index = 1;
    data.forEach(element => {

      cabinetsCsv.push({
        Numero: index,
        Noms_Sigle: element.nom + ' (' + element.sigle + ')',
        Forme_juridique: element.forme_juridique.intitule,
        Annee_Creation: element.annee_creation,
        Responsable: element.prenom_principal_dirigeant + ' ' + element.nom_principal_dirigeant,
        Phone: element.telephone_principal_dirigeant,
        Email: element.courriel_principal_dirigeant
      });
      index++;
    });

    const nomArchiveCsv = 'archive_cabinets_csv_' + this.api.getTimePrefix();

    new AngularCsv(cabinetsCsv, nomArchiveCsv, csvOptions);

  }


  // Format cabinets data to export offline
  formatCabinetDataOffline(data: any) {

    const zip = new JSZip();
    // Readme file detail
    zip.file('ReadMe.txt', 'Ce fichier repertoire contient la liste des documents des cabinets');
    const pdf = zip.folder('cvs');

    let index = 1;
    data.forEach(element => {
      if (element && element.file_cabinet_bureau_etude && element.file_cabinet_bureau_etude.length > 0) {
        index = 1;
        element.file_cabinet_bureau_etude.forEach(fileData => {
          if (fileData.file_en_base_64) {
            pdf.file(`doc_${element.nom}_${index++}.pdf`, fileData.file_en_base_64, { base64: true });
          }
        });
      }
    });

    // Generate the zip file asynchronously
    const nomArchive = `archive_cabinet_cv_${this.api.getTimePrefix()}.zip`;
    zip.generateAsync({ type: 'blob' })
      .then(function (content) {
        // Force down of the Zip file
        saveAs(content, nomArchive);
      });
  }

  // Construct csv ong
  constructTableOngCSV(data: any) {

    const dateFormat = new Date();
    const dateValue = dateFormat.getFullYear() + '-' + (dateFormat.getMonth() + 1 < 10 ?
      '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1)) + '-' + (dateFormat.getDate() < 10 ?
        '0' + dateFormat.getDate() : dateFormat.getDate());
    const heureValue = (dateFormat.getHours() < 10 ?
      '0' + dateFormat.getHours() : dateFormat.getHours()) + ':' + (dateFormat.getMinutes() < 10 ?
        '0' + dateFormat.getMinutes() : dateFormat.getMinutes()) + ':' + (dateFormat.getSeconds() < 10 ?
          '0' + dateFormat.getSeconds() : dateFormat.getSeconds());
    // const title = 'Imprimé par : ' + this.userInfos.username + ' le  ' + dateValue + ' à ' + heureValue;
    const title = 'RESULTATS – ONG, Associations et autres';

    const ongCsv = [];

    const csvOptions = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: title,
      useBom: true,
      noDownload: false,
      headers: ['Nº', 'Noms des structures (Sigles)', 'Forme Juridique', 'Année de création', 'Nom du principal Dirigeant',
        'Téléphones', 'Couriel']
    };

    let index = 1;
    data.forEach(element => {

      ongCsv.push({
        Numero: index,
        Noms_Sigle: element.nom + ' (' + element.sigle + ')',
        Forme_juridique: element.forme_juridique.intitule,
        Annee_Creation: element.annee_creation,
        Responsable: element.prenom_principal_dirigeant + ' ' + element.nom_principal_dirigeant,
        Phone: element.telephone_principal_dirigeant,
        Email: element.courriel_principal_dirigeant
      });
      index++;
    });

    const nomArchiveCsv = 'archive_ong_csv_' + this.api.getTimePrefix();

    new AngularCsv(ongCsv, nomArchiveCsv, csvOptions);

  }



  // Format ong data to export
  formatOngDataOffline(data: any) {

    const zip = new JSZip();
    // Readme file detail
    zip.file('ReadMe.txt', 'Ce fichier repertoire contient la liste des documents des ong.');
    const pdf = zip.folder('cvs');

    let index = 1;
    data.forEach(element => {
      if (element && element.file_ong_association && element.file_ong_association.length > 0) {
        element.file_ong_association.forEach(fileData => {
          if (fileData.file_en_base_64) {
            pdf.file(`doc_${element.nom}_${index++}.pdf`, fileData.file_en_base_64, { base64: true });
          }
        });
      }
    });

    // Generate the zip file asynchronously
    const nomArchive = `archive_ong_cv_${this.api.getTimePrefix()}.zip`;
    zip.generateAsync({ type: 'blob' })
      .then(function (content) {
        // Force down of the Zip file
        saveAs(content, nomArchive);
      });

  }


  // Export data to PDF
  exportData() {

    switch (this.tabSelect) {

      case 1:
        this.impessionData = this.consultantsList;
        this.formatConsultantDataOffline(this.impessionData);
        break;

      case 2:
        this.impessionData = this.cabinetsList;
        this.formatCabinetDataOffline(this.impessionData);
        break;

      case 3:
        this.impessionData = this.ongList;
        this.formatOngDataOffline(this.impessionData);
        break;

      default:
        this.impessionData = this.consultantsList;
        this.formatConsultantDataOffline(this.impessionData);
        break;

    }

  }

}
