

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DashboardRoutes } from './dashboard-routing.module';
import { SharedModule } from './../shared/shared.module';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

import { DashboardComponent } from './dashboard.component';
import { ConsultantsComponent } from '../consultants/consultants.component';
import { CabinetsComponent } from '../cabinets/cabinets.component';
import { PdfViewerComponent } from '../pdf-viewer/pdf-viewer.component';
import { AddComponent } from '../add/add.component';
import { AddCabinetComponent } from '../add-cabinet/add-cabinet.component';
import { AddConsultantComponent } from '../add-consultant/add-consultant.component';
import { AddOngComponent } from '../add-ong/add-ong.component';
import { OngComponent } from '../ong/ong.component';
import { FilterConsultantComponent } from '../filter-consultant/filter-consultant.component';
import { FilterCabinetsComponent } from '../filter-cabinets/filter-cabinets.component';
import { FilterOngComponent } from '../filter-ong/filter-ong.component';
import { AddProfilComponent } from '../add-profil/add-profil.component';
import { AddDomainComponent } from '../add-domain/add-domain.component';
import { CabinetDetailComponent } from './../cabinet-detail/cabinet-detail.component';
import { ConsultantDetailComponent } from './../consultant-detail/consultant-detail.component';
import { OngDetailComponent } from './../ong-detail/ong-detail.component';




@NgModule({
  declarations: [
    DashboardComponent,
    ConsultantsComponent,
    CabinetsComponent,
    OngComponent,
    PdfViewerComponent,
    AddComponent,
    AddCabinetComponent,
    AddConsultantComponent,
    AddOngComponent,
    FilterConsultantComponent,
    FilterCabinetsComponent,
    FilterOngComponent,
    AddProfilComponent,
    AddDomainComponent,
    CabinetDetailComponent,
    ConsultantDetailComponent,
    OngDetailComponent
  ],
  imports: [
    PdfViewerModule,
    SharedModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff',
      secondaryColour: '#ffffff',
      tertiaryColour: '#ffffff'
    }),
    RouterModule.forChild(DashboardRoutes)
  ]
})
export class DashboardModule { }
