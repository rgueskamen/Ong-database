import { UserService } from './../../providers/user/user.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate {
  constructor(
    private router: Router,
    private authService: UserService
) { }

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.authService.getSession()) {
      // logged in so return true
         return true;
      }

      // not logged in so redirect to login page with the return url
      this.router.navigate(['/session/user/login'], { queryParams: { returnUrl: state.url }});
      return false;
   }

}
