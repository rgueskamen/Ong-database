import { DataService } from './../../providers/data/data.service';
import { ApiService } from './../../providers/api/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../providers/user/user.service';

@Component({
  selector: 'app-add-domain',
  templateUrl: './add-domain.component.html',
  styleUrls: ['./add-domain.component.scss']
})
export class AddDomainComponent implements OnInit {

  formAddDomain: FormGroup;
  intituleNotFound: boolean;
  intituleAlreadyExist: boolean;
  internetError: boolean;
  updateSuccess: boolean;

  constructor(
    private spinner: NgxSpinnerService,
    private api: ApiService,
    private data: DataService,
    private fb: FormBuilder,
    private userService: UserService
  ) {
      this.intituleNotFound = false;
      this.intituleAlreadyExist = false;
      this.updateSuccess = false;
      this.internetError = false;
  }

  ngOnInit() {
      this.initDomain();
      this.checkSession();
  }

  checkSession() {
    if (this.userService.getSession()) {
      this.api.refreshAccesToken();
    }
  }


    // init the user login form
    initDomain() {

      this.formAddDomain = this.fb.group({
        intitule: ['', Validators.required],
        description: ['']
      });
    }

  // Login the user
  addDomain(userData: any) {

    this.spinner.show('add-domain', {
      type: 'ball-spin-fade',
      size: 'large',
      bdColor: 'rgba(251,140,0, .8)',
      color: 'white'
    });

    this.updateSuccess = false;
    this.internetError = false;

    this.data.addDomaine(userData).then((reponse: any) => {
      
      if (reponse && reponse.message === 'success') {
          // send a message with profil data
          this.initDomain();
          this.data.sendDataMessage({message: 'domain', data: reponse.domaines});
          this.intituleNotFound = false;
          this.intituleAlreadyExist = false;
          this.updateSuccess = true;
          setTimeout(() => {
            this.updateSuccess = false;
          }, 60000);
          this.spinner.hide('add-domain');
      }  

    }).catch(error => {
      
        this.spinner.hide('add-domain');
        this.api.manageServerError(error);
        if (error.error && error.error.intitule_not_found) {
            this.intituleNotFound = true;
        }

        if (error.error && error.error.intitule_not_found) {
          this.intituleAlreadyExist = true;
        }
    });
}
}
