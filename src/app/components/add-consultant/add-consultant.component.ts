import { ConsultantService } from './../../providers/consultant/consultant.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DataService } from '../../providers/data/data.service';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { NgxSpinnerService } from 'ngx-spinner';
import { CustomValidators } from 'ng2-validation';
import { remote } from 'electron';
import { UserService } from '../../providers/user/user.service';


interface Domaine {
  domaine_id: number;
  nbre_consultation: number;
  nomDomaine: string;
}

interface CV {
  CV: string;
  filemime: string;
  filename: string;
}


@Component({
  selector: 'app-add-consultant',
  templateUrl: './add-consultant.component.html',
  styleUrls: ['./add-consultant.component.scss']
})
export class AddConsultantComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});

  domainesList: any;
  experienceList: any;
  profilList: any;
  statutList: any;
  formationsList: any;
  countriesList: any;
  disponibilites: any;
  juridictions: any;
  formConsultant: FormGroup;
  nbreConsultant: number;
  domaine: any;
  listCvs: CV[];
  listDomaines: Domaine[];
  recommandationsList: any;
  fillallfieldError: boolean;
  statutIdError: boolean;
  disponibilyError: boolean;
  recommandationError: boolean;
  levelError: boolean;
  profilError: boolean;
  pdfAddError: boolean;
  experienceError: boolean;
  domainError: boolean;
  doctypeError: boolean;
  bottomup: boolean;
  @Output () cancelled = new EventEmitter<string>();

  constructor(
    private api: ApiService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private consultant: ConsultantService,
    private route: Router,
    private data: DataService,
    private userService: UserService
  ) {

    this.domainesList = [];
    this.experienceList = [];
    this.profilList = [];
    this.statutList = [];
    this.formationsList = [];
    this.recommandationsList = [];
    this.countriesList = [];
    this.nbreConsultant = 0 ;
    this.domaine = '';
    this.listDomaines = [];
    this.listCvs = [];
    this.fillallfieldError = false;
    this.statutIdError = false;
    this.disponibilyError = false;
    this.recommandationError = false;
    this.levelError = false;
    this.profilError = false;
    this.experienceError = false;
    this.domainError = false;
    this.pdfAddError = false;
    this.doctypeError = false;
    this.bottomup = false;
    this.getNewProfilOrDomain();
   }

  ngOnInit() {

    this.getAllDomaines();
    this.getAllExperiences();
    this.getAllProfil();
    this.getAllFormations();
    this.getAllRecommandations();
    this.initConsultantForm();
    this.getCountries();
    this.getDisponibilites();
    this.getJuridictions();
    this.getAllStatus();
    this.checkSession();
  }

      // permet d'ajouter les consultants
      initConsultantForm() {

        const currentDate = new Date();
        const currentDateFormat =  this.formatDate1(currentDate);

        this.formConsultant = this.fb.group({
          statut_id: [null],
          disponibilite_id: [null],
          recommandation_id: [null],
          recommandation_name: [''],
          nom: [null],
          prenom: [null],
          dateNaissance_view: [null, Validators.compose([CustomValidators.maxDate(currentDateFormat)])],
          dateNaissance: [null],
          sexe: [null],
          pays: [null],
          ville: [null],
          niveau_formation_id: [null],
          liste_profil: [null],
          liste_profil_view: [null],
          date1ereExperiencePro_view: [currentDateFormat, Validators.compose([CustomValidators.maxDate(currentDateFormat)])],
          date1ereExperienceEnTantQueConsultant_view: [currentDateFormat,
          Validators.compose([CustomValidators.maxDate(currentDateFormat)])],
          date1ereExperiencePro: [null],
          date1ereExperienceEnTantQueConsultant: [null],
          liste_experience_geographique: [null],
          liste_experience_geographique_view: [null],
          experienceAvecCooperationAllemande: [null],
          experienceAvecLesNationsUnies: [null],
          experienceAvecLesAutresPartenaires: [null],
          experienceAvecCooperationAllemande_view: [null],
          experienceAvecLesNationsUnies_view: [null],
          experienceAvecLesAutresPartenaires_view: [null],
          liste_domaine_expertise: [[]],
          adresse: [null],
          liste_telephone: [null],
          liste_telephone_view: [null],
          liste_courriel: [null],
          liste_courriel_view: [null],
          liste_CV: [null]
        }, {validator: [this.checkDate('dateNaissance_view', 'date1ereExperiencePro_view'),
        this.checkDate('dateNaissance_view', 'date1ereExperienceEnTantQueConsultant_view')]});

     }

     checkSession() {
      if (this.userService.getSession()) {
        this.api.refreshAccesToken();
      }
    }
  

  // check if date is valid
  checkDate(fisrtDate: string, secondDate: string) {
    return (group: FormGroup) => {
      const fisrtDateInput = group.controls[fisrtDate];
      const secondDateInput = group.controls[secondDate];
      if (secondDateInput.value <= fisrtDateInput.value) {
        return secondDateInput.setErrors({notMatch: true});
      } else {
        return secondDateInput.setErrors(null);
      }
    };
  }

      // Listen to messaga
      getNewProfilOrDomain() {
        this.data.getDataMessage().subscribe(reponse=> {
            if (reponse && reponse.message) {
                if (reponse.message === 'profil') {
                  this.profilList.push(reponse.data);
                } else if (reponse.message === 'domain') {
                  this.domainesList.push(reponse.data);
                }
            }
        });
    }

    // Back button
    backButton() {
      this.cancelled.emit('cancel');
    }

    // Scroll on the top
    scrollTop() {
      // Hack: Scrolls to top of Page after page view initialized
      let top = document.getElementById('top');
      this.bottomup = true;
      if (top !== null) {
        top.scrollIntoView();
        top = null;
      }

      // animation duration
      setTimeout(() => {
        this.bottomup = false;
      }, 5000);
    }

  // Cancel the form
  cancel () {
     this.initConsultantForm();
  }

         // show the confirmation dialog cancel
         confirmDialogCancel() {
          const dialog = remote.dialog;
          dialog.showMessageBox({
            type: 'none',
            title: 'Confirmation',
            message: 'Voulez-vous annuler l\'ajout du consultant ?',
            buttons: ['Non', 'Oui']
          }, (reponse) => {
                if (reponse === 0) {
                  return;
                } else {
                  this.initConsultantForm();
                }
          });
        }

  // Get all countries
  getCountries() {

        this.api.getAllCountries().then(countries => {
            this.countriesList = countries;
        }).catch(error => {
            this.api.manageServerError(error);
        });
  }

   // Get all disponibilites
   getDisponibilites () {
    this.data.getAllDisponibilities().then((disponibilites: any) => {
      this.disponibilites = disponibilites.disponibilite;
    }).catch(error => {
       this.api.manageServerError(error);
    });
  }

  // Get all disponibilites
  getJuridictions () {
      this.data.getAllJuridictions().then((juridictions: any) => {
        this.juridictions = juridictions.forme_juridique;
      }).catch(error => {
         this.api.manageServerError(error);
      });
  }

   // Get the list of status
  getAllStatus() {
        this.data.getAllStatut().then((status: any) => {

          if (status.message === 'success') {
            this.statutList = status.statut;
          }
        }).catch(error => {
          this.api.manageServerError(error);
        });
    }


  // Get the list of domanines
  getAllDomaines() {
      this.data.getAllDomaines().then((domaines: any) => {

        if (domaines.message === 'success') {
          this.domainesList = domaines.domaines;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

    // Get the list of experiences
    getAllExperiences() {
      this.data.getAllGeographiqueExperience().then((experiences: any) => {
        if (experiences.message === 'success') {
          this.experienceList =  experiences.experience_geographique;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

   // Get the list of profil
    getAllProfil() {
        this.data.getAllProfils().then((profils: any) => {
          if (profils.message === 'success') {
            this.profilList =  profils.profil;
          }
        }).catch(error => {
          this.api.manageServerError(error);
        });
     }


    // Get the list of formations
    getAllFormations() {
      this.data.getAllFormations().then((formations: any) => {

        if (formations.message === 'success') {
          this.formationsList =  formations.niveau_formation;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

        // Get the list of recommandations
        getAllRecommandations() {
          this.data.getAllRecommandations().then((recommandations: any) => {

            if (recommandations.message === 'success') {
              this.recommandationsList =  recommandations.recommandation;
            }

          }).catch(error => {
            this.api.manageServerError(error);
          });
       }


       // add un domaine
       addDomaine() {
        this.listDomaines.push({domaine_id: parseInt( this.domainesList[this.domaine].id, 10), nbre_consultation : this.nbreConsultant,
          nomDomaine: this.domainesList[this.domaine].intitule });
          this.formConsultant.controls['liste_domaine_expertise'].setValue(this.listDomaines);
          this.domaine = '';
          this.nbreConsultant = 0;
       }

       // Remove the selected domaine
      removeDomaine(index: number) {
        this.listDomaines.splice(index, 1);
      }

      // format liste profil
      formatProfil(profil: string) {

          let listProfil: string[] = [];
          const resultat = [];

          if (profil) {

            listProfil = profil.split(',');
            listProfil.forEach(profilId => {
                if (profilId) {
                  resultat.push({profil_id: profilId});
                }
            });

          }

          this.formConsultant.controls['liste_profil'].setValue(resultat);
      }

      // format liste experience
      formatExperience(experience: string) {

          let listExperience: string[] = [];
          const resultat = [];

          if (experience) {

            listExperience = experience.split(',');
            listExperience.forEach(experienceId => {
                if (experienceId) {
                  resultat.push({experience_geographique_id: experienceId});
                }
            });

          }

          this.formConsultant.controls['liste_experience_geographique'].setValue(resultat);
       }


       // format phone
      formatPhone(phone: string) {

        let listPhones: string[] = [];
        const resultat = [];

        if (phone) {

          listPhones = phone.split(',');
          listPhones.forEach(phoneData => {
              if (phoneData) {
                resultat.push({telephone: phoneData});
              }
          });

        }

        this.formConsultant.controls['liste_telephone'].setValue(resultat);
     }


     // format phone
     formatCouriel(couriels: string) {
       let listCouriel: string[] = [];
       const resultat = [];
        if (couriels) {
          listCouriel = couriels.split(',');
          listCouriel.forEach(couriel => {
            if (couriel) {
              resultat.push({email: couriel});
            }
           });
         }
         this.formConsultant.controls['liste_courriel'].setValue(resultat);
      }

      // Format date  aaaa-mm-jj
      formatDate1(date: any) {
        const dateFormat  = new Date(date);
        const month  = (dateFormat.getMonth() + 1) < 10 ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
        const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
        return dateFormat.getFullYear() + '-' + month + '-' + day;
      }


      // Format date aaaa/mm/jj
      formatDate2(date: any) {
        const dateFormat  = new Date(date);
        const month  = (dateFormat.getMonth() + 1) < 10 ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
        const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
        return dateFormat.getFullYear() + '/' + month + '/' + day;
      }


     // Add a cv
      addCv(cvdata: any) {
        this.listCvs.push({CV: cvdata.data, filemime: cvdata.filemime, filename: cvdata.filename});
        this.formConsultant.controls['liste_CV'].setValue(this.listCvs);
      }

      // remove cv
      removeCv(index: number) {
        this.listCvs.splice(index, 1);
        this.uploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});
      }


   // Update upload the pdf
  onFilePdf(event) {
    this.pdfAddError = false;
    this.doctypeError = false;
    this.data.pdfReader(event).then(pdf => {
      if (pdf && pdf.filemime === 'application/pdf') {
        this.addCv(pdf);
      } else {
          // alert('Veuillez choisir un document au format PDF');
          this.doctypeError = true;
      }
    }).catch(error => {
      this.pdfAddError = true;
    });
   }


   // show the checkbox/radio value
  checkExperience(dataValue: any) {
  }


  // add a consultant
  addConsultant(consulatantData: any) {

        this.spinner.show('consultant', {
          type: 'ball-spin-fade',
          size: 'large',
          bdColor: 'rgba(251,140,0, .8)',
          color: 'white'
        });

        this.formConsultant.get('recommandation_id').setValue(this.formConsultant.value.recommandation_id ? this.formConsultant.value.recommandation_id : 'NULL');
        this.formConsultant.get('recommandation_name').setValue(this.formConsultant.value.recommandation_name ? this.formConsultant.value.recommandation_name : 'NULL');

        const data = consulatantData;

        data.experienceAvecCooperationAllemande_view ? this.formConsultant.controls['experienceAvecCooperationAllemande'].setValue(1)
       : this.formConsultant.controls['experienceAvecCooperationAllemande'].setValue(0);

       data.experienceAvecLesNationsUnies_view ? this.formConsultant.controls['experienceAvecLesNationsUnies'].setValue(1)
       : this.formConsultant.controls['experienceAvecLesNationsUnies'].setValue(0);

       data.experienceAvecLesAutresPartenaires_view ? this.formConsultant.controls['experienceAvecLesAutresPartenaires'].setValue(1)
       : this.formConsultant.controls['experienceAvecLesAutresPartenaires'].setValue(0);

        this.formatProfil(data.liste_profil_view);
        this.formatExperience(data.liste_experience_geographique_view);
        this.formatPhone(data.liste_telephone_view);
        this.formatCouriel(data.liste_courriel_view);
        if (data.dateNaissance_view) {
          this.formConsultant.controls['dateNaissance'].setValue(
            this.formatDate2(data.dateNaissance_view));
        } else {
          this.formConsultant.controls['dateNaissance'].setValue(null);
        }

        if (data.date1ereExperiencePro_view) {
          this.formConsultant.controls['date1ereExperiencePro'].setValue(this.formatDate2(data.date1ereExperiencePro_view));
        } else {
          this.formConsultant.controls['date1ereExperiencePro'].setValue(null);
        }

        if (data.date1ereExperienceEnTantQueConsultant_view) {
          this.formConsultant.controls['date1ereExperienceEnTantQueConsultant'].setValue(
            this.formatDate1(data.date1ereExperienceEnTantQueConsultant_view));
        } else {
          this.formConsultant.controls['date1ereExperienceEnTantQueConsultant'].setValue(null);
        }
 
  
        this.consultant.saveConsultant(this.formConsultant.value).then((response: any) => {

              if (response && response.message === 'success') {
                    this.consultant.sendMessage('new-consultant');
                    setTimeout(() => {
                      this.spinner.hide('consultant');
                      this.route.navigateByUrl('/dashboard/dashboard-v1/1');
                    },15000);
                   
              } else {
                   this.spinner.hide('consultant');
              }
     

        }).catch(error => {

             this.spinner.hide('consultant');
             alert('Essayer encore svp !');

                if (error && error.message === 'error') {

                  if (error.remplir_tous_les_champs) {
                       // Veuillez remplir tous les champs
                       this.fillallfieldError = true;
                  }

                  if (error.statut_id_not_exist) {
                    // Ce statut n'existe pas
                    this.statutIdError = true;

                  }

                  if (error.disponibilite_id_not_exist) {
                    // cette disponibilité n'existe pas
                    this.disponibilyError = true;

                  }

                  if (error.recommandation_id_not_exist) {
                    // Cette recommandation n'existe pas
                    this.recommandationError = true;
                  }

                  if (error.niveau_formation_id_not_exist) {
                     // Cette niveau n'existe pas
                     this.levelError = true;
                  }

                  if (error.profil_id_not_exist) {
                      // Ce profil n'existe pas
                      this.profilError = true;

                  }


                  if (error.experience_geographique_id_not_exist) {
                      // Cette experience n'existe pas
                      this.experienceError = false;

                  }

                  if (error.domaine_id_not_exist) {
                    // Ce domaine n'existe pas
                    this.domainError = false;

                  }

                } else {
                   this.api.manageServerError(error);
                }

        });

      }

   }
