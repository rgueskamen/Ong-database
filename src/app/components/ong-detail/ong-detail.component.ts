import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from './../../providers/api/api.service';
import { OngService } from './../../providers/ong/ong.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../providers/data/data.service';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CustomValidators } from 'ng2-validation';
import { remote } from 'electron';
import { UserService } from '../../providers/user/user.service';


interface Domaine {
  domaine_id: number;
  nbre_consultation: number;
  nomDomaine: string;
}

interface Documents {
  file: string;
  filemime: string;
  filename: string;
}


@Component({
  selector: 'app-ong-detail',
  templateUrl: './ong-detail.component.html',
  styleUrls: ['./ong-detail.component.scss']
})
export class OngDetailComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});

  domainesList: any;
  formOng: FormGroup;
  experienceList: any;
  disponibilites: any;
  statutList: any;
  profilList: any;
  domaine: string;
  formationsList: any;
  listCvs: Documents[];
  listDomaines: Domaine[];
  nbreConsultant: number;
  juridictions: any;
  recommandationsList: any;
  fillallfieldError: boolean;
  statutIdError: boolean;
  disponibilyError: boolean;
  recommandationError: boolean;
  formeJuridiqueError: boolean;
  profilError: boolean;
  pdfAddError: boolean;
  experienceError: boolean;
  domainError: boolean;
  doctypeError: boolean;
  @Output() cancelled = new EventEmitter<string>();
  ongs: any;
  userRole: any;
  userRoleButton: any;
  updateSuccess: boolean;
  internetError: boolean;
  bottomup: boolean;

  constructor(
    private api: ApiService,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private router: ActivatedRoute,
    private userService: UserService,
    private route: Router,
    private ongService: OngService,
    private data: DataService
  ) {
    this.domainesList = [];
    this.experienceList = [];
    this.profilList = [];
    this.formationsList = [];
    this.recommandationsList = [];
    this.statutList = [];
    this.disponibilites = [];
    this.juridictions = [];
    this.listDomaines = [];
    this.listCvs = [];
    this.domaine = '';
    this.nbreConsultant = 0;
    this.fillallfieldError = false;
    this.statutIdError = false;
    this.disponibilyError = false;
    this.recommandationError = false;
    this.formeJuridiqueError = false;
    this.profilError = false;
    this.experienceError = false;
    this.domainError = false;
    this.pdfAddError = false;
    this.doctypeError = false;
    this.updateSuccess = false;
    this.internetError = false;
    this.bottomup = false;
    this.userRole = 'user';
    this.userRoleButton = localStorage.getItem('user-role') ?  JSON.parse(localStorage.getItem('user-role')) : null;
  }

  ngOnInit() {

    this.getAllDomaines();
    this.getAllExperiences();
    this.getAllProfil();
    this.getAllRecommandations();
    this.getAllStatus();
    this.getDisponibilites();
    this.getJuridictions();
    const key  = this.router.snapshot.params.key;
    this.ongs = JSON.parse(localStorage.getItem(key));
    this.initOngForm(this.ongs);
    this.checkSession();
  }


    // Edit Informations
    editInformations() {
      this.userRole === 'user' ? this.userRole = 'admin' : this.userRole = 'user';
    }

    checkSession() {
      if (this.userService.getSession()) {
        this.api.refreshAccesToken();
      }
    }


  // Cancel the page
  cancel () {
      this.listCvs = [];
      this.listDomaines = [];
      this.initOngForm(this.ongs);
  }

       // Scroll on the top
       scrollTop() {
        // Hack: Scrolls to top of Page after page view initialized
        let top = document.getElementById('top');
        this.bottomup = true;
        if (top !== null) {
          top.scrollIntoView();
          top = null;
        }

        // animation duration
        setTimeout(() => {
          this.bottomup = false;
        }, 5000);
      }

     // show the confirmation dialog cancel
     confirmDialogCancel() {
      const dialog = remote.dialog;
      dialog.showMessageBox({
        type: 'none',
        title: 'Confirmation',
        message: 'Voulez-vous annuler les modifications ?',
        buttons: ['Non', 'Oui']
      }, (reponse) => {
            if (reponse === 0) {
              return;
            } else {
              this.cancel();
            }
      });
    }

   // Back to the list of ong
   backButton() {
    this.route.navigateByUrl('/dashboard/dashboard-v1/3');
  }

     // parse the date format 'aaaa'
     parseDate(date: any) {
      const formatDate = new Date(date);
      const month =  formatDate.getMonth() < 9 ? '0' + (formatDate.getMonth() + 1) : (formatDate.getMonth() + 1);
      const day  =  formatDate.getDate() < 10 ? '0' + formatDate.getDate() : formatDate.getDate();
      const resultat = formatDate.getFullYear() + '-' + month + '-' + day;
      return formatDate.getFullYear() + '-' + month + '-' + day;
    }



    // Parse the list of domain
    parseDomaine(data: any) {
      if (data.domaine_expertise_users) {
        data.domaine_expertise_users.forEach(domain => {
          this.listDomaines.push({domaine_id: parseInt(domain.id, 10), nbre_consultation : domain.nbre_consultation,
            nomDomaine: domain.intitule});
        });
      }
      this.formOng.controls['liste_domaine_expertise'].setValue(this.listDomaines);
      this.domaine = '';
      this.nbreConsultant = 0;
   }

   // parse phone data
   parsePhoneData(data: any) {
    let phone = '';
    if (data.contact_users) {
      data.contact_users.forEach(tel => {
        phone ? phone += ',' + tel.telephone : phone = tel.telephone;
      });
    }
    return phone;
   }

   // parse phone data
  parseEmailData(data: any) {
     let phone = '';
     if(data.courriel_users) {
      data.courriel_users.forEach(mail => {
        phone ? phone += ',' + mail.email : phone = mail.email;
      });
     }
      return phone;
   }

   // Add a cv
   parseDocument(docdata: any) {
     if (docdata.file_ong_association) {
      docdata.file_ong_association.forEach(file => {
        const name = file.file.split('/') || [];
        this.listCvs.push({file: file.file_en_base_64, filemime: 'application/pdf', filename: name.length > 0 ? name[name.length - 1] : ''});
      });
     }
    this.formOng.controls['liste_files'].setValue(this.listCvs);
  }

      // permet d'ajouter les cabinets
      initOngForm(data: any) {
        const currentDate = new Date();
        const currentDateFormat = this.formatDate1(currentDate);
        this.formOng = this.fb.group({
          ong_id: [data.ong_association_id >= 0 ? data.ong_association_id : null],
          statut_id: [data.statut && data.statut.id >= 0 ? data.statut.id : null],
          disponibilite_id: [data.disponibilite && data.disponibilite.id >= 0 ?  data.disponibilite.id : null],
          recommandation_id: [data.recommandation && data.recommandation.id >= 0 ? data.recommandation.id : null],
          recommandation_name: [data.recommandation && data.recommandation.intitule ? data.recommandation.intitule : null],
          nom: [data.nom ? data.nom : null],
          sigle: [data.sigle ? data.sigle : null],
          forme_juridique_id: [data.forme_juridique && data.forme_juridique.id >= 0 ? data.forme_juridique.id : null],
          annee_creation_view: [data.annee_creation ? this.parseDate(data.annee_creation) : null, Validators.compose([
            CustomValidators.maxDate(currentDateFormat)])],
          annee_creation: [null],
          registre_commerce: [data.registre_commerce ? data.registre_commerce : null],
          siege_social: [data.siege_social ? data.siege_social : null],
          liste_profil_view: [data.profil_users && data.profil_users[0] && data.profil_users[0].id >= 0 ? data.profil_users[0].id : null],
          liste_profil: [null],
          date1ereExperiencePro_view: [null],
          date1ereAnneeExperienceEnTantONG_view: [data.date1ereAnneeExperienceEnTantONG ? this.parseDate(data.date1ereAnneeExperienceEnTantONG) : null,
          Validators.compose([CustomValidators.maxDate(currentDateFormat)])],
          date1ereExperiencePro: [null],
          date1ereAnneeExperienceEnTantQueCabinet: [null],
          liste_experience_geographique_view: [data.experience_geographique_users && data.experience_geographique_users[0] && data.experience_geographique_users[0].id >= 0 ? data.experience_geographique_users[0].id : null],
          liste_experience_geographique: [null],
          experienceAvecCooperationAllemande_view: [data.experienceAvecCooperationAllemande && data.experienceAvecCooperationAllemande === 1 ? true : null ],
          experienceAvecLesNationsUnies_view: [data.experienceAvecLesNationsUnies && data.experienceAvecLesNationsUnies === 1 ? true : null],
          experienceAvecLesAutresPartenaires_view: [data.experienceAvecLesAutresPartenaires && data.experienceAvecLesAutresPartenaires === 1 ? true : null],
          experienceAvecCooperationAllemande: [null],
          experienceAvecLesNationsUnies: [null],
          experienceAvecLesAutresPartenaires: [null],
          liste_domaine_expertise: [[]],
          adresse: [data.adresse ? data.adresse : null],
          liste_telephone_view: [ data ? this.parsePhoneData(data) : null],
          liste_telephone: [null],
          liste_courriel_view: [data ? this.parseEmailData(data) : null],
          liste_courriel: [null],
          nom_principal_dirigeant: [data.nom_principal_dirigeant ? data.nom_principal_dirigeant : null],
          prenom_principal_dirigeant: [data.prenom_principal_dirigeant ? data.prenom_principal_dirigeant : null],
          telephone_principal_dirigeant: [data.telephone_principal_dirigeant ? data.telephone_principal_dirigeant : null],
          courriel_principal_dirigeant: [data.courriel_principal_dirigeant ? data.courriel_principal_dirigeant : null],
          liste_files: [null]
        }, {validator: [this.checkDate('annee_creation_view', 'date1ereAnneeExperienceEnTantONG_view')]});
        this.parseDomaine(data);
        this.parseDocument(data);
     }


      // check if date is valid
  checkDate(fisrtDate: string, secondDate: string) {
    return (group: FormGroup) => {
      const fisrtDateInput = group.controls[fisrtDate];
      const secondDateInput = group.controls[secondDate];
      if (secondDateInput.value <= fisrtDateInput.value) {
        return secondDateInput.setErrors({notMatch: true});
      } else {
        return secondDateInput.setErrors(null);
      }
    };
  }


     // Get the list of status
     getAllStatus() {
          this.data.getAllStatut().then((status: any) => {

            if (status.message === 'success') {
              this.statutList = status.statut;
            }
          }).catch(error => {
            this.api.manageServerError(error);
          });
      }

    // Get the list of domanines
    getAllDomaines() {
      this.data.getAllDomaines().then((domaines: any) => {

        if (domaines.message === 'success') {
          this.domainesList =  domaines.domaines;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

    // Get the list of experiences
    getAllExperiences() {
      this.data.getAllGeographiqueExperience().then((experiences: any) => {
        if (experiences.message === 'success') {
          this.experienceList =  experiences.experience_geographique;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

   // Get the list of profil
    getAllProfil() {
        this.data.getAllProfils().then((profils: any) => {
          if (profils.message === 'success') {
            this.profilList = profils.profil;
          }
        }).catch(error => {
          this.api.manageServerError(error);
        });
     }

        // Get the list of recommandations
        getAllRecommandations() {
          this.data.getAllRecommandations().then((recommandations: any) => {

            if (recommandations.message === 'success') {
              this.recommandationsList = recommandations.recommandation;
            }

          }).catch(error => {
            this.api.manageServerError(error);
          });
       }


          // Get all disponibilites
   getDisponibilites () {
    this.data.getAllDisponibilities().then((disponibilites: any) => {
      this.disponibilites = disponibilites.disponibilite;
    }).catch(error => {
       this.api.manageServerError(error);
    });
  }

    // Get all disponibilites
    getJuridictions () {
      this.data.getAllJuridictions().then((juridictions: any) => {
        this.juridictions = juridictions.forme_juridique;
      }).catch(error => {
         this.api.manageServerError(error);
      });
    }




       // add un domaine
       addDomaine() {
        this.listDomaines.push({domaine_id: parseInt( this.domainesList[this.domaine].id, 10), nbre_consultation : this.nbreConsultant,
          nomDomaine: this.domainesList[this.domaine].intitule });
          this.formOng.controls['liste_domaine_expertise'].setValue(this.listDomaines);
          this.domaine = '';
          this.nbreConsultant = 0;
       }

       // Remove the selected domaine
      removeDomaine(index: number) {
        this.listDomaines.splice(index, 1);
      }

      // format liste profil
      formatProfil(profil: string) {

          let listProfil: string[] = [];
          const resultat = [];

          if (profil) {

            const profilData = profil + ',';
            listProfil = profilData.split(',');
            listProfil.forEach(profilId => {
                if (profilId) {
                  resultat.push({profil_id: profilId});
                }
            });

          }

          this.formOng.controls['liste_profil'].setValue(resultat);
      }

      // format liste experience
      formatExperience(experience: string) {

          let listExperience: string[] = [];
          const resultat = [];

          if (experience) {

            const experienceData = experience + ',';
            listExperience = experienceData.split(',');
            listExperience.forEach(experienceId => {
                if (experienceId) {
                  resultat.push({experience_geographique_id: experienceId});
                }
            });

          }

          this.formOng.controls['liste_experience_geographique'].setValue(resultat);
       }


       // format phone
      formatPhone(phone: string) {

        let listPhones: string[] = [];
        const resultat = [];

        if (phone) {

          const phoneData = phone + ',';
          listPhones = phoneData.split(',');
          listPhones.forEach(phon => {
              if (phon) {
                resultat.push({telephone: phon});
              }
          });

        }

        this.formOng.controls['liste_telephone'].setValue(resultat);
     }


     // format phone
     formatCouriel(couriels: string) {
       let listCouriel: string[] = [];
       const resultat = [];
        if (couriels) {
          const courielsData = couriels + ',';
          listCouriel = courielsData.split(',');
          listCouriel.forEach(couriel => {
            if (couriel) {
              resultat.push({email: couriel});
            }
           });
         }
         this.formOng.controls['liste_courriel'].setValue(resultat);
      }
      // Format date  aaaa-mm-jj
      formatDate1(date: any) {
        const dateFormat  = date ? new Date(date) : new Date();
        const month  = (dateFormat.getMonth() + 1) < 10 ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
        const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
        return dateFormat.getFullYear() + '-' + month + '-' + day;
      }


      // Format date aaaa
      formatDate2(date: any) {
        const dateFormat  = date ? new Date(date) : new Date();
        return dateFormat.getFullYear();
      }

     // Add a cv
      addDocument(docdata: any) {
        this.listCvs.push({file: docdata.data, filemime: docdata.filemime, filename: docdata.filename});
        this.formOng.controls['liste_files'].setValue(this.listCvs);
      }

      // remove cv
      removeDocument(index: number) {
        this.listCvs.splice(index, 1);
        this.uploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});
      }

   // Update upload the pdf
  onFilePdf(event) {
    this.pdfAddError = false;
    this.doctypeError = false;
    this.data.pdfReader(event).then(pdf => {
      if (pdf && pdf.filemime === 'application/pdf') {
        this.addDocument(pdf);
      } else {
         // alert('Veuillez choisir un document au format PDF');
          this.doctypeError = true;
      }
    }).catch(error => {
      this.pdfAddError = true;
    });
   }
        // show the confirmation dialog cancel
        confirmDialogUpdate(cabinetData: any) {
          const dialog = remote.dialog;
          dialog.showMessageBox({
            type: 'none',
            title: 'Confirmation',
            message: 'Voulez-vous enregistrer les modifications ?',
            buttons: ['Non', 'Oui']
          }, (reponse) => {
                if (reponse === 0) {
                  return;
                } else {
                  this.updateOng(cabinetData);
                }
          });
        }

      // update the ong
      updateOng(cabinetData: any) {

        this.spinner.show('ong', {
          type: 'ball-spin-fade',
          size: 'large',
          bdColor:'rgba(251,140,0, .8)',
          color: 'white'
        });

        this.formOng.get('recommandation_id').setValue(this.formOng.value.recommandation_id ? this.formOng.value.recommandation_id : 'NULL');
        this.formOng.get('recommandation_name').setValue(this.formOng.value.recommandation_name ? this.formOng.value.recommandation_name : 'NULL');

        const data = cabinetData;

        data.experienceAvecCooperationAllemande_view ? this.formOng.controls['experienceAvecCooperationAllemande'].setValue(1)
        : this.formOng.controls['experienceAvecCooperationAllemande'].setValue(0);

        data.experienceAvecLesNationsUnies_view ? this.formOng.controls['experienceAvecLesNationsUnies'].setValue(1)
        : this.formOng.controls['experienceAvecLesNationsUnies'].setValue(0);

        data.experienceAvecLesAutresPartenaires_view ? this.formOng.controls['experienceAvecLesAutresPartenaires'].setValue(1)
        : this.formOng.controls['experienceAvecLesAutresPartenaires'].setValue(0);

       this.formatProfil(data.liste_profil_view);
        this.formatExperience(data.liste_experience_geographique_view);
        this.formatPhone(data.liste_telephone_view);
        this.formatCouriel(data.liste_courriel_view);
        this.formOng.controls['annee_creation'].setValue(this.formatDate2(data.annee_creation_view));
        this.formOng.controls['date1ereExperiencePro'].setValue(this.formatDate1(data.date1ereExperiencePro_view));
        this.formOng.controls['date1ereAnneeExperienceEnTantQueCabinet'].setValue(
        this.formatDate1(data.date1ereAnneeExperienceEnTantONG_view));

        this.ongService.updateOngAssociation(this.formOng.value).then((response: any) => {
              if (response && response.message === 'success') {
                    // Show a message that the ong  updated
                   this.ongService.sendMessage('update-ong');
                   this.scrollTop();
                   this.updateSuccess = true;
                   setTimeout(() => {
                      this.updateSuccess = false;
                   }, 5000);
                   this.spinner.hide('ong');
              } 

        }).catch(error => {


               this.spinner.hide('ong');

               alert('Essayer encore svp !');

                if (error && error.error) {

                  if (error.error.remplir_tous_les_champs) {
                       // Veuillez remplir tous les champs
                       this.fillallfieldError = true;
                  }

                  if (error.error.statut_id_not_exist) {
                    // Ce statut n'existe pas
                    this.statutIdError = true;
                 }

                  if (error.error.disponibilite_id_not_exist) {
                    // cette disponibilité n'existe pas
                    this.disponibilyError = true;

                  }

                  if (error.error.recommandation_id_not_exist) {
                    // Cette recommandation n'existe pas
                    this.recommandationError = true;
                  }

                  if (error.error.forme_juridique_id_not_exist) {
                     // Cette forme juridique n'existe pas
                     this.formeJuridiqueError = true;
                  }

                  if (error.error.profil_id_not_exist) {
                      // Ce profil n'existe pas
                      this.profilError = true;

                  }


                  if (error.error.experience_geographique_id_not_exist) {
                      // Cette experience n'existe pas
                      this.experienceError = false;

                  }
                if (error.error.domaine_id_not_exist) {
                    // Ce domaine n'existe pas
                    this.domainError = false;

                  }

                } else {
                   this.api.manageServerError(error);
                }

        });

      }
}
