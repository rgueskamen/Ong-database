import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../providers/data/data.service';

interface Category {
  choice: boolean;
  name: string;
  value: string;
}

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  categoryList: Category[];
  activePannel: string;
  domainesList: any;
  experienceList: any;
  profilList: any;
  formationsList: any;
  recommandationsList: any;
  selectedCategory: string;

  constructor(
    private api: ApiService,
    private data: DataService,
    private route: Router
  ) {

    this.activePannel = 'option';
    this.categoryList = [];
    this.categoryList.push({choice: false, name: 'Consultant Individuel', value: 'consultant'});
    this.categoryList.push({choice: false, name: 'Cabinet/Bureau d\'Etudes', value: 'cabinet'});
    this.categoryList.push({choice: false, name: 'ONG/Association', value: 'ong'});
    this.domainesList = [];
    this.experienceList = [];
    this.profilList = [];
    this.formationsList = [];
    this.recommandationsList = [];
    this.selectedCategory = '';
  }

  ngOnInit() {

    this.getAllDomaines();
    this.getAllExperiences();
    this.getAllProfil();
    this.getAllFormations();
    this.getAllRecommandations();

  }


  // back button
  backButton() {
      this.route.navigate(['/dashboard/dashboard-v1/1']);
  }

  // Cancel de selection
  cancel () {
    for (let i = 0; i < this.categoryList.length; i++) {
        this.categoryList[i].choice = false;
    }
  }

  // Cancel consulltant event
  handleConsultant(event: any) {
    if (event === 'cancel') {
      this.activePannel = 'option';
    }
  }

  // Cancel cabinet event
  handleCabinet(event: any) {
    if (event === 'cancel') {
      this.activePannel = 'option';
    }
  }


  // Cancel ong event
  handleOng(event: any) {
    if (event === 'cancel') {
      this.activePannel = 'option';
    }
  }



  // Go to the next Panel
  nextPanel() {
    this.activePannel = 'registration';
    this.categoryList.forEach(category => {
        if (category.choice) {
             this.selectedCategory = category.value;
             this.cancel();
        }
    });
  }




    // Get the list of domanines
    getAllDomaines() {
      this.data.getAllDomaines().then((domaines: any) => {

        if (domaines.message === 'success') {
           domaines.domaines.forEach(element => {
            this.domainesList.push({choice: false, categorie: 'experience', value: element.id, name: element.intitule, data: element});
          });
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

    // Get the list of experiences
    getAllExperiences() {
      this.data.getAllGeographiqueExperience().then((experiences: any) => {
        if (experiences.message === 'success') {
         experiences.experience_geographique.forEach(element => {
          this.experienceList.push({choice: false, categorie: 'experience', value: element.id, name: element.intitule, data: element});
          });
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

   // Get the list of profil
    getAllProfil() {
        this.data.getAllProfils().then((profils: any) => {
          if (profils.message === 'success') {
            profils.profil.forEach(element => {
              this.profilList.push({choice: false, categorie: 'experience', value: element.id, name: element.intitule, data: element});
            });
          }
        }).catch(error => {
          this.api.manageServerError(error);
        });
     }


    // Get the list of formations
    getAllFormations() {
      this.data.getAllFormations().then((formations: any) => {

        if (formations.message === 'success') {
          formations.niveau_formation.forEach(element => {
            this.formationsList.push({choice: false, categorie: 'experience', value: element.id, name: element.intitule, data: element});
          });
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

        // Get the list of recommandations
        getAllRecommandations() {
          this.data.getAllRecommandations().then((recommandations: any) => {

            if (recommandations.message === 'success') {
             recommandations.recommandation.forEach(element => {
                this.recommandationsList.push({choice: false, categorie: 'experience', value: element.id, name: element.intitule,
                 data: element});
              });
            }

          }).catch(error => {
            this.api.manageServerError(error);
          });
       }





}
