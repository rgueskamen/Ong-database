import { CabinetService } from './../../providers/cabinet/cabinet.service';
import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit, Input, Output, EventEmitter, NgZone } from '@angular/core';


interface Filter {
  choice: boolean;
  categorie: string;
  value: any;
  name: string;
  data: any;
}

interface FilterData {
  key: string;
  data: any;
}

@Component({
  selector: 'app-filter-cabinets',
  templateUrl: './filter-cabinets.component.html',
  styleUrls: ['./filter-cabinets.component.scss']
})
export class FilterCabinetsComponent implements OnInit {

  noFilter: string;
  nbExperienceMin: string;
  nbExperienceMax: string;
  userFilter: FilterData[];
  filterDataCab: any;
  @Input() active: string[];
  @Output () filters  = new EventEmitter<string>();


  constructor(
    private api: ApiService,
    private cabinet: CabinetService,
    public zone: NgZone
  ) {
     this.noFilter = '';
     this.nbExperienceMin = '0';
     this.nbExperienceMax = '0';
     this.userFilter = [];
     this.userFilter.push({key: 'noFilter', data: []});
     this.userFilter.push({key: 'statut', data: []});
     this.userFilter.push({key: 'domain', data: []});
     this.userFilter.push({key: 'profil', data: []});
     this.userFilter.push({key: 'age', data: []});
     this.userFilter.push({key: 'exp_geographique', data: []});
     this.userFilter.push({key: 'exp_professionel', data: []});
     this.userFilter.push({key: 'experience', data: []});
     this.filterDataCab = [[]];
     this.cabinet.getMessage().subscribe(data => {
      if (data && data.message === 'new-cabinet' || data.message === 'update-cabinet'  || data.message === 'delete-cabinet') {
        this.getAllCabinetsFilter();
      }
     });
  }

  ngOnInit() {
    this.nbExperienceMin = '0';
    this.nbExperienceMax = '0';
    this.getAllCabinetsFilterOffline();
    this.refreshFilter();
  }



         // Get all cabinets filter
       getAllCabinetsFilter() {
        this.cabinet.getDomainesAndProfileCabinetsOrBureaux(true).then((cabinetsFilter: any) => {

          if (cabinetsFilter.message === 'success') {
              this.initRefeshCabinetData(cabinetsFilter);
          }
        }).catch(error => {
          this.api.manageServerError(error);
        });
      }


        // Refresh the filter
        refreshFilter() {
          setInterval(() => {
            this.cabinet.getDomainesAndProfileCabinetsOrBureaux(true);
          }, 600000);
        }

          // Get all cabinets filter
          getAllCabinetsFilterOffline() {
             const cabinetsFilter = localStorage.getItem(`data-cabinets-filters`) ?  JSON.parse(localStorage.getItem(`data-cabinets-filters`)) : null;
             const cabinetsFilterState =  localStorage.getItem('filter-userdata-cabinets') ? JSON.parse(localStorage.getItem('filter-userdata-cabinets')) : [];

             if (cabinetsFilterState && cabinetsFilterState.length > 0) {

              this.filterDataCab = cabinetsFilterState;
              this.noFilter = cabinetsFilterState[6][0];
              this.nbExperienceMin = cabinetsFilterState[7][0];
              this.nbExperienceMax = cabinetsFilterState[7][1];

            } else if (cabinetsFilter && cabinetsFilter.message === 'success') {
                  this.initRefeshCabinetData(cabinetsFilter);
            } else {
               this.getAllCabinetsFilter();
            }

          }


   // init de refresh cabinet data
   initRefeshCabinetData(cabinetsFilter: any) {
    this.filterDataCab[0] = [];
    if (cabinetsFilter && cabinetsFilter.liste_statut) {
      cabinetsFilter.liste_statut.forEach(element => {
        this.zone.run(() => {
          this.filterDataCab[0].push({choice: false, categorie: 'cabinet', value: element.statut.id,
          name: element.statut.intitule, data: element.nbre_cabinet});
        });

      });
    }

    // console.log(this.filterDataCab[0]);


    this.filterDataCab[1] = [];
    if (cabinetsFilter && cabinetsFilter.liste_domaine_expertise) {
      cabinetsFilter.liste_domaine_expertise.forEach(element => {
        this.zone.run(() => {
          this.filterDataCab[1].push({choice: false, categorie: 'cabinet', value: element.domaine.id,
          name: element.domaine.intitule, data: element.nbre_cabinet_bureau_etude});
        });
    
      });
    }
    // console.log(this.filterDataCab[1]);

    this.filterDataCab[2] = [];
    if (cabinetsFilter && cabinetsFilter.liste_profil) {
      cabinetsFilter.liste_profil.forEach(element => {
        this.zone.run(() => {
          this.filterDataCab[2].push({choice: false, categorie: 'cabinet', value: element.profil.id,
          name: element.profil.intitule, data: element.nbre_cabinet_bureau_etude});
        });

      });
    }
    // console.log(this.filterDataCab[2]);

    this.filterDataCab[3] = [];
    if (cabinetsFilter && cabinetsFilter.liste_age_cabinet_bureau_etude) {
      cabinetsFilter.liste_age_cabinet_bureau_etude.forEach(element => {
        this.zone.run(() => {
          this.filterDataCab[3].push({choice: false, categorie: 'cabinet', value: element.nbre_annee_creation,
          name: element.nbre_annee_creation > 1 ? ' ans' : ' an', data: element.nbre_cabinet_bureau_etude});
        });
        
      });
    }
    // console.log(this.filterDataCab[3]);

    this.filterDataCab[4] = [];
    if (cabinetsFilter && cabinetsFilter.liste_experience_geographique) {
      cabinetsFilter.liste_experience_geographique.forEach(element => {
        this.zone.run(() => {
          this.filterDataCab[4].push({choice: false, categorie: 'cabinet', value: element.experience_geographique.id,
          name: element.experience_geographique.intitule, data: element.nbre_cabinet_bureau_etude});
        });
       
      });
    }
    // console.log(this.filterDataCab[4]);

    this.filterDataCab[5] = [];
    if (cabinetsFilter && cabinetsFilter.liste_annee_experience_cabinet_bureau_etude) {
      cabinetsFilter.liste_annee_experience_cabinet_bureau_etude.forEach(element => {
        this.zone.run(() => {
          this.filterDataCab[5].push({choice: false, categorie: 'cabinet', value: element.nbre_annee_d_experience,
          name:  element.nbre_annee_d_experience > 1 ? ' ans' :  ' an', data: element.nbre_cabinet_bureau_etude});
        });
     
      });
    }
    // console.log(this.filterDataCab[5]);

    this.filterDataCab[6] = [0];

    this.filterDataCab[7] = [0, 0];

    localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));


   }

        // Update no Filter
        updateNoFilter() {
          this.userFilter[0].data = [this.noFilter ? 1 : 0 ];
          this.filterDataCab[6] = [this.noFilter ? 1 : 0 ];
          localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

        // Update the status
        updateFilterStatus() {
          const status = [];
          this.filterDataCab[0].forEach(statut => {
                if (statut.choice) {
                  status.push(statut.value);
                }
          });
          this.userFilter[1].data = status;
          localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

       // Update the filter domaine
       updateFilterDomaine() {
        const domanins = [];
        this.filterDataCab[1].forEach(domain => {
              if (domain.choice) {
                domanins.push(domain.value);
              }
        });
        this.userFilter[2].data = domanins;
        localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));
        this.filters.emit(JSON.stringify(this.userFilter));
       }

        // Update the filter profil
        updateFilterProfil() {
          const profils = [];
          this.filterDataCab[2].forEach(domain => {
            if (domain.choice) {
              profils.push(domain.value);
            }
          });
          this.userFilter[3].data = profils;
          localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));
          this.filters.emit(JSON.stringify(this.userFilter));
        }

         // update age
         updateFilterAge() {
          const ages = [];
          this.filterDataCab[3].forEach(age => {
            if (age.choice) {
              ages.push(age.value);
            }
          });
          this.userFilter[4].data = ages;
          localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));
          this.filters.emit(JSON.stringify(this.userFilter));
         }

         // update experience geogragrique
         updateFilterExpGeographique() {

          const expGeo = [];
          this.filterDataCab[4].forEach(exp => {
                if (exp.choice) {
                  expGeo.push(exp.value);
                }
          });
          this.userFilter[5].data = expGeo;
          localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));
          this.filters.emit(JSON.stringify(this.userFilter));

        }

         // update experience professionnelle
         updateFilterExpProfessionnel() {

          const expPro = [];
          this.filterDataCab[5].forEach(exp => {
                if (exp.choice) {
                  expPro.push(exp.value);
                }
          });
          this.userFilter[6].data = expPro;
          localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));
          this.filters.emit(JSON.stringify(this.userFilter));

        }


         // Update the filter
         updateFilerExperience() {
             this.userFilter[7].data = [this.nbExperienceMin, this.nbExperienceMax];
             this.filterDataCab[7] = this.userFilter[7].data;
             localStorage.setItem('filter-userdata-cabinets', JSON.stringify(this.filterDataCab));
              if (this.nbExperienceMin <= this.nbExperienceMax) {
                this.filters.emit(JSON.stringify(this.userFilter));
              }
          }

}
