import { DataService } from './../../providers/data/data.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isprofil: boolean;
  userData: any;
  userInfos: any;

  constructor(
    private router: Router,
    private data: DataService
  ) {
      this.listenMessage();
      this.userData = localStorage.getItem('user-role') ? JSON.parse(localStorage.getItem('user-role')) : null;
      this.userInfos = localStorage.getItem('user-data') ? JSON.parse(localStorage.getItem('user-data')) : null;
  }

  ngOnInit() {
    this.isprofil = false;
  }

  showProfil() {
    this.router.navigate(['/profil']);
    this.isprofil = true;
  }

  // change image background color
  listenMessage() {
    this.data.getDataMessage().subscribe(message => {
        if (message && message === 'no-back') {
           this.isprofil = false;
        }
    });
  }


}
