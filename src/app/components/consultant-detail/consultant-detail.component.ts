import { ConsultantService } from './../../providers/consultant/consultant.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from './../../providers/api/api.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DataService } from '../../providers/data/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { NgxSpinnerService } from 'ngx-spinner';
import { CustomValidators } from 'ng2-validation';
import { remote } from 'electron';
import { UserService } from '../../providers/user/user.service';

interface Domaine {
  domaine_id: number;
  nbre_consultation: number;
  nomDomaine: string;
}

interface CV {
  CV: string;
  filemime: string;
  filename: string;
}

@Component({
  selector: 'app-consultant-detail',
  templateUrl: './consultant-detail.component.html',
  styleUrls: ['./consultant-detail.component.scss']
})
export class ConsultantDetailComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});

  domainesList: any;
  experienceList: any;
  profilList: any;
  statutList: any;
  formationsList: any;
  countriesList: any;
  disponibilites: any;
  juridictions: any;
  formConsultant: FormGroup;
  nbreConsultant: number;
  domaine: any;
  listCvs: CV[];
  listDomaines: Domaine[];
  recommandationsList: any;
  fillallfieldError: boolean;
  statutIdError: boolean;
  disponibilyError: boolean;
  recommandationError: boolean;
  levelError: boolean;
  profilError: boolean;
  pdfAddError: boolean;
  experienceError: boolean;
  domainError: boolean;
  doctypeError: boolean;
  updateSuccess: boolean;
  internetError: boolean;
  bottomup: boolean;
  @Output () cancelled = new EventEmitter<string>();
  consultants: any;
  userRole: any;
  userRoleButton: any;

  constructor(
    private api: ApiService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private consultant: ConsultantService,
    private route: Router,
    private userService: UserService,
    private router: ActivatedRoute,
    private data: DataService
  ) {

    this.domainesList = [];
    this.experienceList = [];
    this.profilList = [];
    this.statutList = [];
    this.formationsList = [];
    this.recommandationsList = [];
    this.countriesList = [];
    this.nbreConsultant = 0 ;
    this.domaine = '';
    this.listDomaines = [];
    this.listCvs = [];
    this.fillallfieldError = false;
    this.statutIdError = false;
    this.disponibilyError = false;
    this.recommandationError = false;
    this.levelError = false;
    this.profilError = false;
    this.experienceError = false;
    this.domainError = false;
    this.pdfAddError = false;
    this.doctypeError = false;
    this.internetError = false;
    this.updateSuccess = false;
    this.bottomup = false;
    this.userRole = 'user';
    this.userRoleButton = localStorage.getItem('user-role') ?  JSON.parse(localStorage.getItem('user-role')): null;

   }

  ngOnInit() {

    this.getAllDomaines();
    this.getAllExperiences();
    this.getAllProfil();
    this.getAllFormations();
    this.getAllRecommandations();
    this.getCountries();
    this.getDisponibilites();
    this.getJuridictions();
    this.getAllStatus();
    this.checkSession();

    const key  = this.router.snapshot.params.key;
    this.consultants = JSON.parse(localStorage.getItem(key));
    this.initConsultantForm(this.consultants);
  }

  // Edit Informations
  editInformations() {
    this.userRole === 'user' ? this.userRole = 'admin' : this.userRole = 'user';
  }

  checkSession() {
    if (this.userService.getSession()) {
      this.api.refreshAccesToken();
    }
  }

    // parse the date format 'aaaa-mm-dd' or 'aaaa/mm/dd'
    parseDate(date: any) {
      const dateFormat = date.split('-');
      if (dateFormat && dateFormat.length > 0) {
        return  dateFormat[0] + '/' + dateFormat[1] + '/' + dateFormat[2];
      } else {
          return '';
      }
    }

    // Parse the list of domain
    parseDomaine(data: any) {
      if (data.domaine_expertise_users) {
        data.domaine_expertise_users.forEach(domain => {
          this.listDomaines.push({domaine_id: parseInt(domain.id, 10), nbre_consultation : domain.nbre_consultation,
            nomDomaine: domain.intitule});
        });
      }
      this.formConsultant.controls['liste_domaine_expertise'].setValue(this.listDomaines);
      this.domaine = '';
      this.nbreConsultant = 0;
   }


   // parse phone data
   parsePhoneData(data: any) {
    let phone = '';
    if (data.contact_users) {
      data.contact_users.forEach(tel => {
        phone ? phone += ',' + tel.telephone : phone = tel.telephone;
      });
    }
    return phone;
   }


   // parse phone data
  parseEmailData(data: any) {
     let phone = '';
     if (data.courriel_users) {
      data.courriel_users.forEach(mail => {
        phone ? phone += ',' + mail.email : phone = mail.email;
      });
     }
      return phone;
   }

   // Add a cv
   parseCv(docdata: any) {
    if (docdata.cv_users) {
      docdata.cv_users.forEach(file => {
        const name = file.cv.split('/') || [];
        this.listCvs.push({CV: file.cv_en_base_64, filemime: 'application/pdf',
         filename: name.length > 0 ? name[name.length - 1] : ''});
      });
    }
    this.formConsultant.controls['liste_CV'].setValue(this.listCvs);
  }

      // permet d'ajouter les consultants
      initConsultantForm(data: any) {
        const currentDate = new Date();
        const currentDateFormat = this.formatDate1(currentDate);
        this.formConsultant = this.fb.group({
          consultant_id: [data.consultant_id >= 0 ? data.consultant_id : null],
          statut_id: [data.statut && data.statut.id >= 0 ? data.statut.id : null],
          disponibilite_id: [data.disponibilite && data.disponibilite.id  >= 0? data.disponibilite.id : null],
          recommandation_id: [data.recommandation && data.recommandation.id >= 0 ? data.recommandation.id : null],
          recommandation_name: [data.recommandation && data.recommandation.intitule ? data.recommandation.intitule : null],
          nom: [data.nom ? data.nom : null],
          prenom: [data.prenom ? data.prenom : null],
          dateNaissance_view: [data.dateNaissance ? data.dateNaissance : null, Validators.compose([
          CustomValidators.maxDate(currentDateFormat)])],
          dateNaissance: [null],
          sexe: [data.sexe ? data.sexe : null],
          pays: [data.pays ? data.pays : null],
          ville: [data.ville ? data.ville : null],
          niveau_formation_id: [data.niveau_formation && data.niveau_formation.id >= 0 ? data.niveau_formation.id : null],
          liste_profil: [null],
          liste_profil_view: [data.profil_users && data.profil_users[0] && data.profil_users[0].id >= 0 ? data.profil_users[0].id : null],
          date1ereExperiencePro_view: [data.date1ereExperiencePro ? data.date1ereExperiencePro : null, Validators.compose([
          CustomValidators.maxDate(currentDateFormat)])],
          date1ereExperienceEnTantQueConsultant_view: [data.date1ereExperienceEnTantQueConsultant ? data.date1ereExperienceEnTantQueConsultant : null,
          Validators.compose([CustomValidators.maxDate(currentDateFormat)])],
          date1ereExperiencePro: [null],
          date1ereExperienceEnTantQueConsultant: [null],
          liste_experience_geographique: [null],
          liste_experience_geographique_view: [data.experience_geographique_users && data.experience_geographique_users[0] && data.experience_geographique_users[0].id >= 0 ? data.experience_geographique_users[0].id : null],
          experienceAvecCooperationAllemande: [null],
          experienceAvecLesNationsUnies: [null],
          experienceAvecLesAutresPartenaires: [null],
          experienceAvecCooperationAllemande_view: [data.experienceAvecCooperationAllemande && data.experienceAvecCooperationAllemande === 1 ? true :  null],
          experienceAvecLesNationsUnies_view: [data.experienceAvecLesNationsUnies && data.experienceAvecLesNationsUnies === 1 ? true :  null],
          experienceAvecLesAutresPartenaires_view: [data.experienceAvecLesAutresPartenaires && data.experienceAvecLesAutresPartenaires === 1 ? true :  null],
          liste_domaine_expertise: [[]],
          adresse: [data.adresse ? data.adresse : null],
          liste_telephone: [null],
          liste_telephone_view: [ data ? this.parsePhoneData(data) : null],
          liste_courriel: [null],
          liste_courriel_view: [data ? this.parseEmailData(data) : null],
          liste_CV: [null]
        }, {validator: [this.checkDate('dateNaissance_view', 'date1ereExperiencePro_view'),
        this.checkDate('dateNaissance_view', 'date1ereExperienceEnTantQueConsultant_view')]});
        this.parseDomaine(data);
        this.parseCv(data);
     }

  // check if date is valid
  checkDate(fisrtDate: string, secondDate: string) {
    return (group: FormGroup) => {
      const fisrtDateInput = group.controls[fisrtDate];
      const secondDateInput = group.controls[secondDate];
      if (secondDateInput.value <= fisrtDateInput.value) {
        return secondDateInput.setErrors({notMatch: true});
      } else {
        return secondDateInput.setErrors(null);
      }
    };
  }


  // Cancel the page
  cancel () {
      this.listCvs = [];
      this.listDomaines = [];
      this.initConsultantForm(this.consultants);
  }


      // Scroll on the top
      scrollTop() {
        // Hack: Scrolls to top of Page after page view initialized
        let top = document.getElementById('top');
        this.bottomup = true;
        if (top !== null) {
          top.scrollIntoView();
          top = null;
        }

        // animation duration
        setTimeout(() => {
          this.bottomup = false;
        }, 5000);
      }


     // show the confirmation dialog cancel
     confirmDialogCancel() {
      const dialog = remote.dialog;
      dialog.showMessageBox({
        type: 'none',
        title: 'Confirmation',
        message: 'Voulez-vous annuler les modifications ?',
        buttons: ['Non', 'Oui']
      }, (reponse) => {
            if (reponse === 0) {
              return;
            } else {
              this.cancel();
            }
      });
    }

  // Back to the list of Consultant
  backButton() {
      this.route.navigateByUrl('/dashboard/dashboard-v1/1');
  }

  // Get all countries
  getCountries() {

        this.api.getAllCountries().then(countries => {
            this.countriesList = countries;
        }).catch(error => {
            this.api.manageServerError(error);
        });
  }

   // Get all disponibilites
   getDisponibilites () {
    this.data.getAllDisponibilities().then((disponibilites: any) => {
      this.disponibilites = disponibilites.disponibilite;
    }).catch(error => {
       this.api.manageServerError(error);
    });
  }

  // Get all disponibilites
  getJuridictions () {
      this.data.getAllJuridictions().then((juridictions: any) => {
        this.juridictions = juridictions.forme_juridique;
      }).catch(error => {
         this.api.manageServerError(error);
      });
  }

   // Get the list of status
  getAllStatus() {
        this.data.getAllStatut().then((status: any) => {

          if (status.message === 'success') {
            this.statutList = status.statut;
          }
        }).catch(error => {
          this.api.manageServerError(error);
        });
    }


  // Get the list of domanines
  getAllDomaines() {
      this.data.getAllDomaines().then((domaines: any) => {

        if (domaines.message === 'success') {
          this.domainesList = domaines.domaines;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

    // Get the list of experiences
    getAllExperiences() {
      this.data.getAllGeographiqueExperience().then((experiences: any) => {
        if (experiences.message === 'success') {
          this.experienceList =  experiences.experience_geographique;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

   // Get the list of profil
    getAllProfil() {
        this.data.getAllProfils().then((profils: any) => {
          if (profils.message === 'success') {
            this.profilList =  profils.profil;
          }
        }).catch(error => {
          this.api.manageServerError(error);
        });
     }


    // Get the list of formations
    getAllFormations() {
      this.data.getAllFormations().then((formations: any) => {

        if (formations.message === 'success') {
          this.formationsList =  formations.niveau_formation;
        }
      }).catch(error => {
        this.api.manageServerError(error);
      });
   }

        // Get the list of recommandations
        getAllRecommandations() {
          this.data.getAllRecommandations().then((recommandations: any) => {

            if (recommandations.message === 'success') {
              this.recommandationsList =  recommandations.recommandation;
            }

          }).catch(error => {
            this.api.manageServerError(error);
          });
       }


       // add un domaine
       addDomaine() {
        this.listDomaines.push({domaine_id: parseInt( this.domainesList[this.domaine].id, 10), nbre_consultation : this.nbreConsultant,
          nomDomaine: this.domainesList[this.domaine].intitule });
          this.formConsultant.controls['liste_domaine_expertise'].setValue(this.listDomaines);
          this.domaine = '';
          this.nbreConsultant = 0;
       }

       // Remove the selected domaine
      removeDomaine(index: number) {
        this.listDomaines.splice(index, 1);
      }

      // format liste profil
      formatProfil(profil: string) {

          let listProfil: string[] = [];
          const resultat = [];

          if (profil) {
            const profilData = profil + ',';
            listProfil = profilData.split(',');
            listProfil.forEach(profilId => {
                if (profilId) {
                  resultat.push({profil_id: profilId});
                }
            });

          }

          this.formConsultant.controls['liste_profil'].setValue(resultat);
      }

      // format liste experience
      formatExperience(experience: string) {

          let listExperience: string[] = [];
          const resultat = [];

          if (experience) {

            const experienceData = experience + ',';
            listExperience = experienceData.split(',');
            listExperience.forEach(experienceId => {
                if (experienceId) {
                  resultat.push({experience_geographique_id: experienceId});
                }
            });

          }

          this.formConsultant.controls['liste_experience_geographique'].setValue(resultat);
       }


       // format phone
      formatPhone(phone: string) {

        let listPhones: string[] = [];
        const resultat = [];

        if (phone) {

          const phoneData = phone + ',';
          listPhones = phoneData.split(',');
          listPhones.forEach(phoneData => {
              if (phoneData) {
                resultat.push({telephone: phoneData});
              }
          });

        }

        this.formConsultant.controls['liste_telephone'].setValue(resultat);
     }


     // format phone
     formatCouriel(couriels: string) {
       let listCouriel: string[] = [];
       const resultat = [];
        if (couriels) {
          const courielsData = couriels + ',';
          listCouriel = courielsData.split(',');
          listCouriel.forEach(couriel => {
            if (couriel) {
              resultat.push({email: couriel});
            }
           });
         }
         this.formConsultant.controls['liste_courriel'].setValue(resultat);
      }

      // Format date  aaaa-mm-jj
      formatDate1(date: any) {
        const dateFormat  = new Date(date);
        const month  = (dateFormat.getMonth() + 1) < 10 ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
        const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
        return dateFormat.getFullYear() + '-' + month + '-' + day;
      }


      // Format date aaaa/mm/jj
      formatDate2(date: any) {
        const dateFormat  = new Date(date);
        const month  = (dateFormat.getMonth() + 1) < 10 ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
        const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
        return dateFormat.getFullYear() + '/' + month + '/' + day;
      }


     // Add a cv
      addCv(cvdata: any) {
        this.listCvs.push({CV: cvdata.data, filemime: cvdata.filemime, filename: cvdata.filename});
        this.formConsultant.controls['liste_CV'].setValue(this.listCvs);
      }

      // remove cv
      removeCv(index: number) {
        this.listCvs.splice(index, 1);
        this.uploader = new FileUploader({url: 'https://evening-anchorage-3159.herokuapp.com/api/'});
      }


   // Update upload the pdf
  onFilePdf(event) {
    this.pdfAddError = false;
    this.doctypeError = false;
    this.data.pdfReader(event).then(pdf => {
      if (pdf && pdf.filemime === 'application/pdf') {
        this.addCv(pdf);
      } else {
          // alert('Veuillez choisir un document au format PDF');
          this.doctypeError = true;
      }
    }).catch(error => {
      this.pdfAddError = true;
    });
   }


   // show the checkbox/radio value
  checkExperience(dataValue: any) {
}


     // show the confirmation dialog cancel
     confirmDialogUpdate(consulatantData: any) {
      const dialog = remote.dialog;
      dialog.showMessageBox({
        type: 'none',
        title: 'Confirmation',
        message: 'Voulez-vous enregistrer les modifications ?',
        buttons: ['Non', 'Oui']
      }, (reponse) => {
            if (reponse === 0) {
              return;
            } else {
              this.updateConsultant(consulatantData);
            }
      });
    }


      // update a consultant
      updateConsultant(consulatantData: any) {

        this.updateSuccess = false;
        this.internetError = false;

        this.spinner.show('consultant', {
          type: 'ball-spin-fade',
          size: 'large',
          bdColor: 'rgba(251,140,0, .8)',
          color: 'white'
        });


        this.formConsultant.get('recommandation_id').setValue(this.formConsultant.value.recommandation_id ? this.formConsultant.value.recommandation_id : 'NULL');
        this.formConsultant.get('recommandation_name').setValue(this.formConsultant.value.recommandation_name ? this.formConsultant.value.recommandation_name : 'NULL');

        const data = consulatantData;

        data.experienceAvecCooperationAllemande_view ? this.formConsultant.controls['experienceAvecCooperationAllemande'].setValue(1)
       : this.formConsultant.controls['experienceAvecCooperationAllemande'].setValue(0);

       data.experienceAvecLesNationsUnies_view ? this.formConsultant.controls['experienceAvecLesNationsUnies'].setValue(1)
       : this.formConsultant.controls['experienceAvecLesNationsUnies'].setValue(0);

       data.experienceAvecLesAutresPartenaires_view ? this.formConsultant.controls['experienceAvecLesAutresPartenaires'].setValue(1)
       : this.formConsultant.controls['experienceAvecLesAutresPartenaires'].setValue(0);

        this.formatProfil(data.liste_profil_view);
        this.formatExperience(data.liste_experience_geographique_view);
        this.formatPhone(data.liste_telephone_view);
        this.formatCouriel(data.liste_courriel_view);

        if (data.dateNaissance_view) {
          this.formConsultant.controls['dateNaissance'].setValue(
            this.formatDate2(data.dateNaissance_view));
        }

        if (data.date1ereExperiencePro_view) {
          this.formConsultant.controls['date1ereExperiencePro'].setValue(this.formatDate2(data.date1ereExperiencePro_view));
        }

        if (data.date1ereExperienceEnTantQueConsultant_view) {
          this.formConsultant.controls['date1ereExperienceEnTantQueConsultant'].setValue(
            this.formatDate1(data.date1ereExperienceEnTantQueConsultant_view));
        }
    
        this.consultant.updateConsultant(this.formConsultant.value).then((response: any) => {

              if (response && response.message === 'success') {
                    // Show a message that the consulant was add
                    this.consultant.sendMessage('update-consultant');
                    this.scrollTop();
                    this.updateSuccess = true;
                    setTimeout(() => {
                      this.updateSuccess = false;
                    }, 5000);
                    this.spinner.hide('consultant');
               }

        }).catch(error => {

             this.spinner.hide('consultant');

             alert('Essayer encore svp !');

                if (error && error.error) {

                  if (error.error.remplir_tous_les_champs) {
                       // Veuillez remplir tous les champs
                       this.fillallfieldError = true;
                  }

                  if (error.error.statut_id_not_exist) {
                    // Ce statut n'existe pas
                    this.statutIdError = true;

                  }

                  if (error.error.disponibilite_id_not_exist) {
                    // cette disponibilité n'existe pas
                    this.disponibilyError = true;

                  }

                  if (error.error.recommandation_id_not_exist) {
                    // Cette recommandation n'existe pas
                    this.recommandationError = true;
                  }

                  if (error.error.niveau_formation_id_not_exist) {
                     // Cette niveau n'existe pas
                     this.levelError = true;
                  }

                  if (error.error.profil_id_not_exist) {
                      // Ce profil n'existe pas
                      this.profilError = true;

                  }


                  if (error.error.experience_geographique_id_not_exist) {
                      // Cette experience n'existe pas
                      this.experienceError = false;

                  }

                  if (error.error.domaine_id_not_exist) {
                    // Ce domaine n'existe pas
                    this.domainError = false;

                  }

                } else {
                   this.api.manageServerError(error);
                }

        });

      }
}
