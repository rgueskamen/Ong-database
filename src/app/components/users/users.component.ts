import { DataService } from './../../providers/data/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  tabSelect: number;
  userData: any;

  constructor(
    private data: DataService
  ) {
    this.userData = localStorage.getItem('user-role') ?  JSON.parse(localStorage.getItem('user-role')) : null;
    this.data.sendDataMessage('no-back');
  }

  ngOnInit() {
    this.tabSelect = 1;
  }

    // Selected an option
    selectedTab(index: number) {
      this.tabSelect = index;
    }

}
