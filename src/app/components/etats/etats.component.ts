import { DataService } from './../../providers/data/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-etats',
  templateUrl: './etats.component.html',
  styleUrls: ['./etats.component.scss']
})
export class EtatsComponent implements OnInit {

  tabSelect: number;

  constructor(
    private data: DataService
  ) {
    this.data.sendDataMessage('no-back');
  }

  ngOnInit() {
    this.tabSelect = 1;
  }

    // Selected an option
    selectedTab(index: number) {
      this.tabSelect = index;
    }

}
