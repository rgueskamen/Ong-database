import { Component, ViewEncapsulation } from '@angular/core';
import { ElectronService } from './providers/electron.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { UserService } from './providers/user/user.service';
import { ApiService } from './providers/api/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,
    private userService: UserService,
    private api: ApiService
    ) {

     translate.setDefaultLang('en');

    if (electronService.isElectron()) {
         console.log('Mode electron');
         console.log('Electron ipcRenderer', electronService.ipcRenderer);
         console.log('NodeJS childProcess', electronService.childProcess);
    } else {
         console.log('Mode web');
    }

    this.checkSession();

  }


  // ckeck and renew session
  checkSession() {
    if (this.userService.getSession()) {
      this.api.refreshAccesToken();
  }
  }
// Scroll on the top of the page
  onActivate(event) {
    window.scroll(0, 0);
  }
}
