
import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './components/shared/shared.module';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { EtatsComponent } from './components/etats/etats.component';
import { ConsultantStatsComponent } from './components/state/consultant-stats/consultant-stats.component';
import { CabinetStatsComponent } from './components/state/cabinet-stats/cabinet-stats.component';
import { UsersComponent } from './components/users/users.component';
import { OngStatsComponent } from './components/state/ong-stats/ong-stats.component';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { UpdateUserComponent } from './components/user/update-user/update-user.component';
import { AddUserComponent } from './components/user/add-user/add-user.component';
// import 'script-loader!jszip/dist/jszip.min.js';



// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective,
    EtatsComponent,
    ConsultantStatsComponent,
    CabinetStatsComponent,
    UsersComponent,
    OngStatsComponent,
    UserListComponent,
    UpdateUserComponent,
    AddUserComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    FontAwesomeModule,
    ChartsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff',
      secondaryColour: '#ffffff',
      tertiaryColour: '#ffffff'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [ElectronService],
  bootstrap: [AppComponent]
})
export class AppModule { }
