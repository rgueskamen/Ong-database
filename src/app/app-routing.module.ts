import { AuthGuard } from './components/core/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/users/users.component';
import { EtatsComponent } from './components/etats/etats.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpdateUserComponent } from './components/user/update-user/update-user.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: 'session',
        loadChildren : './components/user/user.module#UserModule'
    },
    {
        path: '',
        component: HomeComponent,
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children: [{
            path: 'dashboard',
            loadChildren: './components/dashboard/dashboard.module#DashboardModule'
        },
        {
            path: 'users',
            component : UsersComponent
        },
        {
            path: 'etats',
            component : EtatsComponent
        },
        {
            path: 'profil',
            component : UpdateUserComponent
        }
    ]
    },
    { path: '**', redirectTo: 'session' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
