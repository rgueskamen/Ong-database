import { ApiService } from './../api/api.service';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private event = new Subject<any>();
  private subjet = new Subject<any>();

  constructor(
    private api: ApiService
  ) { }

      // Send message to update ong Data
      sendMessage(param: string) {
        this.subjet.next({message: param});
      }
    
      getMessage(): Observable<any> {
        return this.subjet.asObservable();
      }


  // 1- permet de renvoyer tous les statuts
  getAllStatut(refresh ?: boolean) {
    return new Promise((resolve, reject) => {
      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {
          this.api.get(`statut/get`).subscribe(reponse => {
            localStorage.setItem('ambero-status', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const amberoStatus = JSON.parse(localStorage.getItem('ambero-status'));
          resolve(amberoStatus);
        }
      });

    });
  }


  // 2- permet de renvoyer toutes les disponibilités
  getAllDisponibilities(refresh ?: boolean) {
    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {

          this.api.get(`disponibilite/get`).subscribe(reponse => {
            localStorage.setItem('ambero-disponibilites', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });

        } else if (network === 'offline') {

          const amberoDisponibilites = JSON.parse(localStorage.getItem('ambero-disponibilites'));
          resolve(amberoDisponibilites);

        }

      });


    });

  }

  // 3- permet de renvoyer toutes les recommandations
  getAllRecommandations(refresh ?: boolean) {
    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {
          this.api.get(`recommandation/get`).subscribe(reponse => {
            localStorage.setItem('ambero-recommandations', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });

        } else if (network === 'offline') {
          const amberoRecommandations = JSON.parse(localStorage.getItem('ambero-recommandations'));
          resolve(amberoRecommandations);
        }
      });
    });

  }

  // 4- permet de renvoyer tous les niveaux de formation
  getAllFormations(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`niveau_formation/get`).subscribe(reponse => {
            localStorage.setItem('ambero-nivFormations', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });

        } else if (network === 'offline') {
          const amberoNivFormations = JSON.parse(localStorage.getItem('ambero-nivFormations'));
          resolve(amberoNivFormations);
        }

      });

    });

  }

  // 5- permet de renvoyer tous les profils
  getAllProfils(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {

          this.api.get(`profil/get`).subscribe(reponse => {
            localStorage.setItem('ambero-allProfil', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const amberoAllProfil = JSON.parse(localStorage.getItem('ambero-allProfil'));
          resolve(amberoAllProfil);

        }

      });
    });

  }

  // 5'- permet de renvoyer tous les profils
  addProfil(data: any) {

    return new Promise((resolve, reject) => {

      this.api.post(`profil/post`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });
    });

  }

  // 5''- permet de renvoyer tous les profils
  editProfil(data: any) {

    return new Promise((resolve, reject) => {
      this.api.post(`profil/edit/${data.profil_id}`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });
    });

  }


  // 6- permet de renvoyer toutes les expériences géographiques
  getAllGeographiqueExperience(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {
          this.api.get(`experience_geographique/get`).subscribe(reponse => {
            localStorage.setItem('ambero-expgeographique', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });

        } else if (network === 'offline') {
          const amberoExpgeographique = JSON.parse(localStorage.getItem('ambero-expgeographique'));
          resolve(amberoExpgeographique);
        }

      });
    });

  }

  // 7- permet de renvoyer tous les domaines
  getAllDomaines(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {
          this.api.get(`domaines/get`).subscribe(reponse => {
            localStorage.setItem('ambero-domaines', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const amberoDomaines = JSON.parse(localStorage.getItem('ambero-domaines'));
          resolve(amberoDomaines);

        }

      });
    });

  }

  // 7'- permet de renvoyer tous les domaines
  addDomaine(data: any) {

    return new Promise((resolve, reject) => {

      this.api.post(`domaines/post`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });
    });
  }

  // 7''- permet de renvoyer tous les domaines
  editDomaine(data: any) {

    return new Promise((resolve, reject) => {
      this.api.post(`domaines/edit/${data.domaine_id}`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });
    });
  }

  // 8- permet de renvoyer toutes les formes juridiques
  getAllJuridictions(refresh ?: boolean) {
    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {
          this.api.get(`forme_juridique/get`).subscribe(reponse => {
            localStorage.setItem('ambero-juridictions', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const amberoJurictions = JSON.parse(localStorage.getItem('ambero-juridictions'));
          resolve(amberoJurictions);
        }

      });

    });

  }


  /* Get data by Ids */

  // Group by Profil by Id
  getAllProfilById(id: number, consultants: any) {
    const consultantProfil = [];
    consultants.forEach(consultant => {
      if (consultant.profil_users) {
        consultant.profil_users.forEach(profil => {
          if (profil&&profil.id === id) {
            consultantProfil.push(consultant);
          }
        });
      }
    });

    return consultantProfil;
  }

  // Group by Domain by Id
  getAllDomainById(id: number, consultants: any) {
    const consultantDomain = [];
    consultants.forEach(consultant => {
      if (consultant.domaine_expertise_users) {
        consultant.domaine_expertise_users.forEach(domain => {
          if (domain && domain.id === id) {
            consultantDomain.push(consultant);
          }
        });
      }
    });

    return consultantDomain;
  }

  // Group by Domain by Id
  getAllStatutById(id: number, consultants: any) {
    const consultantStatut = [];
    consultants.forEach(consultant => {
      if (consultant.statut && consultant.statut.id === id) {
        consultantStatut.push(consultant);
      }
    });

    return consultantStatut;
  }

  // Get a single data
  getData(id: number, key: string, data: any) {

    let foundElement = null;

    if (data && data.length) {
      data.forEach(element => {
        if (element && (element[key] === id)) {
          foundElement = element;
        }
      });
    }

    return foundElement;
  }

  // Get data by key
  getDataByKey(key: string, data: any) {

    let i = 0;

    while (i < data.length) {

      if (data[i] && (data[i].key === key)) {
        return data[i].data;
      }

      i++;
    }

    return [];

  }


  // Check if an id  is in array
  isIdinarray(id: any, ids: number[]): boolean {

    let found = false, i = 0, nbOccur = 0;

    if (id && id.length > 0) {
      while (i < ids.length && !found) {

        id.forEach(item => {
          if (item&&(ids[i] === item.id)) {
            nbOccur++;
          }
        });
        i++;
      }
    }
    nbOccur === ids.length ? found = true : found = false;
    return found;
  }

  // Send Message to component
  sendDataMessage(data: any) {
    this.event.next(data);
  }

  // listen to message
  getDataMessage() {
    return this.event.asObservable();
  }

  // Format an array to matrix and can be used for pagination
  formatArrayToMatrix(arraydata: any[], nbElements: number) {

    const nbres = arraydata.length;
    const nbIter = Math.ceil(nbres / nbElements);
    const matrix = [];
    let subMatrix = [];
    let index = 0, j = 0;

    while (index < nbIter) {

      subMatrix = [], j = 0;

      while (j < nbElements) {

        if (index * nbElements + j < nbres) {
          subMatrix.push(arraydata[index * nbElements + j]);
        }
        j++;
      }

      if (subMatrix.length > 0) {
        matrix.push(subMatrix);
      }

      index++;
    }

    return matrix;
  }


  // Pdf reader

  pdfReader(event: any): Promise<any> {


    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      if (event.target.files && event.target.files.length > 0) {

        const file = event.target.files[0];
        reader.readAsDataURL(file);

        reader.onload = () => {

          const pdfData: any = reader.result;
          const filename = file.name;
          const filemime = file.type;
          const data = pdfData.split(',')[1];

          const pdfFormat = {
            filename: filename,
            filemime: filemime,
            data: data
          };

          resolve(pdfFormat);

        };
      } else {
        reject(null);
      }
    });

  }

}
