import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Subject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private subjet = new Subject<any>();

  constructor(
    private api: ApiService
  ) { }


    // Send message to update ong Data
    sendMessage(param: string) {
      this.subjet.next({message: param});
  }

  getMessage(): Observable<any> {
    return this.subjet.asObservable();
  }

    // check if a user has a session
    getSession() {
       const token =  localStorage.getItem('user-role') ? JSON.parse(localStorage.getItem('user-role')) : null;
       return token ? true : false;
    }

     // 1- permet de renvoyer tous les roles des users
     getAllUserRole() {
        return new Promise((resolve, reject) => {
          this.api.checkNetworkStatus().then(network => {
            if (network === 'online') {
              this.api.get(`users/get/role`).subscribe(reponse => {
                resolve(reponse);
                localStorage.setItem('user-roles', JSON.stringify(reponse));
              }, error => {
                reject(error);
              });
            } else if (network === 'offline') {
               const userRole = JSON.parse(localStorage.getItem('user-roles'));
               resolve(userRole);
            }
          });
      });
     }

     // 2- permet d'enregistrer un user avec son rôle
     registerUser(data: any) {
      if (data && data.password) {
        localStorage.setItem('user-pass', JSON.stringify(data.password));
       }
      return new Promise((resolve, reject) => {
        this.api.post(`users/signUp`, data).subscribe(reponse => {
          if (reponse && reponse.message === 'success') {
            if (reponse && reponse.user) {
              localStorage.setItem('user-data', JSON.stringify(reponse.user));
            }

            if (reponse && reponse.token) {
              localStorage.setItem('user-token', JSON.stringify(reponse.token));
            }

            if (reponse && reponse.role) {
              localStorage.setItem('user-role', JSON.stringify(reponse.role));
            }
          }
          resolve(reponse);

        }, error => {
          reject(error);
        });
      });

     }

     // 3-  permet de login un user
     loginUser(data: any) {
       if (data && data.password) {
        localStorage.setItem('user-pass', JSON.stringify(data.password));
       }
      return new Promise((resolve, reject) => {
        this.api.post(`users/signIn`, data).subscribe(reponse => {
          if (reponse && reponse.message === 'success') {

            if (reponse && reponse.user) {
              localStorage.setItem('user-data', JSON.stringify(reponse.user));
            }

            if (reponse && reponse.token) {
              localStorage.setItem('user-token', JSON.stringify(reponse.token));
            }

            if (reponse && reponse.role) {
              localStorage.setItem('user-role', JSON.stringify(reponse.role));
            }
            
          }
          resolve(reponse);
        }, error => {
          reject(error);
        });
      });

     }


     // 4- permet de get la liste des users
     getUsers(refresh ?: boolean) {
        return new Promise((resolve, reject) => {
          this.api.checkNetworkStatus().then(network => {
            if (network === 'online' || refresh) {
              this.api.get(`users/get`).subscribe(reponse => {
                resolve(reponse);
                localStorage.setItem('user-list', JSON.stringify(reponse));
              }, error => {
                reject(error);
              });
            } else if (network === 'offline') {
              const usersList = JSON.parse(localStorage.getItem('user-list'));
              resolve(usersList);
            }
          });
        });
    }


    // 5 - permet d'update les infos d'un user
    updateUser(data: any) {
      return new Promise((resolve, reject) => {
          const token = JSON.parse(localStorage.getItem('user-token'));
          this.api.post(`users/update/${token}`, data).subscribe(reponse => {
            resolve(reponse);
          }, error => {
            reject(error);
          });
      });
    }


    // 6- permet de désactiver ou supprimer un user
    disableUser(user_id: any) {

      return new Promise((resolve, reject) => {
        this.api.get(`users/delete/${user_id}`).subscribe(reponse => {
          resolve(reponse);
        }, error => {
          reject(error);
        });
      });

    }


    // 7- permet de réactiver un user
    activateUser(user_id: any) {
      return new Promise((resolve, reject) => {
        this.api.get(`users/active/${user_id}`).subscribe(reponse => {
          resolve(reponse);
        }, error => {
          reject(error);
        });
      });
    }


    // 8- permet de changer le rôle d'un user
    updateUserRole(data: any) {
      return new Promise((resolve, reject) => {
          this.api.post(`users/update/role/${data.user_id}`, data).subscribe(reponse => {
            resolve(reponse);
          }, error => {
            reject(error);
          });
      });
    }



     // 9- permet d'ajouter un user avec son rôle
     addUser(data: any) {
      return new Promise((resolve, reject) => {
          this.api.post(`users/signUp`, data).subscribe(reponse => {
            resolve(reponse);
          }, error => {
            reject(error);
          });
      });

     }


}
