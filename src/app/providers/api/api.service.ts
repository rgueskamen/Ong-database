import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url: string;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.url = 'http://';
  }

  // Check the network status
  checkNetworkStatus() {
    return new Promise((resolve, reject) => {

      this.http.get(`${this.url}/users/get/role`).subscribe((reponse: any) => {
        this.setWorkMode('online');
        resolve('online');
      }, error => {
        if (error && error.status === 0) {
          this.setWorkMode('offline');
          resolve('offline');
        } else {
          this.setWorkMode('online');
          resolve('online');
        }
      });

    });
  }

    // Logout the session
    logout() {
      localStorage.removeItem('user-data');
      localStorage.removeItem('user-pass');
      localStorage.removeItem('user-roles');
      localStorage.removeItem('user-role');
      localStorage.removeItem('user-token');
      localStorage.removeItem('user-list');
      localStorage.removeItem('workMode');
      localStorage.removeItem(`data-cabinets-all`);
      localStorage.removeItem(`data-cabinets-filters`);
      localStorage.removeItem(`data-cabinets-statistiques`);
      localStorage.removeItem('data-consultant-all');
      localStorage.removeItem('data-consultant-filters');
      localStorage.removeItem('data-consultant-statistique');
      localStorage.removeItem('ambero-status');
      localStorage.removeItem('ambero-disponibilites');
      localStorage.removeItem('ambero-recommandations');
      localStorage.removeItem('ambero-nivFormations');
      localStorage.removeItem('ambero-allProfil');
      localStorage.removeItem('ambero-expgeographique');
      localStorage.removeItem('ambero-domaines');
      localStorage.removeItem('ambero-juridictions');
      localStorage.removeItem(`data-ong-all`);
      localStorage.removeItem(`data-ong-filters`);
      localStorage.removeItem(`data-ong-statistiques`);
      localStorage.clear();
      this.router.navigate(['/session']);
    }

  /**
   *  Get the users working mode
   * */
  getWorkMode(): string {
    return  localStorage.getItem('workMode') ? JSON.parse(localStorage.getItem('workMode')) : null;
  }


  /**
  *  Get the users working mode
  * */
  setWorkMode(mode: string) {
    localStorage.setItem('workMode', JSON.stringify(mode));
  }


  // HTTP Ends points
  get(endpoint: string): Observable<any> {
    return this.http.get(this.url + '/' + endpoint);
  }

  post(endpoint: string, body: any): Observable<any> {
    return this.http.post(this.url + '/' + endpoint, JSON.stringify(body));
  }

  put(endpoint: string, body: any): Observable<any> {
    return this.http.put(this.url + '/' + endpoint, JSON.stringify(body));
  }

  delete(endpoint: string): Observable<any> {
    return this.http.delete(this.url + '/' + endpoint);
  }

  patch(endpoint: string, body?: any): Observable<any> {
    return this.http.patch(this.url + '/' + endpoint, JSON.stringify(body));
  }


  // Refresh the token error
  refreshAccesToken() {
    const pass = localStorage.getItem('user-pass') ? JSON.parse(localStorage.getItem('user-pass')) : null;
    const data = localStorage.getItem('user-data') ? JSON.parse(localStorage.getItem('user-data')) : null;
    if (data && pass) {
      const user_param = { email_or_phone: data.email || data.phone, password: pass };
      this.post(`/users/signIn`, user_param).subscribe(reponse => {
        if (reponse && reponse.message === 'success') {
          if (reponse && reponse.user) {
            localStorage.setItem('user-data', JSON.stringify(reponse.user));
          }
          if (reponse && reponse.token) {
            localStorage.setItem('user-token', JSON.stringify(reponse.token));
          }

          if (reponse && reponse.role) {
            localStorage.setItem('user-role', JSON.stringify(reponse.role));
          }
        }
      }, error => {
      });
    } else {
        this.logout();
    }
  }


  /**
   *  Manage server error
   */
  manageServerError(error: any) {
    switch (error.status) {
      case 0:
        this.setWorkMode('offline');
        // Show Error with Electron Dialog.
        break;

      case 401:
        this.refreshAccesToken();
        break;

      case 500:
        // Something wrong happen on the server.
        break;

      default:
        break;
    }
  }

    // get all countries
  getAllcountriesData() {
  const baseUrl = 'https://restcountries.eu/rest/v2/';
    return new Observable((observer) => {
    this.http.get(baseUrl + 'all').subscribe((countries: any) => {
        observer.next(countries);
      }, error => {
        this.http.get(`assets/json/countries.json`).subscribe((countries: any) => { observer.next(countries); });
      }); 
    });
  }

  // All countries
  getAllCountries() {
    return new Promise((resolve, reject) => {
      this.getAllcountriesData()
        .subscribe(response => {
          resolve(response);
        }, error => {
          reject(error);
        });
    });
  }

  // Get time prefix
  getTimePrefix() {
    const dateFormat = new Date();
    const month = (dateFormat.getMonth() + 1) < 10 ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
    const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
    const hour = dateFormat.getHours() < 10 ? '0' + dateFormat.getHours() : dateFormat.getHours();
    const minute = dateFormat.getMinutes() < 10 ? '0' + dateFormat.getMinutes() : dateFormat.getMinutes();
    const second = dateFormat.getSeconds() < 10 ? '0' + dateFormat.getSeconds() : dateFormat.getSeconds();
    return dateFormat.getFullYear() + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
  }


}
