import { ApiService } from './../api/api.service';
import { Injectable, EventEmitter } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OngService {

  private subjet = new Subject<any>();

  constructor(
    private api: ApiService
  ) { }

  // Send message to update ong Data
  sendMessage(param: string) {
      this.subjet.next({message: param});
  }

  getMessage(): Observable<any> {
    return this.subjet.asObservable();
  }

  // 1-  permet d'enregistrer une ong ou association
  saveOngAssociation(data: any) {

    return new Promise((resolve, reject) => {
      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.post(`ong/association/post/${token}`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });
    });

  }

  // 2- permet de renvoyer la liste des  ongs ou associations sans filtre à partir d'un numéro de page
  listOngAssociation(num_page: number, nbre_donnees_par_page: number, refresh ?: boolean) {

    return new Promise((resolve, reject) => {
      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`ong/association/get/byPage/${num_page}/${nbre_donnees_par_page}`).subscribe(reponse => {
            localStorage.setItem(`data-ong-page-${num_page}`, JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataOngPage = JSON.parse(localStorage.getItem(`data-ong-page-${num_page}`));
          resolve(dataOngPage);
        }
      });
    });

  }

  // 3- permet de renvoyer la liste de tous les  ongs ou associations
  listOngAssociationAll(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`ong/association/get/all`).subscribe(reponse => {
            localStorage.setItem(`data-ong-all`, JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataOngAll = JSON.parse(localStorage.getItem(`data-ong-all`));
          resolve(dataOngAll);
        }
      });

    });

  }


  /* 4- permet de get les domaines d'intervention et les profils en comptant les  ong ou association
   dans chaque domaine et dans chaque profil */

  getDomainesAndProfileOngAssociation(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`ong/association/get/infos/sidebar`).subscribe(reponse => {
            localStorage.setItem(`data-ong-filters`, JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });

        } else if (network === 'offline') {
          const dataOngFilters = JSON.parse(localStorage.getItem(`data-ong-filters`));
          resolve(dataOngFilters);
        }

      });

    });

  }


  /**
   *  5-permet d'exporter dans un dossier zip tous les fichiers des  ongs ou associations d'une page
   */

  exportFolderOngAssociation(data: any) {

    return new Promise((resolve, reject) => {
      this.api.post(`ong/association/export/file`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });
    });

  }


  /**
  *  6 - permet de donner les statisques en fonction des ongs ou associations
  */

  ongAssociationStats(refresh ?: boolean) {
    return new Promise((resolve, reject) => {
      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {
          this.api.get(`ong/association/get/statistique`).subscribe(reponse => {
            localStorage.setItem(`data-ong-statistiques`, JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });

        } else if (network === 'offline') {
          const dataOngSatitisque = JSON.parse(localStorage.getItem(`data-ong-statistiques`));
          resolve(dataOngSatitisque);

        }

      });

    });

  }


  /**
 *  7 - Modifier les informations d'une ong ou association
 */

  updateOngAssociation(data: any) {

    return new Promise((resolve, reject) => {
      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.post(`ong/association/update/${data.ong_id}/${token}`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });

    });

  }


  /**
  *  8 - Supprimer les informations d'une ong ou association'
  */
  deleteOngAssociation(ong_id: any) {

    return new Promise((resolve, reject) => {

      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.get(`ong/association/delete/${ong_id}/${token}`).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });

    });

  }
}
