import { ApiService } from './../api/api.service';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CabinetService {

  private subjet = new Subject<any>();

  constructor(
    private api: ApiService
  ) { }

  // 1-  permet d'enregistrer un cabinet ou bureau d'étude

    // Send message to update ong Data
    sendMessage(param: string) {
      this.subjet.next({message: param});
    }
  
    getMessage(): Observable<any> {
      return this.subjet.asObservable();
    }

  saveCabinetOrBureau(data: any) {

    return new Promise((resolve, reject) => {
      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.post(`cabinet/bureau/etude/post/${token}`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });
    });

  }

  // 2- permet de renvoyer la liste des cabinets et bureaux d'étude sans filtre à partir d'un numéro de page

  listCabinetOrBureau(num_page: number, nbre_donnees_par_page: number, refresh ?: boolean) {

    return new Promise((resolve, reject) => {
      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`cabinet/bureau/etude/get/byPage/${num_page}/${nbre_donnees_par_page}`).subscribe(reponse => {
            localStorage.setItem(`data-cabinets-page-${num_page}`, JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataCabinetsPage = JSON.parse(localStorage.getItem(`data-cabinets-page-${num_page}`));
          resolve(dataCabinetsPage);
        }
      });
    });

  }

  // 3- permet de renvoyer la liste de tous les cabinets et bureaux d'étude

  listCabinetOrBureauAll(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`cabinet/bureau/etude/get/all`).subscribe(reponse => {
            localStorage.setItem(`data-cabinets-all`, JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataCabinetsAll = JSON.parse(localStorage.getItem(`data-cabinets-all`));
          resolve(dataCabinetsAll);
        }
      });

    });

  }


  /* 4- permet de get les domaines d'intervention et les profils en comptant les cabinets ou bureaux
   d'étude dans chaque domaine et dans chaque profil */

  getDomainesAndProfileCabinetsOrBureaux(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`cabinet/bureau/etude/get/infos/sidebar`).subscribe(reponse => {
            localStorage.setItem(`data-cabinets-filters`, JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataCabinetsFilters = JSON.parse(localStorage.getItem(`data-cabinets-filters`));
          resolve(dataCabinetsFilters);
        }

      });

    });

  }


  /**
   *  5-permet d'exporter dans un dossier zip tous les fichiers des cabinets ou bureaux d'étude d'une page
   */

  exportFolderCabinetOrBureau(data: any) {

    return new Promise((resolve, reject) => {

      this.api.post(`cabinet/bureau/etude/export/file`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });

    });

  }


  /**
  *  6 - permet de donner les statisques en fonction des cabinets
  */

  cabinetStats(refresh ?: boolean) {

    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {
          this.api.get(`cabinet/bureau/etude/get/statistique`).subscribe(reponse => {
            localStorage.setItem(`data-cabinets-statistiques`, JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });

        } else if (network === 'offline') {
          const dataCabinetsSatitisque = JSON.parse(localStorage.getItem(`data-cabinets-statistiques`));
          resolve(dataCabinetsSatitisque);

        }

      });

    });

  }


  /**
 *  7 - Modifier les informations du cabinets
 */

  updateCabinet(data: any) {

    return new Promise((resolve, reject) => {

      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.post(`cabinet/bureau/etude/update/${data.cabinet_id}/${token}`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });

    });

  }


  /**
  *  8 - Supprimer les informations du cabinets
  */
  deleteCabinet(cabinet_id: any) {

    return new Promise((resolve, reject) => {
      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.get(`cabinet/bureau/etude/delete/${cabinet_id}/${token}`).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });

    });

  }

}
