import { ApiService } from './../api/api.service';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsultantService {

  private subjet = new Subject<any>();

  constructor(
    private api: ApiService
  ) { }


    // Send message to update ong Data
  sendMessage(param: string) {
    this.subjet.next({message: param});
  }

  getMessage(): Observable<any> {
    return this.subjet.asObservable();
  }


  // 1- permet d'enregistrer un consultant
  saveConsultant(data: any) {

    return new Promise((resolve, reject) => {

      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.post(`consultant/post/${token}`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });


    });
  }


  // 2- permet de renvoyer la liste des consultants sans filtre à partir d'un numéro de page
  listConsultant(num_page: number, nbre_donnees_par_page: number, refresh ?: boolean) {

    return new Promise((resolve, reject) => {
      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`consultant/get/byPage/${num_page}/${nbre_donnees_par_page}`).subscribe(reponse => {
            localStorage.setItem(`data-consultant-page-${num_page}`, reponse);
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataConsultantPage = JSON.parse(localStorage.getItem(`data-consultant-page-${num_page}`));
          resolve(dataConsultantPage);
        }
      });

    });

  }


  // 3- permet de renvoyer la liste de tous les consultants
  listConsultantAll(refresh ?: boolean) {

    return new Promise((resolve, reject) => {
      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`consultant/get/all`).subscribe(reponse => {
            localStorage.setItem('data-consultant-all', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataConsultant = JSON.parse(localStorage.getItem('data-consultant-all'));
          resolve(dataConsultant);
        }
      });
    });

  }



  /* 4- permet de get les domaines d'intervention, les profils et les niveaux de formation en comptant
   les consultants dans chaque domaine, dans chaque profil et dans chaque niveau de formation */
  getDomainesConsultants(refresh ?: boolean) {

    return new Promise((resolve, reject) => {
      this.api.checkNetworkStatus().then(network => {
        if (network === 'online' || refresh) {
          this.api.get(`consultant/get/infos/sidebar`).subscribe(reponse => {
            localStorage.setItem('data-consultant-filters', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataConsultantFilter = JSON.parse(localStorage.getItem('data-consultant-filters'));
          resolve(dataConsultantFilter);
        }

      });
    });

  }



  /* 5-  permet d'exporter dans un dossier zip tous les cv des consultants d'une page */
  exportFolderConsultant(data: any) {

    return new Promise((resolve, reject) => {
      this.api.post(`consultant/export/file`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });
    });

  }

  // 6- permet de renvoyer les statistiques des consultants en genre sur les CVs et par domaine d'intervention
  consultantStats(refresh ?: boolean) {
    return new Promise((resolve, reject) => {

      this.api.checkNetworkStatus().then(network => {

        if (network === 'online' || refresh) {
          this.api.get(`consultant/get/statistique`).subscribe(reponse => {
            localStorage.setItem('data-consultant-statistique', JSON.stringify(reponse));
            resolve(reponse);
          }, error => {
            reject(error);
          });
        } else if (network === 'offline') {
          const dataConsultantStatistique = JSON.parse(localStorage.getItem('data-consultant-statistique'));
          resolve(dataConsultantStatistique);
        }

      });

    });
  }


  /**
 *  7 - Modifier les informations du consultant
 */

  updateConsultant(data: any) {

    return new Promise((resolve, reject) => {
      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.post(`consultant/update/${data.consultant_id}/${token}`, data).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });

    });

  }


  /**
  *  8 - Supprimer les informations du consultant
  */
  deleteConsultant(consultant_id: any) {

    return new Promise((resolve, reject) => {
      const token = JSON.parse(localStorage.getItem('user-token'));
      this.api.get(`consultant/delete/${consultant_id}/${token}`).subscribe(reponse => {
        resolve(reponse);
      }, error => {
        reject(error);
      });

    });

  }



}

